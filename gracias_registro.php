<!doctype html>
<html lang="en">
<head>
	<!-- ===================================
	{{Chunk site_seo}}
	==================================== -->
	<?php include 'includes/seo.php' ;?>
	<!-- ===================================
	{{fin Chunk site_seo}}
	==================================== -->
	<meta name="description" content="Detalle del Producto">
    <meta property="og:title" content="[[*page_title]]" /> 
    <meta property="og:url" content="[[++site_url]][[~id]]" />
    <meta property="og:image" content="[[*page_imagen]]" />
    <meta property="og:description" content="[[*page_description]]" /> 
    <meta name="twitter:image:src" content="[[*page_imagen]]"/>
	<title>[[*page_title]]</title>	
</head>
<body>	
	<!-- ===================================
	{{Chunk site_header}}
	==================================== -->
	<?php include 'includes/header.php' ?>
	<!-- ===================================
	{{fin Chunk site_header}}
	==================================== -->

	<!-- ===================================
	{{Contenido principal}}
	==================================== -->
	<section id="principal" class="cleaner carritodecompras maxw container-fluid">		
		<article id="mispedidos" class="cleaner mt2 cupon">
			<div id="titulo" class="cleaner">
				<div class="row cleaner">
					<div class="col-md-12 cleaner">
						<div class="titulo cleaner">
							<div class="capa cleaner">
								<div class="lineas cleaner">
									<h2>Gracias por <strong>registrarte</strong></h2>
								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>
			<div class="listadeproductos cleaner maxw3 gracias">
				<div class="counterproduct cleaner animated">
					<div class="row cleaner">
						<div class="cleaner col-md-5">
							<div class="fotoproducto cleaner">
								<img src="fotos/producto.jpg" alt="" class="cleaner">
							</div>							
						</div>
						<div class="cleaner vat col-md-7">	
							<div class="cleaner tac main">
								<div class="cleaner">
									<p>Te damos la bienvenida a Chocolate Express.</p>
									<p>Por registrarte ganaste un:</p>
								</div>
								<div class="cleaner">
									<div class="etiqueta cleaner" style="background-image:url('imagenes/etiqueta.png')">
										<div class="cont cleaner">
											<em class="cleaner">Descuento del</em>
											<em class="cleaner">10%</em>
										</div>
									</div>
								</div>
								<div class="cleaner">
									<p>Para <strong>adquir tu cupón</strong> es necesario llenar la siguente información</p>
								</div>
								<div class="cleaner row">
									<div class="cleaner col-md-6 pb">
										<a id="quieroahora" href="javascript:void(0)" class="animated mybut but-style1 cleaner full">
											<span class="cleaner">Quiero mi cupón</span>
										</a>
									</div>
									<div class="cleaner col-md-6 pb">
										<a href="galeria.php" class="animated mybut but-style2 cleaner full">
											<span class="cleaner">Volver a los productos</span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>				
			</div>

			<div id="ingresardatos" class="row cleaner maxw3 ma tac " style="display:none">
				<div class="col-md-12 cleaner">
					<div class="cleaner tac">
						<h1>Necesitamos tus datos ahora</h1>
					</div>
				</div>
				<div class="col-md-12 cleaner myline2">
					<form action="" id="frm-crearcuenta" class="cleaner mb">
						<div class="row cleaner">
							<div class="col-md-6 cleaner">
								<div class="control-clone cleaner mt">
									<input type="text" class="myinput style2 cleaner" placeholder="Ingresar nombre completo" id="nombre" name="nombre">
								</div>
							</div>
							<div class="col-md-6 cleaner">
								<div class="control-clone cleaner mt">
									<input type="text" class="myinput style2 cleaner" placeholder="Ingresa correo" id="correo" name="correo">
								</div>
							</div>
						</div>
						<div class="row cleaner counterproduct product" >
							<div class="col-md-6 cleaner">
								<div class="control-clone cleaner mt">
									<input type="text" class="myinput style2 cleaner" placeholder="Contraseña (Min: 6 caracteres)" id="clave1" name="clave1">
								</div>						
							</div>
							<div class="col-md-6 cleaner">											
								<div class="control-clone cleaner mt">
									<input type="text" class="myinput style2 cleaner" placeholder="Repertir contraseña" id="clave2" name="clave2">
								</div>										
							</div>
						</div>
						<div class="row cleaner counterproduct product mt" >

							<!-- MIGX de preguntas	- eL ID de los inputs fecha, deben ser autoincrementales						 -->
							<ul id="preguntas" class="cleaner col-md-12">
								<li class="cleaner toicon row mt">
									<div class="col-md-6 tal">
										<h3>¿Cuándo es el cumpleaños de tu novia?</h3>
									</div>
									<div class="col-md-6">
										<div class="cleaner control-clone minicaja">
											<input id="cumple1" name="cumple1" type="text" class="myinput style2 dib validacion" placeholder="Fecha" >
											<i class="myicon-calendar triggerdate animated"></i>
										</div>
									</div>
								</li>
								<li class="cleaner toicon row mt">
									<div class="col-md-6 tal">
										<h3>¿Cuándo es el cumpleaños de tu novia?</h3>
									</div>
									<div class="col-md-6">
										<div class="cleaner control-clone minicaja">
											<input id="cumple2" name="cumple2" type="text" class="myinput style2 dib validacion" placeholder="Fecha" >
											<i class="myicon-calendar triggerdate animated"></i>
										</div>
									</div>
								</li>
							
							</ul>
						</div>
						<div class="row cleaner mt">
							<div class="butsubmit cleaner maxw3 tac nadaproductos ">
								<button type="submit" class="cleaner animated mybut but-style1">
									<span class="cleaner">Adquirir cupón</span>
								</button>
							</div>		
						</div>
					</form>					
				</div>
				
			</div>	
		</article>
	</section>


	<!-- ==========================================
	{{fin del contenido principal}}
	=========================================== -->

	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<?php include 'includes/footer.php' ?>	
	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
	<script>
		$(window).on({
			load: function(){				
				
			},
			resize: function(){
				
			}
		});

		$(document).ready(function(){

			$('#quieroahora').click(function(){
				$(this).toggleClass('active');
				$('#ingresardatos').slideToggle();

				$('html, body').animate({scrollTop: $('#ingresardatos').offset().top - 100 }, 1000);
			})
		
			$('#frm-crearcuenta').validate({
				rules: {		
					nombre: {
						required: true,
						sololetras: true
					},
					correo: {
						required: true,
						email: true,
						'remote': {
	                        url: 'process.php',
	                        type: 'post',
	                        data: {
	                            correo: function(e){
                                    return $('input[name="correo"]').val();
                                }
                            }
                        }
					},					
					clave1: {
						required: true,
						minlength: 6
					},
					clave2: {
						required: true,
						equalTo: '#clave1'
					}
				},
				messages: {					
					nombre: {
						required: '&nbsp;',
						sololetras: '&nbsp;'
					},
					correo: {
						required: '&nbsp;',
						email: '&nbsp;',
						'remote' : '&nbsp;'
					},					
					clave1:{
						required: '&nbsp;',
						minlength: '&nbsp;'
					},
					clave2:{
						required: '&nbsp;',
						equalTo: '&nbsp;'
					}
				},
				errorClass: "incorrecto",
				errorElement: "span",
				showErrors: function(errorMap, errorList){
				    var counterError = this.numberOfInvalids();					  
					$('.validacion').each(function(i, elem){
						if($(elem).val() == ''){
							$(elem).closest('.control-clone').addClass('error')
							counterError++;
						}else{
							$(elem).closest('.control-clone').removeClass('error')
						}
					});
					if(counterError == 1){
						$("#contenedor-alerta .texto span").html("Se presentó "+ counterError + " error.");						
					}else{
						$("#contenedor-alerta .texto span").html("Se presentaron "+ counterError + " errores.");
					}
					this.defaultShowErrors();
				},				
				unhighlight: function(element){
					$(element).closest('.control-clone').removeClass('error').addClass('success');
				},
				highlight: function(element){
					$(element).closest('.control-clone').removeClass('success').addClass('error');
				},
				invalidHandler: function (event, validator) {
		            var existeError = validator.numberOfInvalids();		            
		            if (existeError) {		            	
		            	$('body').addClass('frmError');  
		            	$("#contenedor-alerta").closest('body').addClass('showContenedor');
		            	$('button:submit').attr('disabled','disabled');
					  	setTimeout(function(){
					    	$('body').removeClass('showContenedor');
					    	$('button:submit').removeAttr('disabled');
					    }, 4900);

		            }else{
		            	$('body').removeClass('frmError');
		            }
		        },
				success: function(element){
					element.closest('body').removeClass('frmError').addClass('frmSuccess');
				},
				submitHandler: function (form) {
					console.log($(form).serialize())
					window.location.href = 'carrito-4.php';
                }
			});	
			

		})		
	</script>	
</body>
</html>