<!doctype html>
<html lang="en">
<head>
	<!-- ===================================
	{{Chunk site_seo}}
	==================================== -->
	<?php include 'includes/seo.php' ;?>
	<!-- ===================================
	{{fin Chunk site_seo}}
	==================================== -->
	<meta name="description" content="Detalle del Producto">
    <meta property="og:title" content="[[*page_title]]" /> 
    <meta property="og:url" content="[[++site_url]][[~id]]" />
    <meta property="og:image" content="[[*page_imagen]]" />
    <meta property="og:description" content="[[*page_description]]" /> 
    <meta name="twitter:image:src" content="[[*page_imagen]]"/>
	<title>[[*page_title]]</title>	
	<style>
		.maxwimg{max-width: 300px;margin: 0 auto}
	</style>
</head>
<body>
	<!-- ===================================
	{{Chunk site_header}}
	==================================== -->
	<?php include 'includes/header.php' ?>
	<!-- ===================================
	{{fin Chunk site_header}}
	==================================== -->

	<!-- ===================================
	{{Contenido principal}}
	==================================== -->
	<section id="principal" class="cleaner">		
		<article id="formularios" class="cleaner mt2 maxw">
			<div id="titulo" class="cleaner">
				<div class="row cleaner">
					<div class="col-md-12 cleaner">
						<div class="titulo cleaner">
							<div class="capa cleaner">
								<div class="lineas cleaner">
									<h2>Estas son nuestros <strong>términos y condiciones</strong></h2>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="misterminos" class="cleaner maxw3 mt mb">
				<div class="richtext cleaner">
					<h1>Tìtulo h1</h1>
					<h2>Descripción del h2</h2>
					<h3>Descripción del h3</h3>
					<h4>Descripción del h4</h4>
					<p>Párrafo</p>
					<ul>
						<li>Lista 1</li>
					</ul>
					<p><strong>Esta es la negrita</strong></p>
					<a href="">Este es un link</a>
					<p>Esta es <em>cursiva</em></p>
				</div>
			</div>	
		</article>
	
	</section>


	<!-- ==========================================
	{{fin del contenido principal}}
	=========================================== -->

	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<?php include 'includes/footer.php' ?>	
	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->

	<script>
		$(window).on({
			load: function(){				  
			},
			resize: function(){				
			}
		});

		$(document).ready(function(){          


		})
	</script>
</body>
</html>