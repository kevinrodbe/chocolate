/*====================================
* 1. Dependencias
====================================*/
var gulp = require('gulp'),
    watch = require('gulp-watch'),
    newer = require('gulp-newer'),
    concat = require('gulp-concat'), //Concatenar archivos
    uglify = require('gulp-uglify'), //Minificar archivos Js
    minifyCSS = require('gulp-minify-css'), //Minificar archivos Css    
    stylus = require('gulp-stylus'), //Compilar Stylus
    nib = require('nib'), //Nib de Stylus
    ftp = require('gulp-ftp'), //Subida de archivos
    // imagemin = require('gulp-imagemin'), //Optimizar las imagenes
    // pngquant = require('imagemin-pngquant'), //Optimizacion orientada a imágenes .png [trabaja con optipng]
    // optipng = require('imagemin-optipng'), //Optimizacion orientada a imágenes .png [trabaja con pngquant]
    // jpegoptim = require('imagemin-jpegoptim'), //Optimizacion orientada a imágenes .jpeg    
    // svgo = require('imagemin-svgo'),  //Optimizacion orientada a imágenes .svg  
    Filter = require('gulp-filter'), //Concatenar stylus al css
    notify = require('gulp-notify'); //mostrar notificaciones despues de ejecutar una tarea

/*====================================
* 2. Configuración
====================================*/
// Variables de desarrollo
var path = {
    inputstylus: 'css/privado/**/*.styl',    
    inputcss: 'css/privado/**/*.css',    
    inputjs: 'js/privado/*.js',    
    inputimg: 'imagenes/privado/*.{gif,png,jpg,svg, jpeg}',
    namejs: 'app.min.js',
    namecss: 'styles.min.css',
    outputstylus: 'css/privado/',
    outputcss: 'css/',
    outputjs: 'js/',  
    outputimg: 'imagenes/',
    uploadimg: 'imagenes/*.{gif,png,jpg,svg,jpeg}',
    uploadtmp: './*.{html,php}',
    uploadincludes: 'includes/*.php',
    uploadjsd: 'js/**/*.js',
    uploadcssd: 'css/**/*.css',
    uploadjsp: 'js/*.+(js)',
    uploadcssp: 'css/*.+(css)'
};
// Server: Variables de desarrollo
var host = '192.168.1.200',
    user = 'aaa',
    pass = '1qaz1qaz1qaz'
    port = '21',
    remotePathp = '/public_html/chocolate/',
    remotePathd = '/public_html/chocolate/desarrollo/'

/*====================================
* 3. Crear tareas
====================================*/
// Compile Stylus(no necessario)
gulp.task('stylus', function () {
    gulp.src(path.inputstylus)
        .pipe(stylus({ use: nib(), compress: true}))
        .pipe(gulp.dest(path.outputstylus));
});

//  Compile Css
gulp.task('css', function () {
    var filter = Filter('**/*.styl');
    gulp.src([
        path.inputstylus,
        path.inputcss
    ])
        .pipe(newer(path.outputcss))
        .pipe(filter)
        .pipe(stylus({ use: nib(), compress: true}))
        .pipe(filter.restore())
        .pipe(concat(path.namecss)) 
        .pipe(minifyCSS())
        .pipe(gulp.dest(path.outputcss))
        // .pipe(notify({ message: 'Generado: estilos.min.css' }));    
});

// Compile Js
gulp.task('js', function () {
    gulp.src(path.inputjs)
        .pipe(concat(path.namejs))
        .pipe(uglify())
        .pipe(gulp.dest(path.outputjs))
        // .pipe(notify({ message: 'Generado: app.min.js' }));
});

// Optimizar imagenes
gulp.task('img', function () {
    gulp.src(path.inputimg)
        .pipe(newer(path.outputimg))
        .pipe(imagemin({
            progressive: true, 
            interlaced: true, 
            svgoPlugins: [{removeViewBox: false}],
            use: [
                pngquant({quality: '65-80', speed: 4}),
                jpegoptim({max: 70})
            ]
        }))        
        .pipe(gulp.dest(path.outputimg));
        
});
// ------------------------------------------------
// [x] Subir al Servidor (Entorno de produccion)
// ------------------------------------------------
gulp.task('upload-images-p', function () {
    gulp.src(path.uploadimg)
        .pipe(ftp({
            host: host,
            user: user,
            pass: pass,
            port: port,
            remotePath: remotePathp+'imagenes'
        }));
});
gulp.task('upload-template-p', function () {
    gulp.src(path.uploadtmp)
        .pipe(ftp({
            host: host,
            user: user,
            pass: pass,
            port: port,
            remotePath: remotePathp
        }));
});
gulp.task('upload-includes-p', function () {
    gulp.src(path.uploadincludes)
        .pipe(ftp({
            host: host,
            user: user,
            pass: pass,
            port: port,
            remotePath: remotePathp+'includes'
        }));
});
gulp.task('upload-js-p', function () {
    gulp.src(path.uploadjsp)
        .pipe(ftp({
            host: host,
            user: user,
            pass: pass,
            port: port,
            remotePath: remotePathp+'js'
        }));
});
gulp.task('upload-css-p', function () {
    gulp.src(path.uploadcssp)
        .pipe(ftp({
            host: host,
            user: user,
            pass: pass,
            port: port,
            remotePath: remotePathp+'css'
        }));
});
// ------------------------------------------------
// [x] Subir al Servidor (Entorno de Desarrollo)
// ------------------------------------------------
gulp.task('upload-images-d', function () {
    gulp.src(path.uploadimg)
        .pipe(ftp({
            host: host,
            user: user,
            pass: pass,
            port: port,
            remotePath: remotePathd+'imagenes'
        }));
});
gulp.task('upload-template-d', function () {
    gulp.src(path.uploadtmp)
        .pipe(ftp({
            host: host,
            user: user,
            pass: pass,
            port: port,
            remotePath: remotePathd
        }));
});
gulp.task('upload-includes-d', function () {
    gulp.src(path.uploadincludes)
        .pipe(ftp({
            host: host,
            user: user,
            pass: pass,
            port: port,
            remotePath: remotePathd+'includes'
        }));
});
gulp.task('upload-js-d', function () {
    gulp.src(path.uploadjsd)
        .pipe(ftp({
            host: host,
            user: user,
            pass: pass,
            port: port,
            remotePath: remotePathd+'js'
        }));
});
gulp.task('upload-css-d', function () {
    gulp.src(path.uploadcssd)
        .pipe(ftp({
            host: host,
            user: user,
            pass: pass,
            port: port,
            remotePath: remotePathd+'css'
        }));
});


// Hacer Watch para Compilar css
gulp.task('ver', function(){   
    gulp.watch('./*.{html,php}', function() {
        // gulp.run('upload-d');
        gulp.run('comprimir');
    });
    gulp.watch('js/privado/*.js', function() {
        // gulp.run('upload-d');
        gulp.run('comprimir');
    });
    gulp.watch('css/privado/*.css', function() {
        // gulp.run('upload-d');
        gulp.run('comprimir');
    });
    gulp.watch('css/privado/*.styl', function() {
        // gulp.run('upload-d');
        gulp.run('comprimir');
    });
});

// Comprimir y generar archivos de desarrollo
gulp.task('comprimir', ['css', 'js']);
// Subida a Desarrollo
gulp.task('upload-d', ['comprimir', 'upload-template-d', 'upload-includes-d','upload-css-d','upload-js-d','upload-images-d']);
// Subida a Producciòn
gulp.task('upload-p', ['comprimir', 'upload-template-p', 'upload-includes-p','upload-css-p','upload-js-p','upload-images-p']);
// Ejecutar Tarea frecuente 
gulp.task('default', ['comprimir','upload-d', 'upload-p']);