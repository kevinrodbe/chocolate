<!doctype html>
<html lang="en">
<head>
	<!-- ===================================
	{{Chunk site_seo}}
	==================================== -->
	<?php include 'includes/seo.php' ;?>
	<!-- ===================================
	{{fin Chunk site_seo}}
	==================================== -->
	<meta name="description" content="Detalle del Producto">
    <meta property="og:title" content="[[*page_title]]" /> 
    <meta property="og:url" content="[[++site_url]][[~id]]" />
    <meta property="og:image" content="[[*page_imagen]]" />
    <meta property="og:description" content="[[*page_description]]" /> 
    <meta name="twitter:image:src" content="[[*page_imagen]]"/>
	<title>[[*page_title]]</title>	
</head>
<body>	
	<!-- ===================================
	{{Chunk site_header}}
	==================================== -->
	<?php include 'includes/header.php' ?>
	<!-- ===================================
	{{fin Chunk site_header}}
	==================================== -->

	<!-- ===================================
	{{Contenido principal}}
	==================================== -->
	<section id="principal" class="cleaner perfil maxw container-fluid ">
		
		<article id="mispedidos" class="cleaner row mt2 maxw2 ma mb">
			<div class="col-md-12 cleaner">		 		
				<div class="listadeproductos cleaner row ">
					<div class="col-md-4">
						<?php include 'includes/menu_perfil.php' ?>
					</div>
					<div class="col-md-8">								
						<div class="tospace cleaner">
							<div id="titulo" class="cleaner">
								<div class="row cleaner">
									<div class="col-md-12 cleaner">
										<div class="titulo cleaner">
											<div class="capa cleaner">
												<div class="lineas cleaner">
													<h2>Bienvenido a tu<strong> Perfil</strong></h2>
												</div>
											</div>
										</div> 
									</div>
								</div>
							</div>
							<div class="cleaner ">
								<div class="row cleaner">
									<div class="col-md-12 cleaner">
										<div class="row cleaner">
											<div class="col-md-6 cleaner">
												<div class="cleaner mt">
													<label class="txt" for="nombre"><i class="icon-user2"></i>&nbsp;Moises Francisco Yance Quispe</label>
												</div>													
											</div>
											<div class="col-md-6 cleaner">
												<div class="cleaner mt">
													<label class="txt" for="correo"><i class="myicon-email"></i>&nbsp;francisco10myq@gmail.com</label>
												</div>													
											</div>
										</div>
									</div>
									<div class="col-md-12 cleaner">
										<div class="row cleaner">
											<div class="col-md-6 cleaner">
												<div class="control-clone cleaner mt">
													<label class="txt" for="telefono"><i class="icon-phone"></i>&nbsp;4943721</label>
												</div>
											</div>
											<div class="col-md-6 cleaner">
												<div class=" cleaner mt">
													<label class="txt" for="telefono"><i class="icon-mobile"></i>&nbsp;959233096</label>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12 cleaner">
										<div class="row cleaner">
											<div class="col-md-6 cleaner  toicon ">
												<div class="cleaner minicaja mt">
													<label class="txt" for="telefono"><i class="myicon-cake"></i>&nbsp;16/10/1987</label>
												</div>
											</div>

											<div class="col-md-6 cleaner">
												<div class="cleaner mt">
													<label class="txt" for="telefono"><i class="icon-place"></i>&nbsp;Ate</label>
												</div>													
											</div>												
										</div>
									</div>
									
									<div class="col-md-12 cleaner mt control-clone">
										<label class="txt" for="telefono"><i class="icon-direction"></i>&nbsp;Mz S lote 1 Calle las MOras, Ceres - Ate Ref, Altura de la Av. Mertropolitana, a 5 cuadras del juzgado</label>
									</div>
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>			
		</article>	
	</section>

	<!-- ==========================================
	{{fin del contenido principal}}
	=========================================== -->

	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<?php include 'includes/footer.php' ?>	
	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	
</body>
</html>