<!doctype html>
<html lang="en">
<head>
	<!-- ===================================
	{{Chunk site_seo}}
	==================================== -->
	<?php include 'includes/seo.php' ;?>
	<!-- ===================================
	{{fin Chunk site_seo}}
	==================================== -->
	<meta name="description" content="Detalle del Producto">
    <meta property="og:title" content="[[*page_title]]" /> 
    <meta property="og:url" content="[[++site_url]][[~id]]" />
    <meta property="og:image" content="[[*page_imagen]]" />
    <meta property="og:description" content="[[*page_description]]" /> 
    <meta name="twitter:image:src" content="[[*page_imagen]]"/>
	<title>[[*page_title]]</title>
	<style>
		
	</style>
</head>
<body>
	<!-- ===================================
	{{Chunk site_header}}
	==================================== -->
	<?php include 'includes/header.php' ?>
	<!-- ===================================
	{{fin Chunk site_header}}
	==================================== -->

	<!-- ===================================
	{{Contenido principal}}
	==================================== -->
	<section id="principal" class="cleaner carritodecompras">		
		<article id="formularios" class="cleaner mt2 maxw">
			<div id="titulo" class="cleaner">
				<div class="row cleaner">
					<div class="col-md-12 cleaner">
						<div class="titulo cleaner">
							<div class="capa cleaner">
								<div class="lineas cleaner">
									<h2>¡Ingresa a tu<strong> cuenta!</strong></h2>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="micontacto" class="cleaner maxw3 mt mb">
				<div class="row cleaner">
					<div class="col-md-7 mb cleaner">
						<div class="cleaner mb">
							<div class="cleaner">
								<form action="carrito-3.php" id="frm-login" class="cleaner">
									<div class="control-clone cleaner mt">
										<input type="text" class="myinput style2 cleaner" placeholder="Ingresar E-mail" id="usuario" name="usuario">
									</div>
									<div id="verclave" class="control-clone cleaner mt">
										<input type="password" class="myinput style2 cleaner" placeholder="Contraseña (Min: 6 caracteres)" id="clave" name="clave">
										<a href="javascipt:void(0)" ><i class="icon-eye"></i></a>
									</div>
									<div class="txtrecuperar cleaner">
										<p><i class="icon-alert"></i>Este usuario no existe</p>
									</div>												
									<div class="cleaner mt ma">
										<div class="todisabled">
											<button id="ingresaramicuenta" type="submit" class="cleaner animated mybut but-style2 full">
												<span class="cleaner">Ingresar a mi cuenta</span>
											</button>
											<div class="capa"></div>
										</div>
									</div>
								</form>
							</div>
							<div class="cleaner mt tologin">
								<a id="meolvidemiclave" href="javascript:void(0)"  title="Olvidé mi contraseña">Olvidé mi contraseña <i class="icon-arrow-right"></i></a>
							</div>
							<div class="cleaner torecuperar">
								<form action="gracias_recuperar.php" id="frm-recuperar" class="cleaner" >
									<div class="control-clone cleaner mt">
										<input type="text" class="myinput style2 cleaner" placeholder="Ingresar E-mail" id="emailrecuperar" name="emailrecuperar">
									</div>
									<div class="txtrecuperar cleaner">
										<p><i class="icon-alert"></i>Este usuario no existe</p>
									</div>
									<div class="cleaner ma tac mt">
										<button id="recuperarmicuenta" type="submit" class="cleaner animated mybut but-style2 ">
											<span class="cleaner">Recuperar cuenta</span>
										</button>
									</div>
								</form>
							</div>
						</div>						
						<div class="cleaner mt">
							<a href="javascript:void(0)" class="cleaner toanimated2 mybut but-style3">
								<span class="cleaner">
									<div class="col-xs-2 cleaner tof"><i class="icon-facebook animated"></i></div>
									<div class="col-xs-10 cleaner"><em>Login con Facebook</em></div>
								</span>
							</a>
						</div>					
					</div>
					<div class="col-md-5 mb cleaner mt">
						<div class="tac ma cleaner">
							<div class="globo cleaner tac txt">
								<p>Si no tiene aún tu cuenta, puedes <a href="registro.php"><b>registrarte ahora</b></a>, también puedes hacerlo con tu cuenta de facebook.</strong></p>
							</div>
							<div class="foto cleanr">
								<img src="imagenes/laptop-3.png" alt="Contactar ahora" class="cleaner full">
							</div>
						</div>
					</div>
				</div>
			</div>		
		</article>
	
	</section>


	<!-- ==========================================
	{{fin del contenido principal}}
	=========================================== -->

	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<?php include 'includes/footer.php' ?>	
	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->

	<script>
		$(window).on({
			load: function(){				  
			},
			resize: function(){				
			}
		});

		$(document).ready(function(){
			

			$('#meolvidemiclave').click(function(){
				$('input').val('');
				$('.torecuperar').slideToggle();
				$('.todisabled').toggleClass('active');
				$('#clave').closest('.control-clone').removeClass('error');
				$('#usuario').closest('.control-clone').removeClass('error');
			})					

			$('#frm-login').validate({
				rules: {		
					usuario: {
						required: true,
						email: true,
						remote:
	                    {
							url: '_getLogin.php',
							type: "post",
							data:
							{
							 	usuario: function()
							 	{
							    	return $('#frm-login input[name="usuario"]').val();
							  	}
							},
							success: function(data)
							{
								if(data == true){
									$('#frm-login .txtrecuperar').removeAttr('style');
									console.log('true')
									return true;
								}else{
									console.log('false')
									$('#frm-login .txtrecuperar').fadeIn();
									return false;
								}
							}
	                    }
					},
					clave: {
						required: true
					}
				},
				messages: {					
					usuario: {
						required: '&nbsp;',
						email: '&nbsp;',
						remote: '&nbsp;'
					},
					clave: {
						required: '&nbsp;'
					}
				},
				errorClass: "incorrecto",
				errorElement: "span",
				showErrors: function(errorMap, errorList){
				    var counterError = this.numberOfInvalids();
					if(counterError == 1){
						$("#contenedor-alerta .texto span").html("Se presentó "+ counterError + " error.");						
					}else{
						$("#contenedor-alerta .texto span").html("Se presentaron "+ counterError + " errores.");
					}
					this.defaultShowErrors();
				},				
				unhighlight: function(element){
					$(element).closest('.control-clone').removeClass('error').addClass('success');
				},
				highlight: function(element){
					$(element).closest('.control-clone').removeClass('success').addClass('error');
				},
				invalidHandler: function (event, validator) {
		            var existeError = validator.numberOfInvalids();		            
		            if (existeError) {		            	
		            	$('body').addClass('frmError');  
		            	$("#contenedor-alerta").closest('body').addClass('showContenedor');
		            	$('#ingresaramicuenta').attr('disabled','disabled');
					  	setTimeout(function(){
					    	$('body').removeClass('showContenedor');
					    	$('#ingresaramicuenta').removeAttr('disabled');
					    }, 4900);
		            }else{
		            	$('body').removeClass('frmError');
		            }
		        },
				success: function(element){
					element.closest('body').removeClass('frmError').addClass('frmSuccess');
				},
				submitHandler: function (form) {
					console.log($(form).serialize())
					window.location.href = 'perfil.php';
                }
			});

			$('#frm-recuperar').validate({
				rules: {		
					emailrecuperar: {
						required: true,
						email: true,
						remote:
	                    {
							url: '_getRecuperarClave.php',
							type: "post",
							data:
							{
							 	emailrecuperar: function()
							 	{
							    	return $('#frm-recuperar input[name="emailrecuperar"]').val();
							  	}
							},
							success: function(data)
							{
								if(data != true){
									$('#frm-recuperar .txtrecuperar').fadeIn();
									return false;
								}else{
									$('#frm-recuperar .txtrecuperar').fadeOut();
									return true;
								}
							}
	                    }
					}
				},
				messages: {					
					emailrecuperar: {
						required: '&nbsp;',
						email: '&nbsp;',
						remote: '&nbsp;'
					}
				},
				errorClass: "incorrecto",
				errorElement: "span",
				showErrors: function(errorMap, errorList){
				    var counterError = this.numberOfInvalids();
					if(counterError == 1){
						$("#contenedor-alerta .texto span").html("Se presentó "+ counterError + " error.");	
						this.defaultShowErrors();					
					}else{
						$("#contenedor-alerta .texto span").html("Se presentaron "+ counterError + " errores.");
						this.defaultShowErrors();
					}					
				},				
				unhighlight: function(element){
					$(element).closest('.control-clone').removeClass('error').addClass('success');
				},
				highlight: function(element){
					$(element).closest('.control-clone').removeClass('success').addClass('error');
				},
				invalidHandler: function (event, validator) {
		            var existeError = validator.numberOfInvalids();		            
		            if (existeError) {		            	
		            	$('body').addClass('frmError');  
		            	$("#contenedor-alerta").closest('body').addClass('showContenedor');
		            	$('#recuperarmicuenta').attr('disabled','disabled');
					  	setTimeout(function(){
					    	$('body').removeClass('showContenedor');
					    	$('#recuperarmicuenta').removeAttr('disabled');
					    }, 4900);
		            }else{
		            	$('body').removeClass('frmError');
		            }
		        },
				success: function(element){
					element.closest('body').removeClass('frmError').addClass('frmSuccess');
				},
				submitHandler: function (form) {
					console.log($(form).serialize())
					window.location.href = 'gracias_recuperarclave.php';
                }
			});

		})
	</script>
</body>
</html>