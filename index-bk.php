<!doctype html>
<html lang="en">
<head>
	<!-- ===================================
	{{Chunk site_seo}}
	==================================== -->
	<?php include 'includes/seo.php' ;?>
	<!-- ===================================
	{{fin Chunk site_seo}}
	==================================== -->
	<meta name="description" content="[[*page_description]]">
    <meta property="og:title" content="[[*page_title]]" /> 
    <meta property="og:url" content="[[++site_url]][[~id]]" />
    <meta property="og:image" content="[[*page_imagen]]" />
    <meta property="og:description" content="[[*page_description]]" /> 
    <meta name="twitter:image:src" content="[[*page_imagen]]"/>
	<title>[[*page_title]]</title>
</head>
<body>
	<!-- ===================================
	{{Chunk site_header}}
	==================================== -->
	<?php include 'includes/header.php' ?>
	<!-- ===================================
	{{fin Chunk site_header}}
	==================================== -->

	<!-- ===================================
	{{Contenido principal}}
	==================================== -->
	<section id="principal" class="cleaner">
		<article id="seccion" class="cleaner">
			<ul id="banner" class="cleaner">
				<li class="item">
					<img src="imagenes/banner1.png" alt="">
					<div class="content cleaner maxw">
						<div class="cleaner descripcion ">
							<div class="cleaner tac animated">
								<h4>No será tu primer regalo, pero sí el que causará la mejor de las impresiones.</h4>
							</div>
							<div class="cleaner">
								<a href="javascript:void(0)" class="cleaner animated mybut but-style1">
									<span class="cleaner">Ver regalo</span>
								</a>
							</div>
						</div>
					</div>
				</li>
				<li class="item">
					<img src="imagenes/banner1.png" alt="">
					<div class="content cleaner maxw">
						<div class="cleaner descripcion ">
							<div class="cleaner tac animated">
								<h4>No será tu primer regalo, pero sí el que causará la mejor de las impresiones.</h4>
							</div>
							<div class="cleaner">
								<a href="javascript:void(0)" class="cleaner animated mybut but-style1">
									<span class="cleaner">Ver regalo</span>
								</a>
							</div>
						</div>
					</div>
				</li>
				<li class="item">
					<img src="imagenes/banner1.png" alt="">
					<div class="content cleaner maxw">
						<div class="cleaner descripcion ">
							<div class="cleaner tac animated">
								<h4>No será tu primer regalo, pero sí el que causará la mejor de las impresiones.</h4>
							</div>
							<div class="cleaner">
								<a href="javascript:void(0)" class="cleaner animated mybut but-style1">
									<span class="cleaner">Ver regalo</span>
								</a>
							</div>
						</div>
					</div>
				</li>
			</ul>
			<div id="categorias" class="cleaner">
				
			</div>
		</article>
		<article id="procesos" class="cleaner mt">
			<div class="row cleaner">
				<div class="col-md-12 cleaner">
					<div class="titulo cleaner">
						<div class="capa cleaner">
							<div class="lineas cleaner">
								<h2>Cómprea fácil  y seguro en <strong>Chocolate Express</strong></h2>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="maxw cleaner content img">
				<div class="row cleaner">
					<div class="col-md-3 linea cleaner">
						<div class="caja cleaner">
							<div class="cleaner">
								<img src="imagenes/entrarweb.png" alt="">
							</div>
							<div class="cleaner">
								<h3>1. Entra a nuestra web</h3>
							</div>
							<div class="cleaner">
								<p>Visita nuestra web para encontrar lo más finios regalos</p>
							</div>
						</div>
					</div>
					<div class="col-md-3 linea cleaner">
						<div class="caja cleaner">
							<div class="cleaner">
								<img src="imagenes/elijeproducto.png" alt="">
							</div>
							<div class="cleaner">
								<h3>2. Elige tu producto</h3>
							</div>
							<div class="cleaner">
								<p>Elige de nuestra gran variedad de productos.</p>
							</div>
						</div>
					</div>
					<div class="col-md-3 linea cleaner">
						<div class="caja cleaner">
							<div class="cleaner">
								<img src="imagenes/compraseguro.png" alt="">
							</div>
							<div class="cleaner">
								<h3>3. Compra seguro</h3>
							</div>
							<div class="cleaner">
								<p>Toda operación estará protegida por una conexión segura.</p>
							</div>
						</div>
					</div>
					<div class="col-md-3 linea cleaner">
						<div class="caja cleaner">
							<div class="cleaner">
								<img src="imagenes/entregaregalo.png" alt="">
							</div>
							<div class="cleaner">
								<h3>4. Entrega del regalo</h3>
							</div>
							<div class="cleaner">
								<p>El regalo llegará en perfectas condiciones a su destino.</p>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</article>		
	</section>
	<!-- ==========================================
	{{fin del contenido principal}}
	=========================================== -->

	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<?php include 'includes/footer.php' ?>	
	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->

	<!-- Mustache Template -Agrupaciones-->
	<script id="tmp-favoritos" type="text/template">
		{{#productos}}				
			<li class="item">
				<div class="producto cleaner">
					<div class="foto cleaner">
						<img src="{{foto}}" alt="{{nombreproducto}}">
					</div>
					<div class="bg cleaner transition">
						<div class="content cleaner">
							<div class="cleaner descripcion">
								<div class="cleaner totop">
									<div class="accion cleaner">
										<div class="cleaner opc compartir">
											<a href="javascript:void(0)" class="cleaner circle toanimated4 sharing"><i class="icon-heart animated iconito center"></i></a>
											<div class="cleaner social">
												<ul class="cleaner">
													<li class="cleaner"><a target="_blank" href="http://www.facebook.com/sharer.php?s=100&p[url]=http://aaa.dev-demanya.com/chocolate/index.php&p[title]=Compartir URLs en Face&p[summary]=Un buen recurso para aprender a compartir enlaces por código&&p[images][0]={{socialimg}}" rel="nofollow" class="toanimated1 cleaner"><i class="animated icon-facebook"></i></a></li><!-- 
													--><li class="cleaner"><a href="javascript:void(0)" class="toanimated1 cleaner"><i class="animated icon-twitter"></i></a></li><!-- 
													--><li class="cleaner"><a href="javascript:void(0)" class="toanimated1 cleaner"><i class="animated icon-googleplus"></i></a></li><!-- 
													--><li class="cleaner"><a href="javascript:void(0)" class="toanimated1 cleaner"><i class="animated icon-pinterest"></i></a></li><!-- 
													--><li class="cleaner"><a href="javascript:void(0)" class="toanimated1 cleaner"><i class="animated icon-instagram"></i></a></li><!-- 
													--><li class="cleaner"><a href="mailto:you@youraddress.com" class="toanimated1 cleaner" target="_blank"><i class="animated icon-mail"></i></a></li>
												</ul>
											</div>
										</div>
										<div class="cleaner opc">
											<a href="javascript:void(0)" title="Agregar producto" class="cleaner circle toanimated4"><i class="icon-plus iconito center animated"></i></a>
										</div>	
									</div>								
								</div>							
								<div class="cleaner tobottom">
									<a href="javascript:void(0)" class="cleaner  mybut but-style1">
										<span class="cleaner">Ver producto</span>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>				
				<div class="cleaner nombre">
					<h4>{{nombreproducto}}</h4>
				</div>
				<div class="cleaner estadistica">
					<div class="cleaner tal"><i class="icon-eye"></i>{{vistos}}</div>
					<div class="cleaner tar"><a href="javascript:void(0)" class="like"><i class="icon-like"></i>{{favoritos}}</a></div>
				</div>
				<div class="precios cleaner">
					<div class="tar cleaner">
						<span>S./ {{precioanterior}}</span>
						<em>S./ {{precionuevo}}</em>
					</div>
				</div>
			</li>
		{{/productos}}
	</script>

	<script>
		$(window).on({
			load: function(){
				// Cargar Productos favoritos
				$.getJSON('json/productos.json', function(data) {
				    var template = $('#tmp-favoritos').html();
				    var info = Mustache.to_html(template, data);
				    $('#productosfavoritos').html(info);
				}).done(function(){
					// Compartir en productos
					$('.sharing').on('click', function(){
						$(this).closest('.compartir').find('.social').toggleClass('activo')
					})
					$('.descripcion').mouseleave(function(){
						$(this).find('.social').removeClass('activo')
					})
					// Click en like
					$('.like').on('click', function(){
						$(this).toggleClass('activo')
					})
					// Galeria del pie de página
					$('.productosfavoritos').owlCarousel({
					    items:5,
					    dots:false,
					    loop:true,
					    margin:30,
					    responsive:{
					        0:{
					            items:1
					        },
					        450:{
					            items:1
					        },
					        550:{
					            items:2
					        },
					        800:{
					            items:3
					        },
					        1000:{
					            items:4
					        },
					        1200:{
					            items:5
					        }
					    }
					});
				});

			},
			resize: function(){
				var w = $(window).width();
				if(w>700){
					
				}else{
				}
			}
		});

		$(document).ready(function(){
			$('#banner').owlCarousel({
			    items:1,
			    dots:true,
			    loop:true,
			    margin:0,
			    responsive:{
			        0:{
			            items:1
			        },
			        1000:{
			            items:1
			        }
			    }
			});

			
		})
	</script>
	

	<!-- ==========================================
	=========================================== -->
</body>
</html>