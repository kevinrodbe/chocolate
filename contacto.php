<!doctype html>
<html lang="en">
<head>
	<!-- ===================================
	{{Chunk site_seo}}
	==================================== -->
	<?php include 'includes/seo.php' ;?>
	<!-- ===================================
	{{fin Chunk site_seo}}
	==================================== -->
	<meta name="description" content="Detalle del Producto">
    <meta property="og:title" content="[[*page_title]]" /> 
    <meta property="og:url" content="[[++site_url]][[~id]]" />
    <meta property="og:image" content="[[*page_imagen]]" />
    <meta property="og:description" content="[[*page_description]]" /> 
    <meta name="twitter:image:src" content="[[*page_imagen]]"/>
	<title>[[*page_title]]</title>	
</head>
<body>
	<!-- ===================================
	{{Chunk site_header}}
	==================================== -->
	<?php include 'includes/header.php' ?>
	<!-- ===================================
	{{fin Chunk site_header}}
	==================================== -->

	<!-- ===================================
	{{Contenido principal}}
	==================================== -->
	<section id="principal" class="cleaner">		
		<article id="formularios" class="cleaner mt2 maxw">
			<div id="titulo" class="cleaner">
				<div class="row cleaner">
					<div class="col-md-12 cleaner">
						<div class="titulo cleaner">
							<div class="capa cleaner">
								<div class="lineas cleaner">
									<h2>¡Queremos mantenernos en <strong>contacto !</strong></h2>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="micontacto" class="cleaner maxw3 mt mb">
				<div class="row cleaner">
					<div class="col-md-7 mb cleaner">
						<div class="cleaner ">
							<form action="gracias_registro.php" id="frm-contacto" class="cleaner">
								<div class="row cleaner">
									<div class="col-md-12 cleaner">
										<div class="control-clone cleaner mt">
											<input type="text" class="myinput style2 cleaner" placeholder="Nombre completo" id="nombres" name="nombres">
										</div>
									</div>
									<div class="col-md-12 cleaner">
										<div class="control-clone cleaner mt">
											<input type="text" class="myinput style2 cleaner" placeholder="Correo electrónico" id="correo" name="correo">
										</div>
									</div>
									<div class="col-md-12 cleaner">
										<div class="control-clone cleaner mt">
											<input type="text" class="myinput style2 cleaner" placeholder="Teléfono" id="telefono" name="telefono">
										</div>
									</div>
									<div class="cleaner col-md-12">
										<div class="cleaner control-clone mt">
											<textarea name="comentario" id="comentario" class="cleaner mytextarea style1" placeholder="comentario"></textarea>
										</div>
				                    </div>
				                    <div class="cleaner mt ma tac col-md-12">
										<button type="submit" class="animated mybut but-style1">
											<span class="cleaner">Enviar mi consulta</span>
										</button>
									</div>
								</div>			
							</form>
						</div>
					</div>
					<div class="col-md-5 mb cleaner mt">
						<div class="tac ma cleaner">
							<div class="globo cleaner tac txt">
								<p>Coméntanos cualquier duda que tengas o comunícate llamándonos al: <strong>(01) 671-5728</strong></p>
							</div>
							<div class="foto cleanr">
								<img src="imagenes/laptop-1.svg" alt="Contactar ahora" class="cleaner full">
							</div>
						</div>
					</div>
				</div>
			</div>		
		</article>
	
	</section>


	<!-- ==========================================
	{{fin del contenido principal}}
	=========================================== -->

	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<?php include 'includes/footer.php' ?>	
	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->

	<script>
		$(window).on({
			load: function(){				  
			},
			resize: function(){				
			}
		});

		$(document).ready(function(){          

			$('#frm-contacto').validate({
				rules: {		
					nombres: {
						required: true,
						sololetras: true
					},
					correo: {
						required: true,
						email: true
					},
					telefono: {
						required: true,
						digits: true
					},
					comentario:{
						required: true
					}
				},
				messages: {					
					nombres: {
						required: '&nbsp;',
						sololetras: '&nbsp;'
					},
					correo: {
						required: '&nbsp;',
						email: '&nbsp;'
					},
					telefono: {
						required: '&nbsp;',
						digits: '&nbsp;'
					},
					comentario:{
						required: '&nbsp;'
					}
				},
				errorClass: "incorrecto",
				errorElement: "span",
				showErrors: function(errorMap, errorList){
				    var counterError = this.numberOfInvalids();
					if(counterError == 1){
						$("#contenedor-alerta .texto span").html("Se presentó "+ counterError + " error.");	
						this.defaultShowErrors();					
					}else{
						$("#contenedor-alerta .texto span").html("Se presentaron "+ counterError + " errores.");
						this.defaultShowErrors();
					}					
				},				
				unhighlight: function(element){
					$(element).closest('.control-clone').removeClass('error').addClass('success');
				},
				highlight: function(element){
					$(element).closest('.control-clone').removeClass('success').addClass('error');
				},
				invalidHandler: function (event, validator) {
		            var existeError = validator.numberOfInvalids();		            
		            if (existeError) {		            	
		            	$('body').addClass('frmError');  
		            	$("#contenedor-alerta").closest('body').addClass('showContenedor');
		            	$('button:submit').attr('disabled','disabled');
					  	setTimeout(function(){
					    	$('body').removeClass('showContenedor');
					    	$('button:submit').removeAttr('disabled');
					    }, 4900);
		            }else{
		            	$('body').removeClass('frmError');
		            }
		        },
				success: function(element){
					element.closest('body').removeClass('frmError').addClass('frmSuccess');
				},
				submitHandler: function (form) {
					console.log($(form).serialize())
                }
			});	

		})
	</script>
</body>
</html>