<!doctype html>
<html lang="en">
<head>
	<!-- ===================================
	{{Chunk site_seo}}
	==================================== -->
	<?php include 'includes/seo.php' ;?>
	<!-- ===================================
	{{fin Chunk site_seo}}
	==================================== -->
	<meta name="description" content="Detalle del Producto">
    <meta property="og:title" content="[[*page_title]]" /> 
    <meta property="og:url" content="[[++site_url]][[~id]]" />
    <meta property="og:image" content="[[*page_imagen]]" />
    <meta property="og:description" content="[[*page_description]]" /> 
    <meta name="twitter:image:src" content="[[*page_imagen]]"/>
	<title>[[*page_title]]</title>		
</head>
<body>	
	<!-- ===================================
	{{Chunk site_header}}
	==================================== -->
	<?php include 'includes/header.php' ?>
	<!-- ===================================
	{{fin Chunk site_header}}
	==================================== -->

	<!-- ===================================
	{{Contenido principal}}
	==================================== -->
	<section id="principal" class="cleaner perfil maxw container-fluid">
		
		<article id="mispedidos" class="cleaner row mt2 maxw2 ma mb">
			<div class="col-md-12 cleaner">
		 		
				<div class="listadeproductos cleaner row ">
					<div class="col-md-4">
						<?php include 'includes/menu_perfil.php' ?>
					</div>
					<div class="col-md-8">								
						<div class="tospace cleaner">
							<div id="titulo" class="cleaner">
								<div class="row cleaner">
									<div class="col-md-12 cleaner">
										<div class="titulo cleaner">
											<div class="capa cleaner">
												<div class="lineas cleaner">
													<h2>Editar mis<strong> datos</strong></h2>
												</div>
											</div>
										</div> 
									</div>
								</div>
							</div>
							<div class="cleaner ">
								<form action="perfil.php" id="frm-perfil" class="cleaner" action="perfil.php">
									<div class="row cleaner">
										<div class="col-md-12 cleaner">
											<div class="control-clone cleaner mt tar">
												<a id="agregarfecha" href="javascript:void(0)" class="animated mybut but-style2">
													<span class="cleaner">Agregar una fecha importante</span>
												</a>		
											</div>
										</div>
										<div class="cleaner col-md-12">
											<!-- Lista de las fechas importantes -->
											<div id="listadefechasimportantes"  class="row cleaner">
												<div id="cont-cumple1" class="mifecha col-md-12 cleaner">
													<div class="cleaner datecontent">
														<div class="cleaner tar todelete">
															<a href="javascript:void(0)" class="delete toanimated1 txteliminar"><span>Eliminar</span><i class="myicon-delete animated"></i></a>
														</div>
														<div class="cleaner ">														
															<div class="row cleaner">
																<div class="col-md-6 cleaner control-clone pb">
																	<div class="cleaner minicaja">
																		<input id="cumple1" name="cumple1" type="text" class="myinput style2 dib validacion" placeholder="Fecha de cumpleaños" value="">
																		<i class="myicon-calendar triggerdate animated"></i>
																	</div>
																</div>
																<div class="col-md-6 cleaner control-clone pb">
																	<input id="motivo2" name="motivo2" type="text" class="myinput style2 cleaner validacion" placeholder="Escriba el nombre del motivo">
																</div>
															</div>
														</div>
													</div>
												</div>
												<div id="cont-cumple2" class="mifecha col-md-12 cleaner">
													<div class="cleaner datecontent">
														<div class="cleaner tar todelete">
															<a href="javascript:void(0)" class="delete toanimated1 txteliminar"><span>Eliminar</span><i class="myicon-delete animated"></i></a>
														</div>
														<div class="cleaner ">														
															<div class="row cleaner">
																<div class="col-md-6 cleaner control-clone pb">
																	<div class="cleaner minicaja">
																		<input id="cumple2" name="cumple2" type="text" class="myinput style2 dib validacion" placeholder="Fecha de cumpleaños" value="">
																		<i class="myicon-calendar triggerdate animated"></i>
																	</div>
																</div>
																<div class="col-md-6 cleaner control-clone pb">
																	<input id="motivo2" name="motivo2" type="text" class="myinput style2 cleaner validacion" placeholder="Escriba el nombre del motivo">
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<!-- end -->
											<div id="cuidadito" class="txt cleaner mt" style="display:none">
												<p><i class="icon-comment"></i>&nbsp;&nbsp;Es necesario tener por lo menos, 1 fecha importante</p>
											</div>
										</div>

										<div class="col-md-12 cleaner">
											<div class="cleaner mt ma tac ">
												<button type="submit" class="cleaner animated mybut but-style1">
													<span class="cleaner">Actualizar información</span>
												</button>
											</div>
										</div>
										
									</div>									
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</article>	
	</section>

	<!-- ==========================================
	{{fin del contenido principal}}
	=========================================== -->

	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<?php include 'includes/footer.php' ?>	
	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<script>
		$(window).on({
			load: function(){							
			},
			resize: function(){				
			}
		});

		$(document).ready(function(){			
			//Agregar Nueva fecha
			$('#agregarfecha').on('click', function(){				
				var total = $('#listadefechasimportantes>div').length;
				crearFecha(total+1)
			});
						
			$('#frm-perfil').on('submit',function(event){
				var error = 0;
				$('.validacion').each(function(i, elem){
					if($(elem).val() == ''){
						$(elem).closest('.control-clone').addClass('error')
						error++;
					}else{
						$(elem).closest('.control-clone').removeClass('error')
					}					
				});
				if(error > 0){
					return false
				}
			});

			$('[id^=motivo]').keyup(function(){
				if($(this).val() == ''){
					$(this).closest('.control-clone').addClass('error')
				}else{
					$(this).closest('.control-clone').removeClass('error')
				}
			})

			$('.delete').click(function(){
				var id = $(this).closest('.mifecha').attr('id')
				deleteDate(id);			
			});
		});


		function crearFecha(id){
			var html = '';
				html += '<div id="cont-cumple'+id+'" class="mifecha col-md-12 cleaner">';
				html += '<div class="cleaner datecontent">';
				html += '<div class="cleaner tar todelete">';
				html += '<a href="javascript:void(0)" class="delete toanimated1 txteliminar"><span>Eliminar</span><i class="myicon-delete animated"></i></a>';
				html += '</div>';
				html += '<div class="cleaner ">';
				html += '<div class="row cleaner">';
				html += '<div class="col-md-6 cleaner control-clone">';
				html += '<div class="cleaner minicaja">';
				html += '<input id="cumple'+id+'" name="cumple'+id+'" type="text" class="myinput style2 dib validacion" placeholder="Fecha de cumpleaños" value="">';
				html += '<i class="myicon-calendar triggerdate animated"></i>';
				html += '</div>';
				html += '</div>';
				html += '<div class="col-md-6 cleaner control-clone">';
				html += '<input id="motivo'+id+'" name="motivo'+id+'" type="text" class="myinput style2 cleaner validacion" placeholder="Escriba el nombre del motivo">';
				html += '</div>';
				html += '</div>';
				html += '</div>';
				html += '</div>';
				html += '</div>';

			$('#listadefechasimportantes .mifecha:last').after(html);
			$("input[name*='cumple']").datetimepicker({				
				lang:'es',
				timepicker:false,
				format:'d/m/Y',
				formatDate:'d/m/Y',
				maxDate: 0			
			});

			$('#motivo'+id).keyup(function(){
				if($(this).val() == ''){
					$(this).closest('.control-clone').addClass('error')
				}else{
					$(this).closest('.control-clone').removeClass('error')
				}
			})
			$('.delete').click(function(){
				var id = $(this).closest('.mifecha').attr('id')
				deleteDate(id);			
			});

			$('#listadefechasimportantes').each(
				function() {
					var mifecha = ($(this).find('.mifecha')).length;
					if(mifecha>1){
						$('#cuidadito').fadeOut();
					}
				}
		    );
		}

		function deleteDate(id){
			$('#listadefechasimportantes').each(
				function() {
					var mifecha = ($(this).find('.mifecha')).length;
					if(mifecha>1){
						$('#'+id).closest('.mifecha').fadeOut('500',function(){
							$('#'+id).remove();
						})						
					}else{
						$('#cuidadito').fadeIn();
					}
				}
		    );
		}



	</script>	
</body>
</html>