<!doctype html>
<html lang="en">
<head>
	<!-- ===================================
	{{Chunk site_seo}}
	==================================== -->
	<?php include 'includes/seo.php' ;?>
	<!-- ===================================
	{{fin Chunk site_seo}}
	==================================== -->
	<meta name="description" content="Detalle del Producto">
    <meta property="og:title" content="[[*page_title]]" /> 
    <meta property="og:url" content="[[++site_url]][[~id]]" />
    <meta property="og:image" content="[[*page_imagen]]" />
    <meta property="og:description" content="[[*page_description]]" /> 
    <meta name="twitter:image:src" content="[[*page_imagen]]"/>
	<title>[[*page_title]]</title>	
</head>
<body>
	<!-- ===================================
	{{Chunk site_header}}
	==================================== -->
	<?php include 'includes/header.php' ?>
	<!-- ===================================
	{{fin Chunk site_header}}
	==================================== -->

	<!-- ===================================
	{{Contenido principal}}
	==================================== -->
	<section id="principal" class="cleaner">		
		<article id="formularios" class="cleaner mt2 maxw">
			<div id="titulo" class="cleaner">
				<div class="row cleaner">
					<div class="col-md-12 cleaner">
						<div class="titulo cleaner">
							<div class="capa cleaner">
								<div class="lineas cleaner">
									<h2>¡Queremos mantenernos en <strong>contacto !</strong></h2>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="micontacto" class="cleaner maxw3 mt mb">
				<div class="row cleaner">
					<div class="col-md-7 mb cleaner">
						<div class="cleaner ">
							<form action="gracias_registro.php" id="frm-contacto" class="cleaner">
								<div class="row cleaner">
									<div class="col-md-12 cleaner">
										<div class="control-clone cleaner mt">
											<input type="text" class="myinput style2 cleaner" placeholder="Nombre completo" id="nombres" name="nombres">
										</div>
									</div>
									<div class="col-md-12 cleaner">
										<div class="control-clone cleaner mt">
											<input type="text" class="myinput style2 cleaner" placeholder="Correo electrónico" id="correo" name="correo">
										</div>
									</div>
									<div class="col-md-12 cleaner">									
										<div id="verclave" class="control-clone cleaner mt">
											<input type="password" class="myinput style2 cleaner" placeholder="Contraseña (Min: 6 caracteres)" id="clave" name="clave">
											<a href="javascipt:void(0)" ><i class="icon-eye"></i></a>
										</div>
									</div>
									<div class="col-md-12 cleaner">
										<div class="control-clone cleaner mt">
											<div class="content-check cleaner">
												<div class="cleaner">
													<input type="checkbox" name="terminos" id="terminos" value="1" class="check">
													<label for="terminos"></label>
												</div>
												<div class="cleaner">
													<label for="terminos">Acepto los <a href="terminos.php" target="_blank">términos y condiciones</a></label>
												</div>
											</div>
										</div>
									</div>
				                    <div class="cleaner mt ma tac col-md-12">
										<button type="submit" class="animated mybut but-style1">
											<span class="cleaner">¡Registrarme ya!</span>
										</button>
									</div>
								</div>			
							</form>
						</div>
					</div>
					<div class="col-md-5 mb cleaner mt">
						<div class="tac ma cleaner">
							<div class="globo cleaner tac txt">
								<p>Cupones, descuentos y promociones, todo al momento de registrarte. Incluso, no volverás a olvidar un día especial gracias a <strong>nuestro calendario digital.</strong></p>
							</div>
							<div class="foto cleanr">
								<img src="imagenes/laptop-2.svg" alt="Contactar ahora" class="cleaner full">
							</div>
						</div>
					</div>
				</div>
			</div>		
		</article>
	
	</section>


	<!-- ==========================================
	{{fin del contenido principal}}
	=========================================== -->

	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<?php include 'includes/footer.php' ?>	
	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->

	<script>
		$(window).on({
			load: function(){				  
			},
			resize: function(){				
			}
		});

		$(document).ready(function(){
			

			// Para no cambiar, cuando ya está mostrado el mensaje de error
			$("#terminos").change(function(){
				if($('button:submit').attr('disabled')){
				}else{
					if($(this).is(':checked')){
						$('button:submit').removeAttr('disabled');
					}
				}
			})

			$('#frm-contacto').validate({
				rules: {		
					nombres: {
						required: true,
						sololetras: true
					},
					correo: {
						required: true,
						email: true
					},
					clave:{
						required: true,
						minlength: 6
					}
				},
				messages: {					
					nombres: {
						required: '&nbsp;',
						sololetras: '&nbsp;'
					},
					correo: {
						required: '&nbsp;',
						email: '&nbsp;'
					},
					clave:{
						required: '&nbsp;',
						minlength: '&nbsp;'
					}
				},
				errorClass: "incorrecto",
				errorElement: "span",
				showErrors: function(errorMap, errorList){
				    var counterError = this.numberOfInvalids();
					if(counterError == 1){
						$("#contenedor-alerta .texto span").html("Se presentó "+ counterError + " error.");	
						this.defaultShowErrors();					
					}else{
						$("#contenedor-alerta .texto span").html("Se presentaron "+ counterError + " errores.");
						this.defaultShowErrors();
					}					
				},				
				unhighlight: function(element){
					$(element).closest('.control-clone').removeClass('error').addClass('success');
				},
				highlight: function(element){
					$(element).closest('.control-clone').removeClass('success').addClass('error');
				},
				invalidHandler: function (event, validator) {
		            var existeError = validator.numberOfInvalids();		            
		            if (existeError) {		            	
		            	$('body').addClass('frmError');  
		            	$("#contenedor-alerta").closest('body').addClass('showContenedor');
		            	$('button:submit').attr('disabled','disabled');
		            	console.log('if');
					  	setTimeout(function(){
					    	$('body').removeClass('showContenedor');
					    	$('button:submit').removeAttr('disabled');
					    	console.log('settime')
					    }, 4900);
		            }else{
		            	$('body').removeClass('frmError');
		            	console.log('else2');
		            }
		        },
				success: function(element){
					element.closest('body').removeClass('frmError').addClass('frmSuccess');				
				},
				submitHandler: function (form) {					
					if($('#terminos').is(':checked')){						
						$('button:submit').removeAttr('disabled');
						console.log($(form).serialize())
						window.location.href = 'gracias.php';
						return true;
					}else{
						$("#contenedor-alerta").closest('body').addClass('showContenedor');
						$("#contenedor-alerta .texto span").html("Es necesario aceptar los términos y políticas");	
		            	$('button:submit').attr('disabled','disabled');
					  	setTimeout(function(){
					    	$('body').removeClass('showContenedor');
					    	$('button:submit').removeAttr('disabled');				    	
					    }, 4900);
					    return false;
					}					
                }
			});	

		})
	</script>
</body>
</html>