<base href="[[++site_url]]" />
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="[[++site_name]]">
<meta name="robots" content="all, index, follow">
<meta name="googlebot" content="all, index, follow">
<meta name="google" content="notranslate" />
<meta name="google-site-verification" content="[[+c.g_verification]]" />
<meta name=apple-mobile-web-app-capable content=yes>

<!-- OG -->
<meta property="og:site_name" content="[[++site_name]]" />
<meta property="og:type" content="website" />
<meta name="twitter:site" content="@[[++site_name]]"/>
<meta name="twitter:creator" content="@[[++site_name]]">
<meta name="twitter:card" content="summary_large_image"/>

<!-- Favicons -->
<link rel="shortcut icon" href="imagenes/favicons/favicon.ico" type="image/x-icon">
<link rel="icon" href="imagenes/favicons/favicon.ico" type="image/x-icon">
<!-- chocolate\imagenes\favicon -->
<link rel="apple-touch-icon" sizes="57x57" href="[[+c.touch_icon_iphone]]" />
<link rel="apple-touch-icon" sizes="72x72" href="[[+c.touch_icon_ipad]]" />
<link rel="apple-touch-icon" sizes="114x114" href="[[+c.touch_icon_iphone_retina]]" />
<link rel="apple-touch-icon" sizes="144x144" href="[[+c.touch_icon_ipad_retina]]" />
<link rel="apple-touch-icon" sizes="460x320" href="[[+c.apple_touchstartup_image]]" />   

<!-- Styles -->
<link rel="stylesheet" href="css/styles.min.css">
