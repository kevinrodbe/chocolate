<div id="panel" class="cleaner">
	<div id="foto" class="cleaner">
		<img src="fotos/usuario.jpg" alt="">
	</div>
	<div class="menuperfil cleaner mt mb">
		<ul class="cleaner">
			<li><a href="perfil.php" class="toanimated3 cleaner"><i class="animated icon-user"></i>&nbsp;&nbsp;Perfil de usuario</a></li>
			<li><a href="perfil_fechas.php" class="toanimated3 cleaner myline2"><i class="animated icon-calendar"></i>&nbsp;&nbsp;Fechas importantes</a></li>
			<li><a href="perfil_compras.php" class="toanimated3 cleaner myline2"><i class="animated icon-cart"></i>&nbsp;&nbsp;Productos comprados</a></li>
			<li><a href="perfil_vistos.php" class="toanimated3 cleaner myline2"><i class="animated icon-eye"></i>&nbsp;&nbsp;Productos vistos</a></li>
			<li><a href="perfil_editar.php" class="toanimated3 cleaner myline2"><i class="animated icon-pen"></i>&nbsp;&nbsp;Editar mis datos</a></li>
			<li><a href="perfil_foto.php" class="toanimated3 cleaner myline2"><i class="animated icon-picture"></i>&nbsp;&nbsp;Foto de perfil</a></li>
			<li><a href="perfil_clave.php" class="toanimated3 cleaner myline2"><i class="animated icon-key"></i>&nbsp;&nbsp;Contraseña</a></li>
			<li><a href="javascript:void(0)" class="toanimated3 cleaner myline2"><i class="animated icon-exit"></i>&nbsp;&nbsp;Cerrar sesión</a></li>
		</ul>
	</div>
</div>