<ul id="productosfavoritos" class="tocarro cleaner productosfavoritos cleanresponsive col-md-12 opacar">
	<!-- Carga de la galeria de productos -->			
</ul>
<script id="tmp-favoritos" type="text/template">
	{{#productos}}				
		<li class="item">
			<div class="cleaner pr">
				<div class="etiqueta cleaner">
					<div class="estado {{estado}}">{{estado}}</div>
				</div>
				{{!La clase "added", se agrega a .producto , cuando el producto se agregó correctamente}}
				<div class="producto cleaner">
					<div class="foto cleaner">
						<img src="{{foto}}" alt="{{nombreproducto}}">
					</div>
					<div class="bg cleaner transition">
						<div class="content cleaner">
							<div class="cleaner descripcion">
								<div class="cleaner totop">
									{{!Div antes que el producto sea agregado}}
									<div class="accion cleaner agregarproducto">
										<div class="opc cleaner">
											<a href="javascript:void(0)" title="Agregar producto" class="cleaner circle toanimated4 agregaralcarrito"><i class="icon-plus iconito center animated"></i></a>
										</div>
									</div>
									{{!DIV despues que el producto se agregó}}
									<div class="productoagregado cleaner">
										<a href="carrito-1.php" class=" mybut but-style1 cleaner">
											<span class="cleaner">Ir al carrito</span>
										</a>
									</div>
								</div>							
								<div class="cleaner tobottom">
									<a href="producto.php" class=" mybut but-style1 cleaner">
										<span class="cleaner">Ver producto</span>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="cleaner text favoritos-but-responsive hidden">
					<a href="producto.php" class="cleaner  mybut but-style1">
						<span class="cleaner">Ver producto</span>
					</a>
				</div>
				<div class="cleaner nombre">
					<h4>{{nombreproducto}}</h4>
				</div>
				<div class="cleaner estadistica">
					<div class="cleaner tal"><i class="icon-eye"></i>{{vistos}}</div>
					<div class="cleaner tar"><a href="javascript:void(0)" class="like"><i class="icon-like animated"></i>{{favoritos}}</a></div>
				</div>
				<div class="precios cleaner myline2">
					<div class="tar cleaner">
						<span>S./ {{precioanterior}}</span>
						<em>S./ {{precionuevo}}</em>
					</div>
				</div>
			</div>
		</li>
	{{/productos}}
</script>