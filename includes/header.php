<!-- Contenedor de la alerta -->
<div id="contenedor-alerta" class="cleaner">
	<div class="progress cleaner"></div>
	<div class="texto cleaner">
		<i class="icon-error icono"></i>
		<span><!-- Aquí va el texto de alerta --></span>
	</div>
</div>

<!-- Anuncio -->
<button id="anuncio" class="cleaner transition" type="button">
	<div class="mitabla cleaner">
		<div class="cleaner caja1">
			<div class="cleaner phone transition"><i class="icon-phone"></i></div>
		</div>
		<div class="cleaner caja2">
			<div class="cleaner"><h6>Atención al cliente</h6> </div>
			<div class="cleaner"><p>265-0352 / 586-8452</p></div>
		</div>
	</div>
</button>

<header id="cabecera" class="cleaner bg1">	
	<section class="cleaner maxw noresponsive">
		<div class="cleaner row">
			<div class="col-md-3 cleaner">
				<div id="logo" class="cleaner">
					<a href="./" class="cleaner"><img src="imagenes/logo.png" alt="Chocolate Express"></a>
				</div>
			</div>
			<div class="col-md-9 cleaner">
				<div class="cleaner tar">
					<div class="cleaner toderecha">
						<nav id="menu" class="cleaner ib">
							<li class="ib cleaner"><a class="cleaner myline1 " href="galeria.php">Regalos</a></li><!--
							--><li class="ib cleaner"><a class="cleaner myline1 " href="contacto.php">Contacto</a></li><!--
							--><li class="ib cleaner"><a class="cleaner myline1 " href="nosotros.php">Nosotros</a></li>
						</nav>
						<div class="cleaner ib">
							<div class="cleaner ib vab touser">
								<a class="cleaner user" href="javascript:void(0)">
									<i class="myicon-user iconito"></i>									
								</a>
								<div class="tooltip cleaner" >
									<div class="mitabla cleaner tologin">
										<div class="cleaner"><a href="registro.php" class="cleaner links">Registro</a></div>
										<div class="cleaner"><a href="login.php" class="cleaner links">Login</a></div>											
									</div>
								</div>							
							</div>
							<div class="cleaner ib">
								<form id="frm-buscar" class="cleaner tobuscar">
									<input class="myinput style1" type="search" placeholder="buscar..." name="buscar" id="buscar">
									<button class="mybut butbuscar" type="submit"><i class="icon-search"></i></button>
								</form>
							</div>							
						</div>
						<div class="liston cleaner ib vat">
							<a href="carrito-1.php" class="cleaner">
								<div class="bg cleaner">
									<div class="cleaner canasta"><img src="imagenes/carrito.png" alt="CARRITO" rel="no-follow" class="animated"></div>
									<div class="cleaner cantidad"><em>0</em></div>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="cleaner responsive">
		<div class="cleaner info">
			<div class="cleaner tal links">
				<a href="login.php" alt="login">Entrar</a>
				<a href="registro.php" alt="registrar">Registrar</a>
			</div>
			<div class="cleaner tar">
				<div class="cleaner canasta"><span class="count">0</span><a href="carrito-1.php" class="canastita
				"><img src="imagenes/carrito.png" alt="" rel="no-follow"></a></div>
			</div>
		</div>
		<div class="cleaner logito">
			<a href="./"><img src="imagenes/logo.png" alt="Chocolate Express"></a>
		</div>
		<div class="cleaner row">
			<div class="col-md-6 cleaner">
				<a href="javascript:void(0)" id="menuresponsive" class="cleaner">
					<div id="menu-toggle" class="cleaner"><div class="icon toggle-icon"><i></i><i></i><i></i></div></div>				
				</a>
				<ul id="opciones" class="cleaner">
					<li class="myline4"><a href="galeria.php" class="cleaner">Regalos</a></li>					
					<li class="myline4"><a href="contacto.php" class="cleaner">Contacto</a></li>					
					<li class="myline4"><a href="nosotros.php" class="cleaner">Nosotros</a></li>				
				</ul>
			</div>
		</div>


		<div class="buscador cleaner">
			<form id="frm-buscar" class="cleaner tobuscar">
				<input class="myinput style1" type="search" placeholder="buscar...">
				<button class="mybut butbuscar transition" type="submit"><i class="icon-search"></i></button>
			</form>
		</div>
	</section>
</header>