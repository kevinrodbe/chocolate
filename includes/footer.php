<footer id="piedepagina" class="cleaner">
	<article id="suscripcion" class="cleaner bg1">
		<div class="contentgeneral cleaner maxw">
			<div class="content1 cleaner">
				<div class="cleaner">
					<h5>¿Deseas futuros descuentos?</h5>
					<p>¿O qué te recordemos esas fechas especiales?</p>
				</div>				
				<div class="cleaner">
					<form id="frm-sucripcion" class="cleaner tosuscripcion pr">
						<div class="cleaner control-clone onlysuscripcion">
							<input class="myinput style1" type="text" placeholder="Correo electrónico" id="correo" name="correo">
							<button class="mybut butbuscar" type="submit">Enviar</button>
						</div>					
					</form>
				</div>
				<div class="llamarcel cleaner">
					<div class="cleaner">
						<h5>Atención al cliente</h5>
					</div>
					<div class="cleaner">
						<div class="mitabla cleane">
							<div class="cleaner"><a href="tel:+01959273096"><i class="icon-phone"></i>&nbsp;959-273-096</a></div>
							<div class="cleaner"><a href="tel:+01959273096"><i class="icon-phone"></i>&nbsp;959-2730-96</a></div>
						</div>
					</div>
				</div>
			</div>			
			<div class="content2 cleaner myline4">
				<ul class="cleaner">
					<li class="cleaner"><a href="galeria.php" class="cleaner">Regalos</a></li>
					<li class="cleaner"><a href="nosotros.php" class="cleaner">Nosotros</a></li>
					<li class="cleaner"><a href="contacto.php" class="cleaner">Contacto</a></li>
					<li class="cleaner"><a href="terminos.php" class="cleaner">Términos y políticas</a></li>
					<li class="cleaner"><a id="verlibrodereclamaciones" href="javascript:void(0)" class="cleaner">Libro de reclamaciones</a></li>
				</ul>
			</div>
			<div class="content3 cleaner myline4">
				<ul id="redes-sociales" class="cleaner">
					<li class="cleaner"><a href="javascript:void(0)" class="cleaner transition"><i class="icon-facebook"></i></a></li>
					<li class="cleaner"><a href="javascript:void(0)" class="cleaner transition"><i class="icon-twitter"></i></a></li>
					<li class="cleaner"><a href="javascript:void(0)" class="cleaner transition"><i class="icon-googleplus"></i></a></li>
					<li class="cleaner"><a href="javascript:void(0)" class="cleaner transition"><i class="icon-pinterest"></i></a></li>
					<li class="cleaner"><a href="javascript:void(0)" class="cleaner transition"><i class="icon-instagram"></i></a></li>
				</ul>
			</div>
			<div class="content4 myline4 cleaner">
				<div class="cleaner mitabla">
					<div class="cleaner"><p>Trabajamos con los siguientes bancos:</p></div>
					<div class="cleaner"><img src="imagenes/bancos.png" alt="bancos"></div>
				</div>
			</div>
			<div class="content5 cleaner myline4">
				<div class="row cleaner">
					<div class="col-md-6 cleaner">
						<div id="manyape" class="cleaner transition">
							<div class="copy cleaner myhover-toico">
								<a href="http://manya.pe" id="logomanya" target="_blank"><img src="imagenes/manya.svg" alt="Manya" title="Manya" class="animated"></a>			
							</div>
							<div class="copy cleaner">
								<a href="http://manya.pe/nuestros-trabajos.html" target="_blank" class="manya-name">Manya.pe</a>
							</div>
							<div class="copy cleaner" style="color:#533e33">&nbsp;|&nbsp;</div>
							<div class="copy cleaner">
								<a href="http://www.manya.pe/blog.html">&nbsp;Marketing Digital</a>				
							</div>
						</div>
					</div>
					<div class="col-md-6 cleaner">
						<div class="derechos cleaner tar">
							<p>2014 © Chocolatexpress. Todos los derechos reservados</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
</footer>

<!-- ===================================
{{Chunk site_js_general}}
==================================== -->
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="js/app.min.js"></script>
                                                                        
<!-- ==========================================
{{fin de Chunk site_js_general}}
=========================================== -->

<!-- Pop , sólo para el libro de Reclamaciones -->
<div id="popup" class="cleaner librodereclamaciones">
	<div class="contenido cleaner">
		<a class="close up cleaner transition" href="javascript:void(0)"><i class="icon-circle-cross"></i></a>
		<div class="cleaner micaja">
			<div class="cleaer">
				<h6>Libro de reclamaciones</h6>
			</div>
			<div class="cleaner desc">
				<p>Si tuviste algún problema en nuestra página web o con nuestro servicio. No dudes en contarnos.</p>
			</div>
			<div class="cleaner">
				<form action="" id="frm-reclamaciones" class="cleaner">
					<div class="cleaner">
						<div class="cleaner control-clone space">
							<input type="text" class="cleaner myinput style2" name="nombre" id="nombre" placeholder="Nombre">
						</div>
						<div class="cleaner control-clone space">
							<input type="text" class="cleaner myinput style2" name="correo" id="correo" placeholder="Correo">
						</div>
						<div class="cleaner control-clone space">
							<textarea type="text" class="cleaner mytextarea style1" name="comentario" id="comentario" placeholder="Comentario"></textarea>
						</div>
					</div>
					<div class="cleaner myline2 tobut">
						<button type="submit" class="clener mybut but-style1">
							<span class="cleaner">Enviar</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>		
</div>

<!-- POpup que se muestra, cuando se agrega un producto al carrito -->
<div id="popup" class="cleaner compraexitosa">
	<div class="contenido cleaner">
		<a class="close up cleaner transition" href="javascript:void(0)"><i class="icon-circle-cross"></i></a>
		<div class="cleaner micaja">
			<div class="compraexitosa cleaner">
				<div class="cleaner">
					<p>Estas a un click de obtener tu regalo</p>
				</div>
				<div class="cleaner mt">
					<a href="compra-paso1.php" class="cleaner mybut but-style1 ">
						<span class="cleaner">Terminar mi compra</span>
					</a>
				</div>
				<div class="cleaner mt">
					<a href="javascript:void(0)" class="cleaner mybut but-style2 close">
						<span class="cleaner">Quiero seguir comprando</span>
					</a>
				</div>
			</div>
		</div>
	</div>		
</div>
