<!doctype html>
<html lang="en">
<head>
	<!-- ===================================
	{{Chunk site_seo}}
	==================================== -->
	<?php include 'includes/seo.php' ;?>
	<!-- ===================================
	{{fin Chunk site_seo}}
	==================================== -->
	<meta name="description" content="Detalle del Producto">
    <meta property="og:title" content="[[*page_title]]" /> 
    <meta property="og:url" content="[[++site_url]][[~id]]" />
    <meta property="og:image" content="[[*page_imagen]]" />
    <meta property="og:description" content="[[*page_description]]" /> 
    <meta name="twitter:image:src" content="[[*page_imagen]]"/>
	<title>[[*page_title]]</title>	
</head>
<body>	
	<!-- ===================================
	{{Chunk site_header}}
	==================================== -->
	<?php include 'includes/header.php' ?>
	<!-- ===================================
	{{fin Chunk site_header}}
	==================================== -->

	<!-- ===================================
	{{Contenido principal}}
	==================================== -->
	<section id="principal" class="cleaner carritodecompras maxw container-fluid">
		<article id="pasos" class="cleaner maxw2 mt2">
			<div class="row cleaner">
				<div class="col-md-4 cleaner tac">
					<a href="carrito-1.php" class=""><i class="myicon-products"></i>Mis productos</a>
				</div>
				<div class="col-md-4 cleaner tac">
					<a href="carrito-2.php" class="active"><i class="myicon-info"></i>Mis Datos</a>
				</div>
				<div class="col-md-4 cleaner tac">
					<a href="carrito-3.php" class=""><i class="myicon-methods"></i>Métodos de pago</a>
				</div>
			</div>
		</article>	
		<article id="mispedidos" class="cleaner row accesos">
			<div class="col-md-12 cleaner">
				<div class="row cleaner">
					<div class="col-md-8 cleaner ">
				 		<div id="titulo" class="cleaner">
							<div class="row cleaner">
								<div class="col-md-12 cleaner">
									<div class="titulo cleaner">
										<div class="capa cleaner">
											<div class="lineas cleaner">
												<h2>Esto es lo que <strong>llevaré</strong></h2>
											</div>
										</div>
									</div> 
								</div>
							</div>
						</div>
						<div class="listadeproductos cleaner maxw2 row tospace">
							<div class="col-md-6">
								<div class="tospace cleaner">
									<div class="cleaner tac">
										<div class="title">
											<h1>¿No tienes una cuenta? ¡No hay problema!</h1>
										</div>
									</div>
									<div class="cleaner mt">
										<a href="carrito-3.php" class="cleaner animated mybut but-style1">
											<span class="cleaner">Seguir como invitado</span>
										</a>
									</div>
									<div class="cleaner mt">
										<a href="javascript:void(0)" class="cleaner toanimated2 mybut but-style3">
											<span class="cleaner">
												<div class="col-xs-2 cleaner tof"><i class="icon-facebook animated"></i></div>
												<div class="col-xs-10 cleaner"><em>Login con Facebook</em></div>
											</span>
										</a>
									</div>
								</div>
							</div>
							<div class="col-md-6">								
								<div class="tospace cleaner">
									<div class="cleaner tac">
										<div class="title">
											<h2>Ingresa los datos de tu cuenta (login de prueba: email=pruebas@manya.pe</h2>
										</div>
									</div>
									<div class="cleaner ">
										<div class="cleaner">
											<form action="carrito-3.php" id="frm-login" class="cleaner">
												<div class="control-clone cleaner mt">
													<input type="text" class="myinput style2 cleaner" placeholder="Ingresar E-mail" id="usuario" name="usuario">
												</div>												
												<div id="verclave" class="control-clone cleaner mt">
													<input type="password" class="myinput style2 cleaner" placeholder="Contraseña (Min: 6 caracteres)" id="clave" name="clave">
													<a href="javascipt:void(0)" ><i class="icon-eye"></i></a>
												</div>
															<div class="txtrecuperar cleaner">
													<p><i class="icon-alert"></i>Este usuario no existe</p>
												</div>												
												<div class="cleaner mt ma">
													<button id="ingresaramicuenta" type="submit" class="cleaner animated mybut but-style2 full">
														<span class="cleaner">Ingresar a mi cuenta</span>
													</button>
												</div>
											</form>
										</div>
										<div class="cleaner mt tologin">
											<a id="meolvidemiclave" href="javascript:void(0)"  title="Olvidé mi contraseña">Olvidé mi contraseña <i class="icon-arrow-right"></i> </a>
										</div>
										<div class="cleaner torecuperar">
											<form action="gracias_recuperar.php" id="frm-recuperar" class="cleaner" >
												<div class="control-clone cleaner mt">
													<input type="text" class="myinput style2 cleaner" placeholder="Ingresar E-mail" id="emailrecuperar" name="emailrecuperar">
												</div>
												<div class="txtrecuperar cleaner">
													<p><i class="icon-alert"></i>Este usuario no existe</p>
												</div>
												<div class="cleaner ma tac mt">
													<button id="recuperarmicuenta" type="submit" class="cleaner animated mybut but-style2 ">
														<span class="cleaner">Recuperar cuenta</span>
													</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4 cleaner myline3">
						<div id="titulo" class="cleaner">
							<div class="row cleaner">
								<div class="col-md-12 cleaner">
									<div class="titulo cleaner">
										<div class="capa cleaner">
											<div class="lineas cleaner">
												<h2>Esto es lo que <strong>llevaré</strong></h2>
											</div>
										</div>
									</div> 
								</div>
							</div>
						</div>
						<div class="listadeproductos cleaner maxw2">
							<div class="producto counterproduct cleaner animated">
								<div class="row cleaner">
									<div class="cleaner col-md-3 col-xs-4">
										<div class="fotoproducto cleaner">
											<img src="fotos/producto.jpg" alt="" class="cleaner">
										</div>							
									</div>
									<div class="cleaner vat col-md-9 col-xs-8">	
										<div class="cleaner titulo">
											<h2>Nombre del Producto <span>(S/ 35.00)</span></h2>
										</div>
										<div class="cleaner">
											<label>Precio: S./20.00</label>
										</div>										
									</div>
								</div>
							</div>
							<div class="producto counterproduct cleaner animated">
								<div class="row cleaner">
									<div class="cleaner col-md-3 col-xs-4">
										<div class="fotoproducto cleaner">
											<img src="fotos/producto.jpg" alt="" class="cleaner">
										</div>							
									</div>
									<div class="cleaner vat col-md-9 col-xs-8">	
										<div class="cleaner titulo">
											<h2>Nombre del Producto <span>(S/ 35.00)</span></h2>
										</div>
										<div class="cleaner">
											<label>Precio: S./20.00</label>
										</div>										
									</div>
								</div>
							</div>			
						</div>
						<div class="total cleaner">
							<div class="tabla cleaner">
								<div class="cleaner"><p>Subotal:</p></div>
								<div class="cleaner"><p>S./60.00</p></div>
							</div>
							<div class="tabla cleaner">
								<div class="cleaner"><p>Envío:</p></div>
								<div class="cleaner"><p>S./60.00</p></div>
							</div>
							<div class="tabla cleaner">
								<div class="cleaner"><p>Dscto:</p></div>
								<div class="cleaner"><p>S./0.00</p></div>
							</div>
							<div class="tabla cleaner">
								<div class="cleaner final"><em>Total:</em></div>
								<div class="cleaner final"><em>S./0.00</em></div>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</article>	
	</section>

	<!-- ==========================================
	{{fin del contenido principal}}
	=========================================== -->

	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<?php include 'includes/footer.php' ?>	
	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<script>
		$(window).on({
			load: function(){
				
			},
			resize: function(){
				
			}
		});

		$(document).ready(function(){
			

			$('#meolvidemiclave').click(function(){
				$('input').val('');
				$('.torecuperar').slideToggle();
				$(this).toggleClass('active');
				$('.todisabled').toggleClass('active');
				$('#clave').closest('.control-clone').removeClass('error');
				$('#usuario').closest('.control-clone').removeClass('error');

			})					

			$('#frm-login').validate({
				rules: {		
					usuario: {
						required: true,
						email: true,
						remote:
	                    {
							url: '_getLogin.php',
							type: "post",
							data:
							{
							 	usuario: function()
							 	{
							    	return $('#frm-login input[name="usuario"]').val();
							  	}
							},
							complete: function(data)
							{
								if(data.responseText != true){
									$('#frm-login .txtrecuperar').fadeIn();
								}else{
									$('#frm-login .txtrecuperar').fadeOut();
								}
							}
	                    }
					},
					clave: {
						required: true
					}
				},
				messages: {					
					usuario: {
						required: '&nbsp;',
						email: '&nbsp;',
						remote: '&nbsp;'
					},
					clave: {
						required: '&nbsp;'
					}
				},
				errorClass: "incorrecto",
				errorElement: "span",
				showErrors: function(errorMap, errorList){
				    var counterError = this.numberOfInvalids();
					if(counterError == 1){
						$("#contenedor-alerta .texto span").html("Se presentó "+ counterError + " error.");						
					}else{
						$("#contenedor-alerta .texto span").html("Se presentaron "+ counterError + " errores.");
					}
					this.defaultShowErrors();
				},				
				unhighlight: function(element){
					$(element).closest('.control-clone').removeClass('error').addClass('success');
				},
				highlight: function(element){
					$(element).closest('.control-clone').removeClass('success').addClass('error');
				},
				invalidHandler: function (event, validator) {
		            var existeError = validator.numberOfInvalids();		            
		            if (existeError) {		            	
		            	$('body').addClass('frmError');  
		            	$("#contenedor-alerta").closest('body').addClass('showContenedor');
		            	$('#ingresaramicuenta').attr('disabled','disabled');
					  	setTimeout(function(){
					    	$('body').removeClass('showContenedor');
					    	$('#ingresaramicuenta').removeAttr('disabled');
					    }, 4900);
		            }else{
		            	$('body').removeClass('frmError');
		            }
		        },
				success: function(element){
					element.closest('body').removeClass('frmError').addClass('frmSuccess');
				}
			});

			$('#frm-recuperar').validate({
				rules: {		
					emailrecuperar: {
						required: true,
						email: true,
						remote:
	                    {
							url: '_getRecuperarClave.php',
							type: "post",
							data:
							{
							 	emailrecuperar: function()
							 	{
							    	return $('#frm-recuperar input[name="emailrecuperar"]').val();
							  	}
							},
							complete: function(data)
							{
								if(data.responseText != true){
									$('#frm-recuperar .txtrecuperar').fadeIn();
								}else{
									$('#frm-recuperar .txtrecuperar').fadeOut()
								}
							}
	                    }
					}
				},
				messages: {					
					emailrecuperar: {
						required: '&nbsp;',
						email: '&nbsp;',
						remote: '&nbsp;'
					}
				},
				errorClass: "incorrecto",
				errorElement: "span",
				showErrors: function(errorMap, errorList){
				    var counterError = this.numberOfInvalids();
					if(counterError == 1){
						$("#contenedor-alerta .texto span").html("Se presentó "+ counterError + " error.");	
						this.defaultShowErrors();					
					}else{
						$("#contenedor-alerta .texto span").html("Se presentaron "+ counterError + " errores.");
						this.defaultShowErrors();
					}					
				},				
				unhighlight: function(element){
					$(element).closest('.control-clone').removeClass('error').addClass('success');
				},
				highlight: function(element){
					$(element).closest('.control-clone').removeClass('success').addClass('error');
				},
				invalidHandler: function (event, validator) {
		            var existeError = validator.numberOfInvalids();		            
		            if (existeError) {		            	
		            	$('body').addClass('frmError');  
		            	$("#contenedor-alerta").closest('body').addClass('showContenedor');
		            	$('#recuperarmicuenta').attr('disabled','disabled');
					  	setTimeout(function(){
					    	$('body').removeClass('showContenedor');
					    	$('#recuperarmicuenta').removeAttr('disabled');
					    }, 4900);
		            }else{
		            	$('body').removeClass('frmError');
		            }
		        },
				success: function(element){
					element.closest('body').removeClass('frmError').addClass('frmSuccess');
				},
				submitHandler: function (form) {
					console.log($(form).serialize())
                }
			});
		    
		})

		
	</script>	
</body>
</html>