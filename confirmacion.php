<!doctype html>
<html lang="en">
<head>
	<!-- ===================================
	{{Chunk site_seo}}
	==================================== -->
	<?php include 'includes/seo.php' ;?>
	<!-- ===================================
	{{fin Chunk site_seo}}
	==================================== -->
	<meta name="description" content="Detalle del Producto">
    <meta property="og:title" content="[[*page_title]]" /> 
    <meta property="og:url" content="[[++site_url]][[~id]]" />
    <meta property="og:image" content="[[*page_imagen]]" />
    <meta property="og:description" content="[[*page_description]]" /> 
    <meta name="twitter:image:src" content="[[*page_imagen]]"/>
	<title>[[*page_title]]</title>	
</head>
<body>
	<!-- ===================================
	{{Chunk site_header}}
	==================================== -->
	<?php include 'includes/header.php' ?>
	<!-- ===================================
	{{fin Chunk site_header}}
	==================================== -->

	<!-- ===================================
	{{Contenido principal}}
	==================================== -->
	<section id="principal" class="cleaner">		
		<article id="formularios" class="cleaner mt2 maxw">
			<div id="titulo" class="cleaner">
				<div class="row cleaner">
					<div class="col-md-12 cleaner">
						<div class="titulo cleaner">
							<div class="capa cleaner">
								<div class="lineas cleaner">
									<h2>¡Confirma tu tranasacción ingresando <strong>tus datos !</strong></h2>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="micontacto" class="cleaner maxw3">
				<div class="row cleaner">
					<div class="col-md-7 mb cleaner">
						<div class="cleaner ">
							<form action="gracias.php" id="frm-confirmar" class="cleaner">
								<div class="row cleaner">
									<div class="col-md-12 cleaner txt mt">
										<p>Número de pedido: <b>46064-65413-4452</b></p>
									</div>
									<div class="col-md-12 cleaner">
										<div class="control-clone cleaner mt ">
											<select name="banco" id="banco" class="cleaner validar_select">
												<option value=""></option>
												<option value="1">BCP</option>
												<option value="2">BBVA</option>
												<option value="3">SCOTIABANK</option>
											</select>
										</div>
									</div>		
									<div class="col-md-12 cleaner">
										<div class="control-clone cleaner mt">
											<input type="text" class="myinput style2 cleaner" placeholder="Número de depósito" id="deposito" name="deposito">
										</div>
									</div>	
									<div class="col-md-12 cleaner">
										<div class="control-clone cleaner minicaja mt">
											<div class="cleaner">
												<input id="fecha" name="fecha" type="text" class="myinput style2 dib validacion" placeholder="Fecha de operación" >
												<i class="myicon-calendar triggerdate animated"></i>
											</div>
										</div>
									</div>
																							
				                    <div class="cleaner mt ma tac col-md-12">
										<button type="submit" class="animated mybut but-style1">
											<span class="cleaner">Enviar confirmación</span>
										</button>
									</div>
								</div>			
							</form>
						</div>
					</div>
					<div class="col-md-5 mb cleaner mt">
						<div class="tac ma cleaner">
							<div class="globo cleaner tac txt">
								<p>¡Los datos requeridos se encuentran en el voucher recibido al momento de haber <strong>realizado el depósito!</strong></p>
							</div>
							<div class="foto cleanr">
								<img src="imagenes/laptop-5.svg" alt="Contactar ahora" class="cleaner full">
							</div>
						</div>
					</div>
				</div>
			</div>		
		</article>
	
	</section>


	<!-- ==========================================
	{{fin del contenido principal}}
	=========================================== -->

	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<?php include 'includes/footer.php' ?>	
	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->

	<script>
		$(window).on({
			load: function(){				  
			},
			resize: function(){				
			}
		});

		$(document).ready(function(){
			$("#banco").select2({
		    	placeholder: "Elegir el Banco ",
				allowClear: false
		    });

			$('#frm-confirmar').validate({
				rules: {
					banco:{
						required: true
					},
					deposito:{
						required: true,
						digits: true
					},
					fecha:{
						required: true
					}
				},
				messages: {	
					banco:{
						required: '&nbsp;'
					},
					deposito:{
						required: '&nbsp;',
						digits: '&nbsp;'
					},
					fecha:{
						required: '&nbsp;'
					}
				},
				errorClass: "incorrecto",
				errorElement: "span",
				showErrors: function(errorMap, errorList){
				    var counterError = this.numberOfInvalids();
					if(counterError == 1){
						$("#contenedor-alerta .texto span").html("Se presentó "+ counterError + " error.");	
						this.defaultShowErrors();					
					}else{
						$("#contenedor-alerta .texto span").html("Se presentaron "+ counterError + " errores.");
						this.defaultShowErrors();
					}					
				},				
				unhighlight: function(element){
					$(element).closest('.control-clone').removeClass('error').addClass('success');
				},
				highlight: function(element){
					$(element).closest('.control-clone').removeClass('success').addClass('error');
				},
				invalidHandler: function (event, validator) {
		            var existeError = validator.numberOfInvalids();		            
		            if (existeError) {		            	
		            	$('body').addClass('frmError');  
		            	$("#contenedor-alerta").closest('body').addClass('showContenedor');
		            	$('button:submit').attr('disabled','disabled');
					  	setTimeout(function(){
					    	$('body').removeClass('showContenedor');
					    	$('button:submit').removeAttr('disabled');
					    }, 4900);
		            }else{
		            	$('body').removeClass('frmError');
		            }
		        },
				success: function(element){
					element.closest('body').removeClass('frmError').addClass('frmSuccess');				
				}
			});	

		})
	</script>
</body>
</html>