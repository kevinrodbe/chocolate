<!doctype html>
<html lang="en">
<head>
	<!-- ===================================
	{{Chunk site_seo}}
	==================================== -->
	<?php include 'includes/seo.php' ;?>
	<!-- ===================================
	{{fin Chunk site_seo}}
	==================================== -->
	<meta name="description" content="[[*page_description]]">
    <meta property="og:title" content="[[*page_title]]" /> 
    <meta property="og:url" content="[[++site_url]][[~id]]" />
    <meta property="og:image" content="[[*page_imagen]]" />
    <meta property="og:description" content="[[*page_description]]" /> 
    <meta name="twitter:image:src" content="[[*page_imagen]]"/>
	<title>[[*page_title]]</title>
</head>
<body>
	<!-- ===================================
	{{Chunk site_header}}
	==================================== -->
	<?php include 'includes/header.php' ?>
	<!-- ===================================
	{{fin Chunk site_header}}
	==================================== -->

	<!-- ===================================
	{{Contenido principal}}
	==================================== -->
	<section id="principal" class="cleaner">
		<article id="seccion" class="cleaner">			
			<ul id="banner" class="cleaner">
				<li class="item">
					<!-- Imagen se verá en versiòn Desktop -->
					<img src="imagenes/banner1.png" alt="" class="noresponsive">
					<!-- Imagen se verá en versiòn Celular/Tablet -->
					<a href="" class="responsive cleaner">
						<img src="imagenes/banner1.png" alt="">
					</a>
					<div class="content cleaner maxw">
						<div class="cleaner descripcion ">
							<div class="cleaner tac animated">
								<h4>No será tu primer regalo, pero sí el que causará la mejor de las impresiones.</h4>
							</div>
							<div class="cleaner">
								<a href="producto.php" class="cleaner animated mybut but-style1">
									<span class="cleaner">Ver regalos</span>
								</a>
							</div>
						</div>
					</div>
				</li>
				<li class="item">
					<!-- Imagen se verá en versiòn Desktop -->
					<img src="imagenes/banner1.png" alt="" class="noresponsive">
					<!-- Imagen se verá en versiòn Celular/Tablet -->
					<a href="" class="responsive cleaner">
						<img src="imagenes/banner1.png" alt="">
					</a>
					<div class="content cleaner maxw">
						<div class="cleaner descripcion ">
							<div class="cleaner tac animated">
								<h4>No será tu primer regalo, pero sí el que causará la mejor de las impresiones.</h4>
							</div>
							<div class="cleaner">
								<a href="producto.php" class="cleaner animated mybut but-style1">
									<span class="cleaner">Ver regalos</span>
								</a>
							</div>
						</div>
					</div>
				</li>
				<li class="item">
					<!-- Imagen se verá en versiòn Desktop -->
					<img src="imagenes/banner1.png" alt="" class="noresponsive">
					<!-- Imagen se verá en versiòn Celular/Tablet -->
					<a href="" class="responsive cleaner">
						<img src="imagenes/banner1.png" alt="" >
					</a>
					<div class="content cleaner maxw">
						<div class="cleaner descripcion ">
							<div class="cleaner tac animated">
								<h4>No será tu primer regalo, pero sí el que causará la mejor de las impresiones.</h4>
							</div>
							<div class="cleaner">
								<a href="producto.php" class="cleaner animated mybut but-style1">
									<span class="cleaner">Ver regalos</span>
								</a>
							</div>
						</div>
					</div>
				</li>
			</ul>
			<!-- Banner desktop -->
			<div id="categorias" class="cleaner noresponsive">
	            <ul class='kwicks kwicks-horizontal'>
	                <li id='cat-1' class="">
	                	<div class="mitabla cleaner ">
	                		<div class="cleaner imagen">
	                			<img src="imagenes/corporativo1.png" alt="" class="nohover">
	                			<img src="imagenes/corporativo2.png" alt="" class="hover">
	                		</div>
	                		<div class="cleaner txt myline3">
	                			<div class="ubicar cleaner">
	                				<div class="cleaner myline5 namecat">
		                				<h5>Regalos corporativos</h5>
		                			</div>
		                			<div class="cleaner desc">
		                				<p>Sorprende a esa persona querida con un obsequio único, original y de calidad.</p>
		                			</div>
		                			<div class="cleaner but">
		                				<a href="javascript:void(0)" class="mybut but-style2 ">
		                					<span class="cleaner">Ver regalo</span>
		                				</a>
		                			</div>
	                			</div>
	                		</div>
	                	</div>
	                </li>
	                <li id='cat-2' class="">
	                	<div class="mitabla cleaner">
	                		<div class="cleaner">
	                			<img src="imagenes/sorprender1.png" alt="" class="nohover">
	                			<img src="imagenes/sorprender2.png" alt="" class="hover">
	                		</div>
	                		<div class="cleaner myline3">
	                			<div class="ubicar cleaner">
	                				<div class="cleaner myline5 namecat">
		                				<h5>Sorprende a esa persona</h5>
		                			</div>
		                			<div class="cleaner desc">
		                				<p>Sorprende a esa persona querida con un obsequio único, original y de calidad.</p>
		                			</div>
		                			<div class="cleaner but">
		                				<a href="galeria.php" class="mybut but-style2 ">
		                					<span class="cleaner">Ver regalo</span>
		                				</a>
		                			</div>
	                			</div>
	                		</div>
	                	</div>
	                </li>
	                <li id='cat-3' class="">
	                	<div class="mitabla cleaner">
	                		<div class="cleaner">
	                			<img src="imagenes/navidad1.png" alt="" class="nohover">
	                			<img src="imagenes/navidad2.png" alt="" class="hover">
	                		</div>	                		
	                		<div class="cleaner">
	                			<div class="ubicar cleaner">
	                				<div class="cleaner myline5 namecat">
			                			<h5>¡Feliz Navidad!</h5>
			                		</div>
		                			<div class="cleaner desc">
		                				<p>Sorprende a esa persona querida con un obsequio único, original y de calidad.</p>
		                			</div>
		                			<div class="cleaner but">
		                				<a href="javascript:void(0)" class="mybut but-style2 ">
		                					<span class="cleaner">Ver regalo</span>
		                				</a>
		                			</div>
	                			</div>
	                		</div>
	                	</div>
	                </li>
	            </ul>
			</div>
			<!-- Banner para celulares/tablets -->
			<div id="categorias" class="cleaner maxw responsive">
				<div class="row cleaner">
					<div class="col-md-12 cleaner">
						<div class="titulo cleaner">
							<div class="capa cleaner">
								<div class="lineas cleaner">
									<h2>CAtegorìas de <strong>nuestros productos</strong></h2>
								</div>
							</div>
						</div>
					</div>
				</div>
	            <ul class='cleaner'>	            	
	            	<li class="cleaner">
	            		<div class="cleaner myline5 namecat">
            				<h5>Regalos corporativos</h5>
            			</div>
            			<div class="cleaner">
            				<img src="imagenes/corporativo1.png" alt="" class="nohover">
            			</div>
            			<div class="cleaner desc">
            				<p>Sorprende a esa persona querida con un obsequio único, original y de calidad.</p>
            			</div>
            			<div class="cleaner">
            				<a href="galeria.php" class="mybut but-style2 cleaner">
            					<span class="cleaner">Ver regalo</span>
            				</a>
            			</div>
	            	</li>
	            	<li class="cleaner">
	            		<div class="cleaner myline5 namecat">
            				<h5>Regalos corporativos</h5>
            			</div>
            			<div class="cleaner">
            				<img src="imagenes/sorprender1.png" alt="" class="nohover">
            			</div>
            			<div class="cleaner desc">
            				<p>Sorprende a esa persona querida con un obsequio único, original y de calidad.</p>
            			</div>
            			<div class="cleaner">
            				<a href="galeria.php" class="mybut but-style2 cleaner">
            					<span class="cleaner">Ver regalo</span>
            				</a>
            			</div>
	            	</li>
	            	<li class="cleaner">
	            		<div class="cleaner myline5 namecat">
            				<h5>Feliz Navidad</h5>
            			</div>
            			<div class="cleaner">
            				<img src="imagenes/navidad1.png" alt="" class="nohover">
            			</div>
            			<div class="cleaner desc">
            				<p>Sorprende a esa persona querida con un obsequio único, original y de calidad.</p>
            			</div>
            			<div class="cleaner">
            				<a href="galeria.php" class="mybut but-style2 cleaner">
            					<span class="cleaner">Ver regalo</span>
            				</a>
            			</div>
	            	</li>
	            </ul>
			</div>
		</article>
		<article id="procesos" class="cleaner mt maxw">
			<div class="row cleaner">
				<div class="col-md-12 cleaner">
					<div class="titulo cleaner">
						<div class="capa cleaner">
							<div class="lineas cleaner">
								<h2>Compre fácil  y seguro en <strong>Chocolate Express</strong></h2>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="cleaner content img">
				<div class="row cleaner">
					<div class="col-md-3 myline3 cleaner">
						<div class="caja cleaner">
							<div class="cleaner noresponsive">
								<img src="imagenes/entrarweb.png" alt="">
							</div>
							<div class="cleaner">
								<h3>1. Entra a nuestra web</h3>
							</div>
							<div class="cleaner">
								<p>Visita nuestra web para encontrar lo más finios regalos</p>
							</div>
							<div class="cleaner responsive">
								<img src="imagenes/entrarweb.png" alt="">
							</div>
						</div>
					</div>
					<div class="col-md-3 myline3 cleaner">
						<div class="caja cleaner">
							<div class="cleaner noresponsive">
								<img src="imagenes/elijeproducto.png" alt="">
							</div>
							<div class="cleaner">
								<h3>2. Elige tu producto</h3>
							</div>
							<div class="cleaner">
								<p>Elige de nuestra gran variedad de productos.</p>
							</div>
							<div class="cleaner responsive">
								<img src="imagenes/elijeproducto.png" alt="">
							</div>
						</div>
					</div>
					<div class="col-md-3 myline3 cleaner">
						<div class="caja cleaner">
							<div class="cleaner noresponsive">
								<img src="imagenes/compraseguro.png" alt="">
							</div>
							<div class="cleaner">
								<h3>3. Compra seguro</h3>
							</div>
							<div class="cleaner">
								<p>Toda operación estará protegida por una conexión segura.</p>
							</div>
							<div class="cleaner responsive">
								<img src="imagenes/compraseguro.png" alt="">
							</div>
						</div>
					</div>
					<div class="col-md-3 myline3 cleaner">
						<div class="caja cleaner">
							<div class="cleaner noresponsive">
								<img src="imagenes/entregaregalo.png" alt="">
							</div>
							<div class="cleaner">
								<h3>4. Entrega del regalo</h3>
							</div>
							<div class="cleaner">
								<p>El regalo llegará en perfectas condiciones a su destino.</p>
							</div>
							<div class="cleaner responsive">
								<img src="imagenes/entregaregalo.png" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>			
		</article>
	</section>

	<!-- Productos favoritos -->
	<section id="favoritos" class="cleaner maxw movetrigger">
		<div class="row cleaner">
			<div class="col-md-12 cleaner">
				<div class="titulo cleaner">
					<div class="capa cleaner">					
						<div class="lineas cleaner flechas">
							<a href="javascript:void(0)" class="flecha toizq cleaner noresponsive"><i class="icon-arrow-left"></i></a>
							<h2>Los favoritos de <strong>Chocolate Express</strong></h2>
							<a href="javascript:void(0)" class="flecha toizq cleaner responsive"><i class="icon-arrow-left"></i></a>
							<a href="javascript:void(0)" class="flecha toder cleaner"><i class="icon-arrow-right"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row cleaner">
			<!-- Productos Favoritos -->			
			<?php include 'includes/productosfavoritos.php' ?>
			<!-- end -->
		</div>
	</section>	

	<!-- end Favoritos -->

	<!-- ==========================================
	{{fin del contenido principal}}
	=========================================== -->

	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<?php include 'includes/footer.php' ?>	
	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<script>
		$(window).on({
			load: function(){

				// Cargar Productos vistos/Favoritos para Vender {Footer}
				$.getJSON('json/productos.json', function(data) {
				    var template = $('#tmp-favoritos').html();
				    var info = Mustache.to_html(template, data);
				    $('#productosfavoritos').html(info);
				}).done(function(){					
					$('.productosfavoritos').owlCarousel({
					    items:5,
					    dots:false,
					    loop:true,
					    margin:30,
					    // autoHeight: true,
					    responsive:{
					        0:{
					            items:1
					        },
					        450:{
					            items:1
					        },
					        550:{
					            items:2
					        },
					        800:{
					            items:3
					        },
					        1000:{
					            items:4
					        },
					        1200:{
					            items:5
					        }
					    }
					});
					$('.productosfavoritos').removeClass('opacar').delay(10).promise().done(function(){						
						$('.like').on('click', function(){
							$(this).toggleClass('activo');					

							if($(this).hasClass('activo')){
								$(this).find('i').addClass('bounceIn');						
							}else{
								$(this).find('i').removeClass('bounceIn');
							}
						})
					});
				});
				
			},
			resize: function(){
				$('.kwicks').kwicks({
	                maxSize : '40%',
	                behavior: 'menu'
	            });
			}
		});

		$(document).ready(function(){

			// Activar expand para CAtegorìas
			$('.kwicks').kwicks({
                maxSize : '40%',
                behavior: 'menu'
            });
			// Banner
			$('#banner').owlCarousel({
				items:1,			  
				autoplay:true,
    			autoplayTimeout:2000,
    			autoplayHoverPause:true,
			    dots:true,
			    loop:true,
			    margin:0,
			    // autoHeight: true,
			    responsive:{
			        0:{
			            items:1
			        },
			        1000:{
			            items:1
			        }
			    }
			});	
	
		})
	</script>

	

	<!-- ==========================================
	=========================================== -->
</body>
</html>