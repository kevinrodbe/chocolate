<!doctype html>
<html lang="en">
<head>
	<!-- ===================================
	{{Chunk site_seo}}
	==================================== -->
	<?php include 'includes/seo.php' ;?>
	<!-- ===================================
	{{fin Chunk site_seo}}
	==================================== -->
	<meta name="description" content="[[*page_description]]">
    <meta property="og:title" content="[[*page_title]]" /> 
    <meta property="og:url" content="[[++site_url]][[~id]]" />
    <meta property="og:image" content="[[*page_imagen]]" />
    <meta property="og:description" content="[[*page_description]]" /> 
    <meta name="twitter:image:src" content="[[*page_imagen]]"/>
	<title>[[*page_title]]</title>

	<style>
		#loader.show{display: block }
	</style>

</head>
<body>	
	<!-- Capa para mostrar, cuando se ejecute una consulta. Agregar clase "Show" -->
	<div id="loader" class="cleaner">
	    <div class="icono cleaner"></div>
	</div>
	<!-- ===================================
	{{Chunk site_header}}
	==================================== -->
	<?php include 'includes/header.php' ?>
	<!-- ===================================
	{{fin Chunk site_header}}
	==================================== -->

	<!-- ===================================
	{{Contenido principal}}
	==================================== -->
	<section id="principal" class="cleaner">		
		<article id="galeria" class="cleaner mt2 maxw">
			<div id="titulo" class="cleaner">
				<div class="row cleaner">
					<div class="col-md-12 cleaner">
						<div class="titulo cleaner">
							<div class="capa cleaner">
								<div class="lineas cleaner">
									<h2>Encuentra el <strong>regalo perfecto</strong></h2>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="milistaproductos" class="cleaner">
				<div class="row cleaner">
					<div class="col-md-3 cleaner">
						<div class="cleaner">
							<dl id="misopciones" class="cleaner mb">
		                        <dt id="tab1" class="dt cleaner">
									<div class="content cleaner">
									    <em>Sorprende a esa persona</em>
									    <i class="icon-arrow-left"></i>
									</div>
								</dt>
								<dd class="dd cleaner">
								    <div class="content cleaner">
								    	<div class="linea cleaner">
									    	<div class="opciones cleaner">
									    		<div class="cleaner">
											    	<div class="content-radio cleaner">
														<div class="cleaner">
															<input type="radio" name="filtrogenero" id="ambos" value="1" class="radio" checked>
															<label for="ambos"></label>
														</div>
														<div class="cleaner">
															<label for="ambos">Ambos</label>
														</div>
													</div>
											    </div>
											    <div class="cleaner">
											    	<div class="content-radio cleaner">
														<div class="cleaner">
															<input type="radio" name="filtrogenero" id="ellas" value="2" class="radio">
															<label for="ellas"></label>
														</div>
														<div class="cleaner">
															<label for="ellas">Para ellas</label>
														</div>
													</div>
											    </div>
											    <div class="cleaner">
											    	<div class="content-radio cleaner">
														<div class="cleaner">
															<input type="radio" name="filtrogenero" id="ellos" value="3" class="radio">
															<label for="ellos"></label>
														</div>
														<div class="cleaner">
															<label for="ellos">Para ellos</label>
														</div>
													</div>
											    </div>
									    	</div>
									    </div>
									    <div class="cleaner linea">
									    	<a href="javascript:void(0)" class="cleaner transition">Deportista</a>
									    </div>
								    </div>
								</dd>

								<dt id="tab2" class="dt cleaner myline2">
									<div class="content cleaner">
									    <em>Días festivos</em>
									    <i class="icon-arrow-left"></i>
									</div>
								</dt>
								<dd class="dd cleaner">
								    <div class="content cleaner">								    	
									    <div class="cleaner linea">
									    	<a href="javascript:void(0)" class="cleaner transition">Deportista</a>
									    </div>
								    </div>
								</dd>

								<dt id="tab3" class="dt cleaner myline2">
									<a class="content cleaner" href="javascript:void(0)">
									    <em>Ofertas</em>
									</a>
								</dt>
								<dd class="dd cleaner"></dd>

								<dt id="tab4" class="dt cleaner myline2">
									<div class="content cleaner">
									    <em>Regalos coorporativos</em>
									    <i class="icon-arrow-left"></i>
									</div>
								</dt>
								<dd class="dd cleaner">
								    <div class="content cleaner">								    	
									    <div class="cleaner linea">
									    	<a href="javascript:void(0)" class="cleaner transition">Deportista</a>
									    </div>
								    </div>
								</dd>
							</dl>
						</div>
					</div>
					<div class="col-md-9 cleaner">
						<ul id="galeriadeproductos" class="cleaner productosfavoritos tocarro grid effect-6 load-posts">
							<!-- Carga de la galeria de productos -->			
						</ul>
						<!-- Cargar los productos -->
						<script id="tmp-galeriaproductos" type="text/template">
							{{#productos}}				
								<li class="item">
									<div class="pr cleaner">
										<div class="etiqueta cleaner">
											<div class="estado {{estado}}">{{estado}}</div>
										</div>
										{{!La clase "added", se agrega a .producto , cuando el producto se agregó correctamente}}
										<div class="producto cleaner">
											<div class="foto cleaner">
												<img src="{{foto}}" alt="{{nombreproducto}}">
											</div>
											<div class="bg cleaner transition">
												<div class="content cleaner">
													<div class="cleaner descripcion">
														<div class="cleaner totop">
															{{!Div antes que el producto sea agregado}}
															<div class="accion cleaner agregarproducto">
																<div class="opc cleaner">
																	<a href="javascript:void(0)" title="Agregar producto" class="cleaner circle toanimated4 agregaralcarrito"><i class="icon-plus iconito center animated"></i></a>
																</div>
															</div>
															{{!DIV despues que el producto se agregó}}
															<div class="productoagregado cleaner">
																<a href="carrito.php" class=" mybut but-style1 cleaner">
																	<span class="cleaner">Ir al carrito</span>
																</a>
															</div>
														</div>							
														<div class="cleaner tobottom">
															<a href="producto.php" class=" mybut but-style1 cleaner">
																<span class="cleaner">Ver producto</span>
															</a>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="cleaner text favoritos-but-responsive hidden">
											<a href="javascript:void(0)" class="cleaner  mybut but-style1">
												<span class="cleaner">Ver producto</span>
											</a>
										</div>
										<div class="cleaner nombre">
											<h4>{{nombreproducto}}</h4>
										</div>
										<div class="cleaner estadistica">
											<div class="cleaner tal"><i class="icon-eye"></i>{{vistos}}</div>
											<div class="cleaner tar"><a href="javascript:void(0)" class="like"><i class="icon-like animated"></i>{{favoritos}}</a></div>
										</div>
										<div class="precios cleaner myline2">
											<div class="tar cleaner">
												<span>S./ {{precioanterior}}</span>
												<em>S./ {{precionuevo}}</em>
											</div>
										</div>
									</div>
								</li>
							{{/productos}}
						</script>
						<div class="cleaner">
							<div class="paginator cleaner">
								<ul class="cleaner">
									<li class="cleaner"><a href="javascript:void(0)" class="cleaner prev" rel="prev"><i class="icon-arrow-left"></i></a></li>
									<li class="cleaner"><a href="javascript:void(0)" class="cleaner">1</a></li>
									<li class="cleaner"><a href="javascript:void(0)" class="cleaner">2</a></li>
									<li class="cleaner"><a href="javascript:void(0)" class="cleaner">3</a></li>
									<li class="cleaner"><a href="javascript:void(0)" class="cleaner next" rel="next"><i class="icon-arrow-left"></i></a></li>
								</ul>
							</div>
						</div>
					</div>					
				</div>
			</div>			
		</article>		
	</section>
	
	

	<!-- ==========================================
	{{fin del contenido principal}}
	=========================================== -->

	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<?php include 'includes/footer.php' ?>	
	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<!-- {Galeria dinamica - Codrops} -->
	<script src="js/load_scrolling/classie.min.js"></script>
	<script src="js/load_scrolling/modernizr.custom.2.js"></script>
	<script src="js/load_scrolling/masonry.pkgd.min.js"></script>
	<script src="js/load_scrolling/imagesloaded.js"></script>
	<script src="js/load_scrolling/AnimOnScroll.js"></script>

	<script>
		$(window).on({
			load: function(){
				// Cargar Productos para Vender {Footer}
				$.getJSON('json/productos.json', function(data) {
				    var template = $('#tmp-galeriaproductos').html();
				    var info = Mustache.to_html(template, data);
				    $('#galeriadeproductos').html(info);
				}).done(function(){					
					
					new AnimOnScroll( document.getElementById( 'galeriadeproductos' ), {
						minDuration : 0.4,
						maxDuration : 0.7,
						viewportFactor : 0.2
					});

					$('#galeriadeproductos').addClass('listo').delay(10).promise().done(function(){						
						$('.like').on('click', function(){
							$(this).toggleClass('activo');				
							if($(this).hasClass('activo')){
								$(this).find('i').addClass('bounceIn');						
							}else{
								$(this).find('i').removeClass('bounceIn');
							}
						})
					});			

					// Agregar eventos si es Mobile
					setTimeout(function(){
						comprobarnavegador();
					},100)
				});

			},
			resize: function(){
				
			}
		});

		$(document).ready(function(){
			
			// Menu de filtros
			$('dl#misopciones dd').hide();
	        $('dl#misopciones dt').click(function(e) {

	            if ($(this).hasClass('activo')) {
	                $(this).removeClass('activo');
	                $(this).next().animate({
	                    opacity: '0'
	                }, 'slow');
	                $(this).next().slideUp(700);
	            } else {
	                $('dl#misopciones dt').removeClass('activo');
	                $(this).addClass('activo');
	                $('dl#misopciones dd').slideUp();
	                $(this).next().slideDown(700);
	                $(this).next().animate({
	                    opacity: '1'
	                }, 'slow');
	            }
	        });
	
		})
	</script>

	

	<!-- ==========================================
	=========================================== -->
</body>
</html>