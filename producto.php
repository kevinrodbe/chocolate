<!doctype html>
<html lang="en">
<head>
	<!-- ===================================
	{{Chunk site_seo}}
	==================================== -->
	<?php include 'includes/seo.php' ;?>
	<!-- ===================================
	{{fin Chunk site_seo}}
	==================================== -->
	<meta name="description" content="Detalle del Producto">
    <meta property="og:title" content="[[*page_title]]" /> 
    <meta property="og:url" content="[[++site_url]][[~id]]" />
    <meta property="og:image" content="[[*page_imagen]]" />
    <meta property="og:description" content="[[*page_description]]" /> 
    <meta name="twitter:image:src" content="[[*page_imagen]]"/>
	<title>[[*page_title]]</title>
	<style>
		.zoom{
			vertical-align: middle;position: relative;width: 100%;height: auto;max-width: 400px; overflow: hidden;
			background: url(imagenes/loading.gif) no-repeat center #fff;
		}
		.zoom img{width: 100%;height: auto;vertical-align: middle;}
		.zoom img:hover{background: #fff;  -ms-filter: &quot; progid: DXImageTransform.Microsoft.Alpha(Opacity=30)&quot; filter: alpha(opacity=30);-moz-opacity: 0.3;-khtml-opacity: 0.3;opacity: 0.3;
		    cursor:crosshair;
		}
		/*Galeria c/ click para detalles - Producto*/
		#milistaproductos .flechas .flecha.ocultar{display: none}
		#milistaproductos .flechas i{line-height: 38px}
		#milistaproductos .flechas{text-align: center;margin:0 auto;}
		#milistaproductos .flechas .toizq{position: absolute;left: 0px;top: 0;bottom: 0;margin: auto;width: 40px;height: 40px;}
		#milistaproductos .flechas .toder {position: absolute;right: 0px;top: 0;bottom: 0;margin: auto;	width: 40px;height: 40px;}
		#milistaproductos #photo{margin: 0px auto;text-align: center;width:100%;min-height: 150px}
        #milistaproductos .item{max-width: 250px;margin: 0 auto;cursor: pointer;}
        #milistaproductos #cargar-productos{margin:20px auto;position: relative;max-width: 80%}
	</style>
</head>
<body>
	<!-- ===================================
	{{Chunk site_header}}
	==================================== -->
	<?php include 'includes/header.php' ?>
	<!-- ===================================
	{{fin Chunk site_header}}
	==================================== -->

	<!-- ===================================
	{{Contenido principal}}
	==================================== -->
	<section id="principal" class="cleaner">		
		<article id="galeria" class="cleaner mt2 maxw">
			<div id="titulo" class="cleaner">
				<div class="row cleaner">
					<div class="col-md-12 cleaner">
						<div class="titulo cleaner">
							<div class="capa cleaner">
								<div class="lineas cleaner">
									<h2>Encuentra el <strong>regalo perfecto</strong></h2>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="breadcrumbs" class="cleaner">
				<ul class="cleaner">
					<li class=""><a href="./" class="cleaner">Home</a></li>
					<li class=""><a href="galeria.php" class="cleaner">Galería</a></li>
					<li class=""><a href="javascript:void(0)" class="cleaner active">Producto 1</a></li>
				</ul>
			</div>

			<div id="milistaproductos" class="cleaner movetrigger detallesdeproducto">
				<div class="row cleaner">
					<div class="col-md-4 cleaner">
						<div class="cleaner izquierda">
                            <div id="photo" class="cleaner zoom">                                
                                <img src="fotos/producto1.jpg" alt="Producto 1" class="animated img cleaner">
                            </div>                           
                            <div class="cleaner flechas pr">
                            	<ul id="cargar-productos" class="owl-carousel">
                            		<!-- Cargar fotos decriptivas de 1 producto -->
	                            </ul>	
								<a href="javascript:void(0)" class="flecha toizq cleaner ocultar"><i class="icon-arrow-left"></i></a>
                            	<a href="javascript:void(0)" class="flecha toder cleaner ocultar"><i class="icon-arrow-right"></i></a>
							</div>
							 <!-- Template Productos -->
							<script id="tmp-productos" type="text/template">
								{{#productos}}				
									<li class="item cleaner pr">
										<div class="capa cleaner transition">
			                            	<i class="icon-eye"></i>
			                            </div>
						                <img src="{{foto}}" alt="{{nombre}}" class="img">
						            </li>
								{{/productos}}
							</script>
                        </div>
					</div>
					<div class="col-md-8 cleaner">
						<div class="cleaner derecha">
							<div class="cleaner lineamarron">
								<h2>Nombre del producto</h2>
								<div class="accion cleaner">
									<div class="cleaner opc compartir">
										<a href="javascript:void(0)" class="cleaner circle toanimated4 sharing"><i class="icon-heart animated iconito center"></i></a>
										<div class="cleaner social">
											<ul class="cleaner" id="sharer">
												<li class="cleaner"><a class="popup toanimated1 cleaner" href="javascript:void(0)" data-social='{"type":"facebook", "url":"http://mfyance.dhdinc.com/chocolate"}' title="Chocolate Express"><i class="animated icon-facebook"></i></a></li><!-- 
												--><li class="cleaner"><a class="popup toanimated1 cleaner" href="javascript:void(0)" data-social='{"type":"twitter", "url":"http://mfyance.dhdinc.com/chocolate", "text": "Chocolate Express"}' title="Chocolate Express"><i class="animated icon-twitter"></i></a></li><!-- 
												--><li class="cleaner"><a class="popup toanimated1 cleaner" href="javascript:void(0)" data-social='{"type":"plusone", "url":"http://mfyance.dhdinc.com/chocolate", "text": "Chocolate Express"}' title="Chocolate Express"><i class="animated icon-googleplus"></i></a></li><!-- 
												--><li class="cleaner"><a class="popup toanimated1 cleaner" href="javascript:void(0)" data-social='{"type":"pinterest", "url":"http://mfyance.dhdinc.com/chocolate", "text": "Chocolate Express", "image":"http://mfyance.dhdinc.com/chocolate/imagenes/logo.png"}' title="Chocolate Express"><i class="animated icon-pinterest"></i></a></li><!-- 
												--><li class="cleaner"><a class="toanimated1 cleaner" href="mailto: user@provider.com" target="_blank"><i class="animated icon-mail"></i></a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="cleaner descripcion">
								<p>nteger nec massa quis nunc vehicula elementum vitae at diam. Sed sit amet tellus vel ante sodales tristique eu ut sem. Mauris quis mi ut lorem molestie condimentum. Maecenas quis augue vel eros tempor maximus. Vestibulum porta ultricies leo, ut faucibus enim tincidunt id. Nullam consequat tincidunt metus, nec tristique nisi elementum eu. Donec interdum, odio at volutpat faucibus, augue elit interdum sem, sed eleifend dui justo eget quam.</p>
							</div>
							<div class="cleaner">
								<ul class="cleaner mylist-0">
									<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae ex varius, vulputate justo quis, tristique urna.</li>
									<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae ex varius, vulputate justo quis, tristique urna.</li>
									<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae ex varius, vulputate justo quis, tristique urna.</li>
								</ul>
							</div>
							<div class="cleaner space">
								<div class="cleaner tar">
									<div class="col-md-offset-4 cleaner">
										<div class="row cleaner">
											<div class="cleaner col-md-3 price pt7">
												<p>Precio: S./ 20.00</p>
											</div>
											<div class="cleaner col-md-5">
												<select name="cantidad" id="cantidad">
													<option></option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
												</select>
											</div>
											<div class="cleaner col-md-4 total pt7">
												<p><i class="icon-asterisk asterisk"></i>&nbsp;Total: S/. 200.00</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="myline2 cleaner tar igv pt7">
								<p>(*) No incluye envío</p>
							</div>
							<div class="thebut cleaner tac">
								<a href="javascript:void(0)" class="mybut but-style1 quierocomprar">
									<span class="cleaner">Quiero comprarlo</span>
								</a>
							</div>
						</div>
					</div>					
				</div>
			</div>			
		</article>
		<article id="opinion" class="cleaner maxw ">
			<div class="first cleaner">
				<div class="row cleaner">
					<div class="col-md-12 cleaner">
						<div class="titulo cleaner">
							<div class="capa cleaner">
								<div class="lineas cleaner">
									<h2>Encuentra el <strong>regalo perfecto</strong></h2>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
			<div class="second cleaner">
				<div class="dinamico cleaner">
					<!-- Este "div" se muestra, cuando el usuario NO está logeado -->
					<div class="nologeado cleaner">
						<h3><i class="icon-alert"></i>Debes <a href="login.php">ingresar</a> ó <a href="registro.php">registrarte</a> para poder insertar tu comentario.</h3>
					</div>
					<!-- Este "div" se muestra, cuando el usuario SI está logeado -->
					<div class="cleaner logeado">
						<form action="" id="frm-comentar" class="cleaner">
							<div class="textarea cleaner area-contador control-clone">
								<textarea name="comentario" id="comentario" class="comentario mytextarea style1" placeholder="Ingrese tu comentario"></textarea>
								<div id="numero_contador" class="contador cleaner"></div>
							</div>
							<div class="cleaner tac ">
								<button type="submit" class="clener mybut but-style1">
									<span class="cleaner">Publicar ahora</span>
								</button>
							</div>
						</form>
					</div>
				</div>
				<div id="resultados" class="cleaner">
					<div class="cleaner micomentario">
						<div class="tablita cleaner">
							<div class="cleaner">
								<div class="foto cleaner" style="background-image: url('http://koggdal.com/wp/wp-content/uploads/2011/07/johannes_koggdal.jpg')"></div>
							</div>
							<div class="cleaner texto">
								<div class="cleaner titulo">
									<p>Usuario N° 118900 dice:</p>
								</div>
								<div class="cleaner descripcion">
									<p>nteger nec massa quis nunc vehicula elementum vitae at diam. Sed sit amet tellus vel ante sodales tristique eu ut sem.Integer nec massa quis nunc vehicula elementum vitae at diam. </p>
								</div>
								<div class="cleaner fecha">
									<p>Sábado 30 Octubre 2014</p>
								</div>
							</div>
						</div>						
					</div>
					<div class="cleaner micomentario">
						<div class="tablita cleaner">
							<div class="cleaner">
								<div class="foto cleaner" style="background-image: url('http://koggdal.com/wp/wp-content/uploads/2011/07/johannes_koggdal.jpg')"></div>
							</div>
							<div class="cleaner texto">
								<div class="cleaner titulo">
									<p>Usuario N° 118900 dice:</p>
								</div>
								<div class="cleaner descripcion">
									<p>nteger nec massa quis nunc vehicula elementum vitae at diam. Sed sit amet tellus vel ante sodales tristique eu ut sem.Integer nec massa quis nunc vehicula elementum vitae at diam. </p>
								</div>
								<div class="cleaner fecha">
									<p>Sábado 30 Octubre 2014</p>
								</div>
							</div>
						</div>							
					</div>
					<div class="cleaner micomentario">
						<div class="tablita cleaner">
							<div class="cleaner">
								<div class="foto cleaner" style="background-image: url('http://koggdal.com/wp/wp-content/uploads/2011/07/johannes_koggdal.jpg')"></div>
							</div>
							<div class="cleaner texto">
								<div class="cleaner titulo">
									<p>Usuario N° 118900 dice:</p>
								</div>
								<div class="cleaner descripcion">
									<p>nteger nec massa quis nunc vehicula elementum vitae at diam. Sed sit amet tellus vel ante sodales tristique eu ut sem.Integer nec massa quis nunc vehicula elementum vitae at diam. </p>
								</div>
								<div class="cleaner fecha">
									<p>Sábado 30 Octubre 2014</p>
								</div>
							</div>
						</div>						
					</div>
				</div>
				<div class="cleaner tac">
					<a id="cargarmas" href="javascript:void(0)" class="mybut but-style2 ">
    					<span class="cleaner">Ver más comentarios</span>
    				</a>
				</div>
			</div>
		</article>
	</section>

	<!-- Productos favoritos -->
	<section id="favoritos" class="cleaner maxw movetrigger">
		<div class="row cleaner">
			<div class="col-md-12 cleaner">
				<div class="titulo cleaner">
					<div class="capa cleaner">					
						<div class="lineas cleaner flechas">
							<a href="javascript:void(0)" class="flecha toizq cleaner noresponsive"><i class="icon-arrow-left"></i></a>
							<h2>Los favoritos de <strong>Chocolate Express</strong></h2>
							<a href="javascript:void(0)" class="flecha toizq cleaner responsive"><i class="icon-arrow-left"></i></a>
							<a href="javascript:void(0)" class="flecha toder cleaner"><i class="icon-arrow-right"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row cleaner">
			<!-- Productos Favoritos -->			
			<?php include 'includes/productosfavoritos.php' ?>
			<!-- end -->
		</div>
	</section>	
	

	<!-- ==========================================
	{{fin del contenido principal}}
	=========================================== -->

	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<?php include 'includes/footer.php' ?>	
	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->

	<script>
		$(window).on({
			load: function(){
	           // Cargar Productos vistos/Favoritos para Vender {Footer}
				$.getJSON('json/productos.json', function(data) {
				    var template = $('#tmp-favoritos').html();
				    var info = Mustache.to_html(template, data);
				    $('#productosfavoritos').html(info);
				}).done(function(){					
					$('.productosfavoritos').owlCarousel({
					    items:5,
					    dots:false,
					    loop:true,
					    margin:30,
					    // autoHeight: true,
					    responsive:{
					        0:{
					            items:1
					        },
					        450:{
					            items:1
					        },
					        550:{
					            items:2
					        },
					        800:{
					            items:3
					        },
					        1000:{
					            items:4
					        },
					        1200:{
					            items:5
					        }
					    }
					});
					$('.productosfavoritos').removeClass('opacar').delay(10).promise().done(function(){						
						$('.like').on('click', function(){
							$(this).toggleClass('activo');					

							if($(this).hasClass('activo')){
								$(this).find('i').addClass('bounceIn');						
							}else{
								$(this).find('i').removeClass('bounceIn');
							}
						})
					});
				});
				    
			},
			resize: function(){
				$('#cargar-productos').on('refresh.owl.carousel', function(){
					$('.owl-height').removeAttr('style')
				});
			}
		});

		$(document).ready(function(){

			// Contador del textarea
			contador(200);

			$("#cantidad").select2({
            	placeholder: "Cantidad",
				allowClear: false
            });

            // Validar comentario
            $('form').submit(function(){            	
            	if($('#comentario').val()== ''){
            		$('#comentario').closest('.control-clone').addClass('error');
            		return false
            	}else{
            		$('#comentario').closest('.control-clone').removeClass('error');
            		return true
            	}
            })  
		
			// Cargar detalles del Producto
			var detalleproductos = {
				"productos":[
					{							
						"nombre" : "Nombre del producto - Vista 1",
						"foto": "fotos/producto1.jpg"
					},
					{							
						"nombre" : "Nombre del producto - Vista 2",
						"foto": "fotos/producto2.jpg"
					},
					{							
						"nombre" : "Nombre del producto - Vista 3",
						"foto": "fotos/producto3.jpg"
					},
					{							
						"nombre" : "Nombre del producto - Vista 4",
						"foto": "fotos/producto4.jpg"
					},
					{							
						"nombre" : "Nombre del producto - Vista 5",
						"foto": "fotos/producto5.jpg"
					},
					{							
						"nombre" : "Nombre del producto - Vista 6",
						"foto": "fotos/producto6.jpg"
					},
					{							
						"nombre" : "Nombre del producto - Vista 7",
						"foto": "fotos/producto7.jpg"
					},
					{							
						"nombre" : "Nombre del producto - Vista 8",
						"foto": "fotos/producto8.jpg"
					},
					{							
						"nombre" : "Nombre del producto - Vista 9",
						"foto": "fotos/producto9.jpg"
					}
				]
			}
			var template = $('#tmp-productos').html();
		    var info = Mustache.to_html(template, detalleproductos);
		    $('#cargar-productos').html(info).delay(500).promise().done(function(){	
	            // Zoom
				$('.zoom').zoom();				

				// Condicional
				var sliderThumbs = $("#cargar-productos");
				var counterFotos = $("#cargar-productos li").length;
				if (counterFotos > 0){					
					sliderThumbs.owlCarousel({
		                items: 3,
		                slideBy: 3,
		                dots: false,
		                autoHeight: true,
		                margin: 10,
		                slideSpeed: 880,
			            smartSpeed: 850,
			            lazyLoad: true,
			            loop: false,
		                responsive: {
		                    0: {
		                        items: 2
		                    },
		                    700: {
		                        items: 2
		                    },
		                    1200: {
		                        items: 3
		                    }
		                },
		                onInitialized: loadClick
		            });		                   

			        if(counterFotos > 3){			        	
			        	$('#milistaproductos .flecha').show();
			        }else{
			        	$('#milistaproductos .flecha').hide();
			        }
				}else{
					$('#cargar-productos').hide();
				}
		    });
            function loadClick(event) {
                $('.item').click(function (event) {
                    event.preventDefault();
                    var newimg = $(this).find('img').attr('src');
                    var newalt = $(this).find('img').attr('alt');
                    $('#photo').find('img').attr({
                    	'src': newimg,
                    	'alt': newalt
                    });
                    // Zoom
					$('#photo').zoom().delay(10).promise().done(function(){
						comprobarnavegador()
					});

                    var w = $(window).width();
                    if(w<992){
                    	var mytop = $('#photo').offset().top;
	                    $('html, body').stop().animate({
	                        scrollTop: mytop
	                    }, 200, 'swing');
                    }
                    
                });
            }


			// Agregar eventos si es Mobile
			setTimeout(function(){
				comprobarnavegador();
			},100)

		})
	</script>
</body>
</html>