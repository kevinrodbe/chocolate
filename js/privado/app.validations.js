;(function($,undefined){	
	var app = {
		validar: function(){

			$.validator.addMethod(
				"caracteres",
				function(value, element, regexp) {
					var re = new RegExp(regexp);
					return this.optional(element) || re.test(value);
				}
			);
			
			// {New Method}
			$.validator.addMethod(
				"caracteres",
				function(value, element, regexp) {
					var re = new RegExp(regexp);
					return this.optional(element) || re.test(value);
				}
			);
			$.validator.addMethod(
				"sololetras",
				function(value, element) {
					return this.optional(element) || /^[a-z]+$/i.test(value);
				}
			);			

			$.validator.addMethod("url", function(value, element) {
				return this.optional(element) || /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)*(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
			}, $.validator.messages.url);


			// No Permitir Cortar,Copiar, Pegar en determinados inputs
			$('.clave').on('cut copy paste', function(e){
				e.preventDefault();
			});

			// Agregar/Deshabilitar clases en plugin [select2]
		    $(".validar_tags").change(function() {
		        if($(this).closest('.control-clone').find('.select2-search-choice').length != 0){
		            $(this).closest('.control-clone').removeClass('error').addClass('success');
		        }else{
		            $(this).closest('.control-clone').removeClass('success').addClass('error');
		        }
		    });
		    $(".validar_select").change(function() {
		        if($(this).closest('.control-clone').find('.select2-allowclear').length != 0){
		            $(this).closest('.control-clone').removeClass('error').addClass('success');
		        }else{
		            $(this).closest('.control-clone').removeClass('success').addClass('error');
		        }
		    });
		}
	}
	$(document).on('ready', app.validar);
})(jQuery);