$(window).on({
	load: function(){

	},
	resize: function(){
		
	},
	scroll: function(){
		
	}
})

$(document).bind('click', function(e){
	
})

;(function($,undefined){
	"use strict";	

	$(document).one('ready', iniciar);

	// [1] {Función} Iniciar todo el contenido
	function iniciar(){


		// Ver la clave que pongo
		$('#verclave a').on('click', function(e){
			console.log('clik')
			$('#verclave').toggleClass('active');
			if($('#verclave').hasClass('active')){
				$('#verclave').find('input').attr('type','text')
			}else{
				$('#verclave').find('input').attr('type','password')
			}
			e.preventDefault();
		})

		// Buscador cabecera principal
		$('#frm-buscar').submit(function(){
			if($('#buscar').val().replace(/ /g, "")===""){
				return false
			}
			return true
		})
		// Libro de reclamaciones
		$('#verlibrodereclamaciones').on('click', function(){
			$('#popup.librodereclamaciones').addClass('active');
			$('input, textarea').placeholder();
		})
		$('#popup .close').on('click', function(){
			$('#popup.librodereclamaciones').removeClass('active');
			$('#popup.compraexitosa').removeClass('active');
		})

		// Agregar clase, cuando estamos en el dealle de un producto, y ya quiero comprarlo
		$('.quierocomprar').on('click', function(){
			$('#popup.compraexitosa').addClass('active')
		})

		// Agregar clase, cuando el producto se subió  carrito
		$('.tocarro').on('click', '.agregaralcarrito', function(){
			// $(this).closest('.producto').find('.accion').addClass('processing');
			$(this).closest('.agregarproducto').html('<img src="imagenes/loading.gif" />').delay(1100).promise().done(function(){
				$(this).closest('.producto').addClass('added');
			});			
		})

		// Menu
		$('#menuresponsive').on('click', function(){
			$(this).toggleClass('active')
			$('#opciones').slideToggle()
		})
		$('#menu-toggle').on('click', function(){
			$(this).toggleClass('active')
		})

		// Ver el anuncio para llamar
		$('#anuncio').on('click', function(){
			$(this).toggleClass('active')
		})
		
		// Mostrar Tooltip Menu		
		$('.user').mouseover(function(){
			$(this).closest('.touser').find('.tooltip').fadeIn()			
		})
		$('.tooltip').mouseleave(function(){
			$(this).closest('.touser').find('.tooltip').fadeOut()
		})
		$('#menu, #frm-buscar, #principal').mouseover(function(){	
			if($('.tooltip').is(':visible')){
				$('.tooltip').fadeOut()
			}
		})
	
		
		// Trigger para el carrusel Owl
		$('.toizq').on('click',function(){
		    $(this).closest('.movetrigger').find('.owl-prev').trigger('click');
		})
		$('.toder').on('click',function(){
			$(this).closest('.movetrigger').find('.owl-next').trigger('click');	
		})

		$('.liston .bg').hover(			
            function(){ $(this).find('img').addClass('pulse') },
            function(){ $(this).find('img').removeClass('pulse') }
        ); 

		// [x] Animación de iconos
		$('.toanimated1').hover(			
            function(){ $(this).find('i').addClass('bounceIn') },
            function(){ $(this).find('i').removeClass('bounceIn') }
        );       
        $('.toanimated2').hover(
            function(){ $(this).find('i').addClass('pulse') },
            function(){ $(this).find('i').removeClass('pulse') }
        );
        $('.toanimated3').hover(
            function(){ $(this).find('i').addClass('fadeInDown') },
            function(){ $(this).find('i').removeClass('fadeInDown') }
        );
        $('.toanimated4').hover(
            function(){ $(this).find('i').addClass('tada') },
            function(){ $(this).find('i').removeClass('tada') }
        );
        $('#manyape').hover(
            function(){ $(this).find('img').addClass('pulse') },
            function(){ $(this).find('img').removeClass('pulse') }
        );

        
        validacionesGenerales();
        carrito();
        compartir();
        comprobarnavegador();
     
	}	

})(jQuery);

function compartir(){
	// Compartir en productos
	$("#sharer a.popup").jqSocialSharer();			
	$('.sharing').on('click', function(){
		$(this).closest('.compartir').find('.social').toggleClass('activo')
	})
	$('.social').mouseleave(function(){
		$(this).removeClass('activo')
	})
	$('.like').on('click', function(){
		$(this).toggleClass('activo')
	})
}
function contador(caracteres){
	// Contador de comentario
    var max_caracteres = caracteres;
    var area_contador = $('#comentario');
    var numero_contador = $('#numero_contador');
    $(numero_contador).text('Máximo '+max_caracteres+' caracteres');
    $(area_contador).keyup(function(){
        var counter = $(this);
        if(counter.val().length > max_caracteres){
            counter.val( counter.val().substr(0, max_caracteres) );
        }else {
            $(numero_contador).text(max_caracteres-counter.val().length);
        }
    });
}

function carrito(){
	// Enviar comentario al obsequio
	$("input[name$='enviarmensaje']").change(function(){
		if($('#si').is(':checked')){
       		$('#frm-mensaje').slideDown();
        }else{
           $('#frm-mensaje').slideUp();
        }  		
	})

	$("input[name$='anonimo']").change(function(){
		if($(this).is(':checked')){
       		$(this).closest('#frm-mensaje').find('#elqueregala').val('Anónimo').attr('readonly','true');
        }else{
          $(this).closest('#frm-mensaje').find('#elqueregala').val('').removeAttr('readonly');
        }  		
	})

	// Icono de hora
	$('.triggertime').click(function(){
		$(this).closest('.minicaja').find('input').datetimepicker('show');
	})
	// Icono de calendario
	$('.triggerdate').click(function(){
		$(this).closest('.minicaja').find('input').datetimepicker('show');
	})

	// Time fecha para productos
	$("input[name*='cumple']").datetimepicker({				
		lang:'es',
		timepicker:false,
		format:'d/m/Y',
		formatDate:'d/m/Y'
	});
	$("input[name*='happy']").datetimepicker({				
		lang:'es',
		timepicker:false,
		format:'d/m/Y',
		formatDate:'d/m/Y',
		minDate: 0		
	});
	$("input[name*='fecha']").datetimepicker({				
		lang:'es',
		timepicker:false,
		format:'d/m/Y',
		formatDate:'d/m/Y',
		minDate: 0		
	});

	// Horas 
	$("input[name*='hora']").datetimepicker({
		lang:'es',
		datepicker:false,
		format:'H:i',
		allowTimes:[
			'07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00','16:00','17:00','18:00','19:00','20:00','21:00'
		]
	});
	// Selects
	$(".toselect").select2({
    	placeholder: "Cantidad",
		allowClear: false
    });

    // Validar los inputs de fechas y hora
	$("input[name*='cumple']").change(function(){
		if($("input[name*='cumple']").val()==''){
			$(this).closest('.control-clone').addClass('error')
		}else{
			$(this).closest('.control-clone').removeClass('error')
		}
	});
	$("input[name*='fecha']").change(function(){
		if($("input[name*='fecha']").val()==''){
			$(this).closest('.control-clone').addClass('error')
		}else{
			$(this).closest('.control-clone').removeClass('error')
		}
	});
	$("input[name*='hora']").change(function(){
		if($("input[name*='hora']").val()==''){
			$(this).closest('.control-clone').addClass('error')
		}else{
			$(this).closest('.control-clone').removeClass('error')
		}
	})
	

}


function capturarNombre(){
	// [Cloned]			
	$("#nombre").keyup(function(){
		if($(this).val()==""){
			$(this).closest('.control-clone').find('span.correcto').css('display','none');				
		}else{
			$(this).closest('.control-clone').find('span.correcto').css('display','block');
			var txtclonado = $(this).val();            		
    		$(this).closest('.control-clone').find('.name-cloned').html(txtclonado);
		}
	});

	if (localStorage) {
	 	if (localStorage.nombre) {
        	$("#nombre").val(localStorage.nombre);
        	$(".name-cloned").text(localStorage.nombre);
        	// class to input
        	$("#nombre").addClass('showName');
        }else{
        	$("#nombre").removeClass('showName');
        }

        $("#nombre").change(function(){
        	localStorage[$(this).attr("name")] = $(this).val();
        });
        $("#nombre").keyup(function () {
			localStorage[$(this).attr('name')] = $(this).val();					
		});
	}
}

function validacionesGenerales(){
	// Sucripción
	$('#frm-sucripcion').validate({				
		rules: {								
			correo:{
				required: true,
				email: true
			}	
		},
		messages: {		
			correo:{
				required: '&nbsp;',
				email: '&nbsp;'
			}
		},
		errorClass: "incorrecto",
		errorElement: "span",
		showErrors: function(errorMap, errorList){
		    var counterError = this.numberOfInvalids();					  
			$('.validacion').each(function(i, elem){
				if($(elem).val() == ''){
					$(elem).closest('.control-clone').addClass('error')
					counterError++;
				}else{
					$(elem).closest('.control-clone').removeClass('error')
				}
			});
			if(counterError == 1){
				$("#contenedor-alerta .texto span").html("Se presentó "+ counterError + " error.");						
			}else{
				$("#contenedor-alerta .texto span").html("Se presentaron "+ counterError + " errores.");
			}
			this.defaultShowErrors();
		},				
		unhighlight: function(element){
			$(element).closest('.control-clone').removeClass('error').addClass('success');
		},
		highlight: function(element){
			$(element).closest('.control-clone').removeClass('success').addClass('error');
		},
		invalidHandler: function (event, validator) {
	        var existeError = validator.numberOfInvalids();		            
	        if (existeError) {		            	
	        	$('body').addClass('frmError');  
	        	$("#contenedor-alerta").closest('body').addClass('showContenedor');
	        	$('button:submit').attr('disabled','disabled');
			  	setTimeout(function(){
			    	$('body').removeClass('showContenedor');
			    	$('button:submit').removeAttr('disabled');
			    }, 4900);

	        }else{
	        	$('body').removeClass('frmError');
	        }
	    },
		success: function(element){
			element.closest('body').removeClass('frmError').addClass('frmSuccess');
		}
	});

	// Libro de reclamaciones
	$('#frm-reclamaciones').validate({				
		rules: {
			nombre:{
				required: true,
				caracteres:"^[a-zA-Z\u00E1\u00E9\u00ED\u00F3\u00FA \u00C1\u00C9\u00CD\u00D3\u00DA \u00F1\u00D1]+$"
			},
			correo:{
				required: true,
				email: true
			},
			comentario:{
				required: true
			}
		},
		messages: {		
			nombre:{
				required: '&nbsp;',
				caracteres: '&nbsp;'
			},
			correo:{
				required: '&nbsp;',
				email: '&nbsp;'
			},
			comentario:{
				required: '&nbsp;',
			}
		},
		errorClass: "incorrecto",
		errorElement: "span",		
		highlight: function(element){
			$(element).closest('.control-clone').removeClass('success').addClass('error');
		},
		success: function(element){
			element.closest('.control-clone').removeClass('error').addClass('success');
		}
	});
}

function gotoTop(){
	$("#gototop").on('click',function(e){
		e.preventDefault();
		$("body,html").animate({
			scrollTop:0},1000);
	});
}

function comprobarnavegador() {
	console.log('Cargado comprobarnavegador')
    var is_chrome= navigator.userAgent.toLowerCase().indexOf('chrome/') > -1;
    var is_firefox = navigator.userAgent.toLowerCase().indexOf('firefox/') > -1;          
    var is_safari = navigator.userAgent.toLowerCase().indexOf('safari/') > -1;
    var is_opera = navigator.userAgent.toLowerCase().indexOf('opera/') > -1;
    var is_ie = navigator.userAgent.toLowerCase().indexOf('msie') > -1;

    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };

    if( isMobile.any() ){

    	// Productos favoritos {Index}
		$('#productosfavoritos .favoritos-but-responsive').removeClass('hidden');
		$('#productosfavoritos .bg').remove();
		$('#verlibrodereclamaciones').remove();
		$('#popup.librodereclamaciones').remove();
		$('#metodopago .help').remove();
		$('#banner .owl-controls').remove();
		$('#anuncio').remove();
		$('#photo').trigger('zoom.destroy');
		$('#photo').removeClass('zoom');
		$('#photo .zoomImg').remove()
		$('.flecha.toizq').remove();
		$('.flecha.toder').remove();
		

    }else{        	

    	switch(true){
            case (is_chrome):            		
	       		
                break;

            case (is_firefox):	                	

                break;

            case (is_safari):	                	

                break;

            case (is_opera):

                break;	                    

            default:
            	$('body').addClass('iexplorer');				

                break;
        }
       
    }            
}

function loadGmap(posLat, posLng) {
	// Latitud y Longitud de un lugar central
	this.latitud =  posLat;
	this.longitud =  posLng;

	// Bloquear enter del buscador
	$('#buscarUbicacion').keypress(function(event){
	    if (event.keyCode == 10 || event.keyCode == 13) 
	        event.preventDefault();
  	});
	var nameProyect = 'Chocolate Express';
	var placeIco = 'imagenes/place.png';

	// Detectar mi Ubicación inicial
	document.getElementById("latitud").value = posLat;
    document.getElementById("longitud").value = posLng;

	// Estilos
	var styles = [
	    {
  			stylers: [
				{hue:"#f3a848"},
				{ "saturation": -10 },
				{ "lightness": 10 }
	      	]
	    },
	    {
			featureType: "road",
			elementType: "geometry",
			stylers: [
				{ lightness: 10 },
				{ visibility: "simplified" }
			]
	    },
	    {
			featureType: "road",
			elementType: "labels",
			stylers: [
				{ visibility: "on" }
			]
	    }
  	];
  	var styledMap = new google.maps.StyledMapType(styles, {name:nameProyect});    	  	
	// Edit Ubication
	var miUbicacion = new google.maps.LatLng(posLat,posLng);
	// Opciones para la ubicacion en un aprimera vista
  	var mapOptions = {
		zoom: 16,
		center: miUbicacion,
		scrollwheel: false,
		mapTypeControlOptions: {
			mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
		}
  	};
  	// Cargar el mapa por 1era vez
  	var map = new google.maps.Map(document.getElementById('load-gmap'), mapOptions);
	
	// Autocomplete del buscador Google Map
	var input = (document.getElementById('buscarUbicacion'));
	var autocomplete = new google.maps.places.Autocomplete(input);
	autocomplete.bindTo('bounds', map);			
	
	// Crear marcador
	var marker = new google.maps.Marker({
		position: miUbicacion,
		map: map,
		title:nameProyect,
		icon: placeIco,
		draggable: true,
		animation: google.maps.Animation.DROP
	});

	google.maps.event.addListener(autocomplete, 'place_changed', function() {
		marker.setVisible(false);
		var place = autocomplete.getPlace();
		if (!place.geometry) {
		  return;
		}
		// If the place has a geometry, then present it on a map.
		if (place.geometry.viewport) {
	  		map.fitBounds(place.geometry.viewport);
		} else {
			map.setCenter(place.geometry.location);
			map.setZoom(17);  // Why 17? Because it looks good.
		}
		marker.setIcon(/** @type {google.maps.Icon} */({
			url: placeIco,
			draggable: true
		}));
		marker.setPosition(place.geometry.location);
		marker.setVisible(true);

		var address = '';
		if (place.address_components) {
			address = [
				(place.address_components[0] && place.address_components[0].short_name || ''),
				(place.address_components[1] && place.address_components[1].short_name || ''),
				(place.address_components[2] && place.address_components[2].short_name || '')
			].join(' ');
			// $('#latitud').val(place.geometry.location.lat());
   			// $('#longitud').val(place.geometry.location.lng());
            document.getElementById("latitud").value = place.geometry.location.lat();
	   		document.getElementById("longitud").value = place.geometry.location.lng();
		}
	});
	
	// Capturar la Longitud y Latitud   
	google.maps.event.addListener(marker, 'dragend', function (event) {
	    document.getElementById("latitud").value = this.getPosition().lat();
	    document.getElementById("longitud").value = this.getPosition().lng();
	});    		

	// Cargar estilos del mapa   
  	map.mapTypes.set('map_style', styledMap);
	map.setMapTypeId('map_style');
}
function loadGmap2(posLat, posLng) {

	this.latitud =  posLat;
	this.longitud =  posLng;

	// Posición/Ubicación actual
	var nameProyect = 'Chocolate Express';
	var placeIco = 'imagenes/place.png';

	// Detectar mi Ubicación inicial
	document.getElementById("latitud").value = posLat;
    document.getElementById("longitud").value = posLng;


	var miUbicacion = new google.maps.LatLng(posLat,posLng);
  	
  	// Opciones del mapa
  	var mapOptions = {
		zoom: 16,
		center: miUbicacion,
		scrollwheel: false,
		mapTypeControlOptions:{
			mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
		}
  	};
  	// Crear una nueva vista (estilos)
	var styles = [
	    {
  			stylers: [
				{hue:"#f3a848"},
				{ "saturation": -10 },
				{ "lightness": 10 }
	      	]
	    },
	    {
			featureType: "road",
			elementType: "geometry",
			stylers: [
				{ lightness: 10 },
				{ visibility: "simplified" }
			]
	    },
	    {
			featureType: "road",
			elementType: "labels",
			stylers: [
				{ visibility: "on" }
			]
	    }
  	];
  	// // Crear una nueva vista (nombre)
  	var styledMap = new google.maps.StyledMapType(styles, {name: nameProyect});

  	// Cargar el mapa
  	var map = new google.maps.Map(document.getElementById('load-gmap'), mapOptions);
  	map.mapTypes.set('map_style', styledMap);
	map.setMapTypeId('map_style');

	// Marcador
	var marker = new google.maps.Marker({
		position: miUbicacion,
		map: map,
		title:nameProyect,
		icon: placeIco,
		animation: google.maps.Animation.DROP,
		draggable: true
	});

	// Capturar la Longitud y Latitud   
	google.maps.event.addListener(marker, 'dragend', function (event) {
	    document.getElementById("latitud").value = this.getPosition().lat();
	    document.getElementById("longitud").value = this.getPosition().lng();
	});    		
}