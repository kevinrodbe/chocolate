<!doctype html>
<html lang="en">
<head>
	<!-- ===================================
	{{Chunk site_seo}}
	==================================== -->
	<?php include 'includes/seo.php' ;?>
	<!-- ===================================
	{{fin Chunk site_seo}}
	==================================== -->
	<meta name="description" content="Detalle del Producto">
    <meta property="og:title" content="[[*page_title]]" /> 
    <meta property="og:url" content="[[++site_url]][[~id]]" />
    <meta property="og:image" content="[[*page_imagen]]" />
    <meta property="og:description" content="[[*page_description]]" /> 
    <meta name="twitter:image:src" content="[[*page_imagen]]"/>
	<title>[[*page_title]]</title>	
</head>
<body>	
	<!-- ===================================
	{{Chunk site_header}}
	==================================== -->
	<?php include 'includes/header.php' ?>
	<!-- ===================================
	{{fin Chunk site_header}}
	==================================== -->

	<!-- ===================================
	{{Contenido principal}}
	==================================== -->
	<section id="principal" class="cleaner carritodecompras maxw container-fluid">
		<article id="pasos" class="cleaner maxw2 mt2">
			<div class="row cleaner">
				<div class="col-md-4 cleaner tac">
					<a href="carrito-1.php" class=""><i class="myicon-products"></i>Mis productos</a>
				</div>
				<div class="col-md-4 cleaner tac">
					<a href="carrito-2.php" class=""><i class="myicon-info"></i>Mis Datos</a>
				</div>
				<div class="col-md-4 cleaner tac">
					<a href="carrito-3.php" class="active"><i class="myicon-methods"></i>Métodos de pago</a>
				</div>
			</div>
		</article>
		<article id="article" class="cleaner">
			<form action="carrito-4.php" id="frm-pago" class="cleaner">
				<article id="mispedidos" class="cleaner row metodos ">
					<div class="col-md-12 cleaner">
						<div class="row cleaner">
							<div class="col-md-8 cleaner ">
						 		<article id="informacion" class="cleaner ">
						 			<div class="titulo cleaner">
										<div class="row cleaner">
											<div class="col-md-12 cleaner">
												<div class="titulo cleaner">
													<div class="capa cleaner">
														<div class="lineas cleaner">
															<h2>Información <strong>de invitado</strong></h2>
														</div>
													</div>
												</div> 
											</div>
										</div>
									</div>
									<div class="listadeproductos cleaner maxw2 row tospace">										
										<div class="cleaner col-md-12">
											<div class="row cleaner">
												<div class="cleaner tac">
													<h1>Facilitanos tus datos para comunicarte que tu regalo ha llegado a su destino.</h1>
												</div>
											</div>
											<div class="row cleaner">
												<div class="col-md-6 cleaner">
													<div class="control-clone cleaner mt">
														<input type="text" class="myinput style2 cleaner" placeholder="Ingresar nombre completo" id="nombre" name="nombre">
													</div>
													<div class="control-clone cleaner mt">
														<input type="text" class="myinput style2 cleaner" placeholder="Ingresa número fijo" id="fijo" name="fijo">
													</div>										
												</div>
												<div class="col-md-6 cleaner">
													<div class="control-clone cleaner mt">
														<input type="text" class="myinput style2 cleaner" placeholder="Ingresa correo" id="correo" name="correo">
													</div>
													<div class="control-clone cleaner mt">
														<input type="text" class="myinput style2 cleaner" placeholder="Ingresa número de celular" id="celular" name="celular">
													</div>										
												</div>												
											</div>						
										</div>				
									</div>

									<!-- DIV que se verá en caso de entrar como INVITADO -->
									<div class="content-check cleaner">
										<div class="cleaner">
											<input type="checkbox" name="crearusuario" id="crearusuario" value="1" class="check" >
											<label for="crearusuario"></label>
										</div>
										<div class="cleaner">
											<label for="crearusuario">¿Deseas crearte una cuenta de usuario?</label>
										</div>
									</div>
									<div id="crearmicuenta" class="row cleaner" style="display:none">
										<div class="col-md-6 cleaner">
											<div class="control-clone cleaner mt">
												<input type="text" class="myinput style2 cleaner" placeholder="Contraseña (Min: 6 caracteres)" id="clave1" name="clave1">
											</div>						
										</div>
										<div class="col-md-6 cleaner">											
											<div class="control-clone cleaner mt">
												<input type="text" class="myinput style2 cleaner" placeholder="Repertir contraseña" id="clave2" name="clave2">
											</div>										
										</div>																						
									</div>
									<!-- end DIV -->
						 		</article>
						 		<article id="mensaje" class="nadaproductos cleaner mt">
									<div class="row cleaner">
										<div class="col-md-12 cleaner">
											<div class="titulo cleaner">
												<div class="capa cleaner">
													<div class="lineas cleaner">
														<h2>¿Deseas boleta <strong>o factura</strong>?</h2>
													</div>
												</div>
											</div> 
										</div>
									</div>
									<div id="" class="row cleaner">
										<div class="content cleaner maxw3 ">
											<div class="col-md-6">
												<div class="content-radio cleaner tac ma">
													<div class="cleaner">
														<input type="radio" name="tipoderecibo" id="boleta" value="boleta" class="radio" >
														<label for="boleta"></label>
													</div>
													<div class="cleaner">
														<label for="boleta">Deseo boleta</label>
													</div>
												</div>						
											</div>	
											<div class="col-md-6">
												<div class="content-radio cleaner tac ma">
													<div class="cleaner">
														<input type="radio" name="tipoderecibo" id="factura" value="factura" class="radio">
														<label for="factura"></label>
													</div>
													<div class="cleaner">
														<label for="factura">Deseo factura</label>
													</div>
												</div>
											</div>	
											
										</div>									
										<div id="frm-mensaje" class="cleaner col-md-12 recibos">
											<div class="row cleaner">
												<div class="col-md-12 cleaner parafactura" style="display:none">
													<div class="row cleaner">
														<div class="col-md-6 cleaner">
															<div class="control-clone pb cleaner">
										                        <div class="cleaner">
										                            <input type="text" class="myinput style2 cleaner" placeholder="Ingresa el RUC?" id="ruc" name="ruc">
										                        </div>
										                    </div>
														</div>							
									                    <div class="col-md-6 cleaner">
															<div class="control-clone pb cleaner">
										                        <div class="cleaner">
										                            <input type="text" class="myinput style2 cleaner" placeholder="Ingresa tu razón social" id="razonsocial" name="razonsocial">
										                        </div>
										                    </div>
														</div>
													</div>
												</div>
												<div class="col-md-12 cleaner paraboleta" style="display:none">
													<div class="row cleaner">
														<div class="col-md-6 cleaner">
															<div class="control-clone pb cleaner">
										                        <div class="cleaner">
										                            <input type="text" class="myinput style2 cleaner" placeholder="Ingresar DNI" id="dni" name="dni">
										                        </div>
										                    </div>
														</div>						
									                   
													</div>
												</div>
											</div>
										</div>
									</div>
								</article>
								<article id="ubicacion" class="nadaproductos cleaner ">
									<div class="row cleaner">
										<div class="col-md-12 cleaner">
											<div class="titulo cleaner">
												<div class="capa cleaner">
													<div class="lineas cleaner">
														<h2>¿A dónde llevamos <strong>el recibo</strong>?</h2>
													</div>
												</div>
											</div> 
										</div>
									</div>
									<div class="row cleaner">
										<div class="col-md-12 cleaner">											
											<div id="mimapa" class="cleaner">
												<div class="cleaner">
													<input id="buscarUbicacion" class="myinput style2" type="text" placeholder="Buscar mi ubicación en el mapa (*)">					    
												</div>
												<div class="cleaner txt">
													<p>(*)Digite su ubicación y presionar la tecla "Enter" para encontrarla.</p>
												</div>
												<div class="cleaner">
													<div id="load-gmap" class="cleaner" style="width:100%;height:290px"></div>													
												</div>
												<div class="cleaner mt control-clone">
													<input id="direccion" name="direccion" type="text" class="myinput style2" placeholder="Indicar la dirección exacta del lugar" >
													<!-- Me muestra las coordenadas -->														
													<input type="hidden" name="latitud" id="latitud" value="">
													<input type="hidden" name="longitud" id="longitud" value="">
												</div>
												<!-- Esta opciòn solo se mostrará si en el Perfil, ha ingresado datos de su casa y domicilio -->										
												<div class="cleaner mt">
													<a id="cambiarentrega" href="javascript:void(0)" class="enlace"><i class="icon-eye"></i> Enviármelos a mi hogar (ver los datos)</a>
												</div>	
												<!-- end -->											
											</div>
										</div>						
									</div>
								</article>
								
								<article id="metodopago" class="nadaproductos cleaner">
									<div class="row cleaner">
										<div class="col-md-12 cleaner">
											<div class="titulo cleaner">
												<div class="capa cleaner">
													<div class="lineas cleaner">
														<h2>Elije tu <strong>método de pago</strong></h2>
													</div>
												</div>
											</div> 
										</div>
									</div>
									<div class="row cleaner">
										<div class="col-md-12 cleaner">
											<div class="metodo cleaner">
												<label for="metodo1" class="cleaner">
													<div class="mitable cleaner">
														<div class="cleaner logo padd" style="background-image: url(fotos/paypal.jpg)"></div>
														<div class="cleaner padd">
															<div class="content-radio cleaner ">
																<div class="cleaner">
																	<input type="radio" name="metodo" id="metodo1" value="metodo1" class="radio" checked>
																	<label for="metodo1"></label>
																</div>
																<div class="cleaner">
																	<label for="metodo1">Paga vía Paypal con tus tarjetas de crédito.</label>
																</div>
															</div>
														</div>
														<div class="cleaner">
															<div class="help cleaner">
																<i class="icon-help"></i>
																<div class="cont cleaner transition">
																	<p>Aquí va un texto descriptivo referente.</p>
																</div>
															</div>
														</div>
													</div>
												</label>
											</div>
											<div class="metodo cleaner">
												<label for="metodo2" class="cleaner">
													<div class="mitable cleaner">
														<div class="cleaner logo padd" style="background-image: url(fotos/safety.jpg)"></div>
														<div class="cleaner padd">
															<div class="content-radio cleaner ">
																<div class="cleaner">
																	<input type="radio" name="metodo" id="metodo2" value="metodo2" class="radio">
																	<label for="metodo2"></label>
																</div>
																<div class="cleaner">
																	<label for="metodo2">Haz la transacción mediante depósito o transferencia bancaria.</label>
																</div>
															</div>
														</div>
														<div class="cleaner">
															<div class="help cleaner">
																<i class="icon-help"></i>
																<div class="cont cleaner transition">
																	<p>Aquí va un texto descriptivo referente al método de pago.</p>
																</div>
															</div>
														</div>
													</div>
												</label>
											</div>
											<div class="metodo cleaner">
												<label for="metodo3" class="cleaner">
													<div class="mitable cleaner">
														<div class="cleaner logo padd" style="background-image: url(fotos/efectivo.jpg)"></div>
														<div class="cleaner padd">
															<div class="content-radio cleaner ">
																<div class="cleaner">
																	<input type="radio" name="metodo" id="metodo3" value="metodo3" class="radio">
																	<label for="metodo3"></label>
																</div>
																<div class="cleaner">
																	<label for="metodo3">Haz la transacción mediante depósito o transferencia bancaria.</label>
																</div>
															</div>
														</div>
														<div class="cleaner">
															<div class="help cleaner">
																<i class="icon-help"></i>
																<div class="cont cleaner transition">
																	<p>Aquí va un texto descriptivo referente al método de pago.</p>
																</div>
															</div>
														</div>
													</div>
												</label>
											</div>
											<div class="metodo cleaner">
												<label for="metodo4" class="cleaner">
													<div class="mitable cleaner">
														<div class="cleaner logo padd" style="background-image: url(fotos/bcp.jpg)"></div>
														<div class="cleaner padd">
															<div class="content-radio cleaner ">
																<div class="cleaner">
																	<input type="radio" name="metodo" id="metodo4" value="metodo4" class="radio">
																	<label for="metodo4"></label>
																</div>
																<div class="cleaner">
																	<label for="metodo4">Haz la transacción mediante depósito o transferencia bancaria.</label>
																</div>
															</div>
														</div>
														<div class="cleaner">
															<div class="help cleaner">
																<i class="icon-help"></i>
																<div class="cont cleaner transition">
																	<p>Aquí va un texto descriptivo referente al método de pago.</p>
																</div>
															</div>
														</div>
													</div>
												</label>
											</div>
										</div>
									</div>
								</article>
								<article id="fin" class="cleaner myline2 ">
									<div class="control-clone cleaner">
				                        <div class="content-check cleaner ma tac">
											<div class="cleaner">
												<input type="checkbox" name="terminosycondiciones" id="terminosycondiciones" value="1" class="check" checked>
												<label for="terminosycondiciones"></label>
											</div>
											<div class="cleaner">
												<label for="terminosycondiciones">Acepto los términos y condiciones</label>
											</div>
										</div>
				                    </div>
				                   <div class="cleaner mt ma tac">
										<button type="submit" class="animated mybut but-style1">
											<span class="cleaner">Finalizar compra</span>
										</button>
									</div>
								</article>
							</div>
							<div class="col-md-4 cleaner myline3">
								<div id="titulo" class="cleaner">
									<div class="row cleaner">
										<div class="col-md-12 cleaner">
											<div class="titulo cleaner">
												<div class="capa cleaner">
													<div class="lineas cleaner">
														<h2>Esto es lo que <strong>llevaré</strong></h2>
													</div>
												</div>
											</div> 
										</div>
									</div>
								</div>
								<div class="listadeproductos cleaner maxw2">
									<div class="producto counterproduct cleaner animated">
										<div class="row cleaner">
											<div class="cleaner col-md-3 col-xs-4">
												<div class="fotoproducto cleaner">
													<img src="fotos/producto.jpg" alt="" class="cleaner">
												</div>							
											</div>
											<div class="cleaner vat col-md-9 col-xs-8">	
												<div class="cleaner titulo">
													<h2>Nombre del Producto <span>(S/ 35.00)</span></h2>
												</div>
												<div class="cleaner">
													<label>Precio: S./20.00</label>
												</div>										
											</div>
										</div>
									</div>
									<div class="producto counterproduct cleaner animated">
										<div class="row cleaner">
											<div class="cleaner col-md-3 col-xs-4">
												<div class="fotoproducto cleaner">
													<img src="fotos/producto.jpg" alt="" class="cleaner">
												</div>							
											</div>
											<div class="cleaner vat col-md-9 col-xs-8">	
												<div class="cleaner titulo">
													<h2>Nombre del Producto <span>(S/ 35.00)</span></h2>
												</div>
												<div class="cleaner">
													<label>Precio: S./20.00</label>
												</div>										
											</div>
										</div>
									</div>			
								</div>
								<div class="total cleaner">
									<div class="tabla cleaner">
										<div class="cleaner"><p>Subotal:</p></div>
										<div class="cleaner"><p>S./60.00</p></div>
									</div>
									<div class="tabla cleaner">
										<div class="cleaner"><p>Envío:</p></div>
										<div class="cleaner"><p>S./60.00</p></div>
									</div>
									<div class="tabla cleaner">
										<div class="cleaner"><p>Dscto:</p></div>
										<div class="cleaner"><p>S./0.00</p></div>
									</div>
									<div class="tabla cleaner">
										<div class="cleaner final"><em>Total:</em></div>
										<div class="cleaner final"><em>S./0.00</em></div>
									</div>
								</div>
							</div>
						</div>
					</div>			
				</article>
			</form>
		</article>
	</section>

	<!-- ==========================================
	{{fin del contenido principal}}
	=========================================== -->

	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<?php include 'includes/footer.php' ?>	
	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
	<script>
		$(window).on({
			load: function(){
				// Primera carga, con coordenadas por defecto al establecimiento de Chocolate Express
				var aquiestaChocolateExpress = new loadGmap(-12.127498, -77.028022);
			},
			resize: function(){				
			}
		});

		$(document).ready(function(){
			// Tipo de recibo
			$("input[name$='tipoderecibo']").change(function(){
				if($('#factura').is(':checked')){		       		
		       		$('#frm-mensaje .paraboleta').hide();
		       		$('#frm-mensaje .parafactura').show();
		       		console.log('factura')
		       		$('#dni').attr('disabled','disabled');
		       		$('#razonsocial').removeAttr('disabled');
		       		$('#ruc').removeAttr('disabled');
		        }else{		           	
		           	$('#frm-mensaje .parafactura').hide();
		       		$('#frm-mensaje .paraboleta').show();
		       		console.log('boleta')
		       		$('#dni').removeAttr('disabled');
		       		$('#razonsocial').attr('disabled','disabled');
		       		$('#ruc').attr('disabled','disabled');
		        }  		
			})

			// Ver mi direcciòn ingresada en mi perfil	
			$("#cambiarentrega").on('click',function(){
				var aquiestamicasa = new loadGmap(-12.093767, -77.052993);
				$('#direccion').val('Mz s lote 1 calle las MOras Urb Ceres (estos datos son capturados del perfil del usuario, si es que lo hay)')
			});
			// end

			$("#crearusuario").change(function(){
				if($(this).is(':checked')){
					$('#crearmicuenta').slideDown(500);
				}else{
					$('#crearmicuenta').slideUp(500);
					$('#crearmicuenta').find('.error').removeClass('error')
				}
			})

			$("#terminosycondiciones").change(function(){
				if($(this).is(':checked')){
					$('button:submit').removeAttr('disabled');
				}else{					
					$('button:submit').attr('disabled','disabled');
				}
			})

			$('#frm-pago').validate({
				rules: {		
					nombre: {
						required: true,
						sololetras: true
					},
					correo: {
						required: true,
						email: true,
						'remote': {
	                        url: 'process.php',
	                        type: 'post',
	                        data: {
	                            correo: function(e){                                    
                                    return $('input[name="correo"]').val();
                                }
                            }
                        }
					},
					celular: {
						required: true,
						digits: true
					},
					fijo: {
						required: true,
						digits: true
					},
					ruc: {
						required: true,
						digits: true,
						minlength: 11,
						maxlength: 11
					},
					razonsocial: {
						required: true
					},
					dni: {
						required: true,
						digits: true,
						minlength: 8,
						maxlength: 8
					},
					clave1: {
						required: true,
						minlength: 6
					},
					clave2: {
						required: true,
						equalTo: '#clave1'
					},
					direccion:{
						required: true
					}
				},
				messages: {					
					nombre: {
						required: '&nbsp;',
						sololetras: '&nbsp;'
					},
					correo: {
						required: '&nbsp;',
						email: '&nbsp;',
						'remote' : '&nbsp;'
					},
					celular: {
						required: '&nbsp;',
						digits: '&nbsp;'
					},
					fijo: {
						required: '&nbsp;',
						digits: '&nbsp;'
					},
					ruc: {
						required: '&nbsp;',
						digits: '&nbsp;',
						minlength: '&nbsp;',
						maxlength: '&nbsp;'
					},
					razonsocial: {
						required: '&nbsp;'
					},
					dni: {
						required: '&nbsp;',
						digits: '&nbsp;',
						minlength: '&nbsp;',
						maxlength: '&nbsp;'
					},
					clave1:{
						required: '&nbsp;',
						minlength: '&nbsp;'
					},
					clave2:{
						required: '&nbsp;',
						equalTo: '&nbsp;'
					},
					direccion:{
						required: '&nbsp;'
					}
				},
				errorClass: "incorrecto",
				errorElement: "span",
				showErrors: function(errorMap, errorList){
				    var counterError = this.numberOfInvalids();
					if(counterError == 1){
						$("#contenedor-alerta .texto span").html("Se presentó "+ counterError + " error.");	
						this.defaultShowErrors();					
					}else{
						$("#contenedor-alerta .texto span").html("Se presentaron "+ counterError + " errores.");
						this.defaultShowErrors();
					}					
				},				
				unhighlight: function(element){
					$(element).closest('.control-clone').removeClass('error').addClass('success');
				},
				highlight: function(element){
					$(element).closest('.control-clone').removeClass('success').addClass('error');
				},
				invalidHandler: function (event, validator) {
		            var existeError = validator.numberOfInvalids();		            
		            if (existeError) {		            	
		            	$('body').addClass('frmError');  
		            	$("#contenedor-alerta").closest('body').addClass('showContenedor');
		            	$('button:submit').attr('disabled','disabled');
					  	setTimeout(function(){
					    	$('body').removeClass('showContenedor');
					    	$('button:submit').removeAttr('disabled');
					    }, 4900);
		            }else{
		            	$('body').removeClass('frmError');
		            }
		        },
				success: function(element){
					element.closest('body').removeClass('frmError').addClass('frmSuccess');
				},
				submitHandler: function (form) {
					console.log($(form).serialize())
                }
			});		    
		})

		
		
	</script>	
</body>
</html>