<!doctype html>
<html lang="en">
<head>
	<!-- ===================================
	{{Chunk site_seo}}
	==================================== -->
	<?php include 'includes/seo.php' ;?>
	<!-- ===================================
	{{fin Chunk site_seo}}
	==================================== -->
	<meta name="description" content="Detalle del Producto">
    <meta property="og:title" content="[[*page_title]]" /> 
    <meta property="og:url" content="[[++site_url]][[~id]]" />
    <meta property="og:image" content="[[*page_imagen]]" />
    <meta property="og:description" content="[[*page_description]]" /> 
    <meta name="twitter:image:src" content="[[*page_imagen]]"/>
	<title>[[*page_title]]</title>	
	<style>
		#load-gmap.active{height: 290px;margin-top:30px;}
	</style>
</head>
<body>	
	<!-- ===================================
	{{Chunk site_header}}
	==================================== -->
	<?php include 'includes/header.php' ?>
	<!-- ===================================
	{{fin Chunk site_header}}
	==================================== -->

	<!-- ===================================
	{{Contenido principal}}
	==================================== -->
	<section id="principal" class="cleaner perfil maxw container-fluid">
		
		<article id="mispedidos" class="cleaner row mt2 maxw2 ma mb">
			<div class="col-md-12 cleaner">
		 		
				<div class="listadeproductos cleaner row ">
					<div class="col-md-4">
						<?php include 'includes/menu_perfil.php' ?>
					</div>
					<div class="col-md-8">								
						<div class="tospace cleaner">
							<div id="titulo" class="cleaner">
								<div class="row cleaner">
									<div class="col-md-12 cleaner">
										<div class="titulo cleaner">
											<div class="capa cleaner">
												<div class="lineas cleaner">
													<h2>Editar mis<strong> datos</strong></h2>
												</div>
											</div>
										</div> 
									</div>
								</div>
							</div>
							<div class="cleaner ">
								<form action="perfil.php" id="frm-perfil" class="cleaner">
									<div class="row cleaner">
										<div class="col-md-12 cleaner">
											<div class="row cleaner">
												<div class="col-md-6 cleaner">
													<div class="control-clone cleaner mt">
														<input type="text" class="myinput style2 cleaner" placeholder="Nombre completo" id="nombres" name="nombres">
													</div>													
												</div>
												<div class="col-md-6 cleaner">
													<div class="control-clone cleaner mt">
														<input type="text" class="myinput style2 cleaner" placeholder="Correo electrónico" id="correo" name="correo">
													</div>													
												</div>
											</div>
										</div>
										<div class="col-md-12 cleaner">
											<div class="row cleaner">
												<div class="col-md-6 cleaner">
													<div class="control-clone cleaner mt">
														<input type="password" class="myinput style2 cleaner" placeholder="Teléfono" id="telefono" name="telefono">
													</div>
												</div>
												<div class="col-md-6 cleaner">
													<div class="control-clone cleaner mt">
														<input type="password" class="myinput style2 cleaner" placeholder="Celular" id="celular" name="celular">
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-12 cleaner">
											<div class="row cleaner">
												<div class="col-md-6 cleaner  toicon ">
													<div class="control-clone cleaner minicaja mt">
														<div class="cleaner">
															<input id="cumple1" name="cumple1" type="text" class="myinput style2 dib validacion" placeholder="Fecha de cumpleaños" >
															<i class="myicon-calendar triggerdate animated"></i>
														</div>
													</div>
												</div>

												<div class="col-md-6 cleaner">
													<div class="control-clone cleaner mt">
														<select name="distrito" id="distrito" class="toselect"></select>
													</div>
													<!-- Mustache Template - Distritos -->
													<script id="tmp-distrito" type="text/template">
														<option></option>
														{{#distritos}}				
															<option id="distrito-{{nombre}}" value="{{nombre}}" data-latitud="{{latitud}}" data-longitud="{{longitud}}">{{nombre}}</option>				
														{{/distritos}}
													</script>
												</div>												
											</div>
										</div>
										<div class="col-md-12 cleaner  ">										
											<div id="load-gmap" class="cleaner" style="width:100%;"></div>
											<!-- Me muestra las coordenadas -->														
											<input type="hidden" name="latitud" id="latitud" value="">
											<input type="hidden" name="longitud" id="longitud" value="">												
										</div>
										<div class="col-md-12 cleaner mt control-clone">
											<input id="direccion" name="direccion" type="text" class="myinput style2" placeholder="Indicar la dirección exacta de tu domicilio" >
										</div>
								
										<div class="col-md-12 cleaner">
											<div class="cleaner mt ma tac">
												<button type="submit" class="cleaner animated mybut but-style1">
													<span class="cleaner">Actualizar información</span>
												</button>
											</div>
										</div>
									</div>
									
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</article>	
	</section>

	<!-- ==========================================
	{{fin del contenido principal}}
	=========================================== -->

	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<?php include 'includes/footer.php' ?>	
	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
	<script>
		$(window).on({
			load: function(){							
			},
			resize: function(){				
			}
		});

		$(document).ready(function(){
			$('select').change(function(){			    
			    var lat = $(this).find(':selected').attr('data-latitud');
			    var lng = $(this).find(':selected').attr('data-longitud');			    
			    var aquiestamicasa = new loadGmap2(lat,lng);
			    $('#load-gmap').addClass('active')
			});							
			
			// Listar los Distritos
		    $.getJSON('json/distritos.json', function(data) {
			    var template = $('#tmp-distrito').html();
			    var info = Mustache.to_html(template, data);
			    $('#distrito').html(info);
			}).done(function(){
				$("#distrito").select2({
			    	placeholder: "Distrito donde vives",
					allowClear: false
			    });
			});

			$('#frm-perfil').validate({
				rules: {						
					correo: {
						required: true,
						email: true
					},
					nombres: {
						required: true,
						sololetras: true
					}
				},
				messages: {					
					correo: {
						required: '&nbsp;',
						email: '&nbsp;'
					},
					nombres: {
						required: '&nbsp;',
						sololetras: '&nbsp;'
					}
				},
				errorClass: "incorrecto",
				errorElement: "span",
				showErrors: function(errorMap, errorList){
				    var counterError = this.numberOfInvalids();
					if(counterError == 1){
						$("#contenedor-alerta .texto span").html("Se presentó "+ counterError + " error.");	
						this.defaultShowErrors();					
					}else{
						$("#contenedor-alerta .texto span").html("Se presentaron "+ counterError + " errores.");
						this.defaultShowErrors();
					}					
				},				
				unhighlight: function(element){
					$(element).closest('.control-clone').removeClass('error').addClass('success');
				},
				highlight: function(element){
					$(element).closest('.control-clone').removeClass('success').addClass('error');
				},
				invalidHandler: function (event, validator) {
		            var existeError = validator.numberOfInvalids();		            
		            if (existeError) {		            	
		            	$('body').addClass('frmError');  
		            	$("#contenedor-alerta").closest('body').addClass('showContenedor');
		            	$('button:submit').attr('disabled','disabled');
					  	setTimeout(function(){
					    	$('body').removeClass('showContenedor');
					    	$('button:submit').removeAttr('disabled');
					    }, 4900);
		            }else{
		            	$('body').removeClass('frmError');
		            }
		        },
				success: function(element){
					element.closest('body').removeClass('frmError').addClass('frmSuccess');
				},
				submitHandler: function (form) {
					console.log($(form).serialize())
					window.location.href = 'perfil.php';
                }
			});		    
		})

		
	</script>	
</body>
</html>