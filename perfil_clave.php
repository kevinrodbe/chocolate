<!doctype html>
<html lang="en">
<head>
	<!-- ===================================
	{{Chunk site_seo}}
	==================================== -->
	<?php include 'includes/seo.php' ;?>
	<!-- ===================================
	{{fin Chunk site_seo}}
	==================================== -->
	<meta name="description" content="Detalle del Producto">
    <meta property="og:title" content="[[*page_title]]" /> 
    <meta property="og:url" content="[[++site_url]][[~id]]" />
    <meta property="og:image" content="[[*page_imagen]]" />
    <meta property="og:description" content="[[*page_description]]" /> 
    <meta name="twitter:image:src" content="[[*page_imagen]]"/>
	<title>[[*page_title]]</title>		
</head>
<body>	
	<!-- ===================================
	{{Chunk site_header}}
	==================================== -->
	<?php include 'includes/header.php' ?>
	<!-- ===================================
	{{fin Chunk site_header}}
	==================================== -->

	<!-- ===================================
	{{Contenido principal}}
	==================================== -->
	<section id="principal" class="cleaner perfil maxw container-fluid">
		
		<article id="mispedidos" class="cleaner row mt2 maxw2 ma mb">
			<div class="col-md-12 cleaner">		 		
				<div class="listadeproductos cleaner row ">
					<div class="col-md-4">
						<?php include 'includes/menu_perfil.php' ?>
					</div>
					<div class="col-md-8">								
						<div class="tospace cleaner">
							<div id="titulo" class="cleaner">
								<div class="row cleaner">
									<div class="col-md-12 cleaner">
										<div class="titulo cleaner">
											<div class="capa cleaner">
												<div class="lineas cleaner">
													<h2>Cambiar mi<strong> contraseña</strong></h2>
												</div>
											</div>
										</div> 
									</div>
								</div>
							</div>
							<div class="cleaner ">
								<form action="perfil.php" id="frm-perfil" class="cleaner">
									<div class="row cleaner">
										<div class="col-md-12 cleaner">
											<div class="row cleaner">
												<div class="col-md-6 cleaner">
													<div class="control-clone cleaner mt">
														<input type="text" class="myinput style2 cleaner" placeholder="Contraseña actual" id="clave1" name="clave1">
													</div>													
												</div>
												<div class="col-md-6 cleaner">
													<div class="control-clone cleaner mt">
														<input type="text" class="myinput style2 cleaner" placeholder="Nueva contraseña" id="clave2" name="clave2">
													</div>													
												</div>
											</div>
										</div>
										<div class="col-md-12 cleaner">
											<div class="row cleaner">
												<div class="col-md-6 cleaner">
													<div class="control-clone cleaner mt">
														<input type="password" class="myinput style2 cleaner" placeholder="Repertir su nueva contraseña" id="clave3" name="clave3">
													</div>
												</div>												
											</div>
										</div>
										<div class="col-md-12 cleaner">
											<div class="cleaner mt ma tac">
												<button type="submit" class="cleaner animated mybut but-style1">
													<span class="cleaner">Cambiar contraseña</span>
												</button>
											</div>
										</div>
									</div>
									
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</article>	
	</section>

	<!-- ==========================================
	{{fin del contenido principal}}
	=========================================== -->

	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<?php include 'includes/footer.php' ?>	
	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<script>
		$(window).on({
			load: function(){							
			},
			resize: function(){				
			}
		});

		$(document).ready(function(){
			$('#frm-perfil').validate({
				rules: {						
					clave1: {
						required: true
					},
					clave2: {
						required: true
					},
					clave3: {
						required: true,
						equalTo: '#clave2'
					}
				},
				messages: {					
					clave1: {
						required: '&nbsp;',
						email: '&nbsp;'
					},
					clave2: {
						required: '&nbsp;'
					},
					clave3: {
						required: '&nbsp;',
						equalTo: '&nbsp;'
					}
				},
				errorClass: "incorrecto",
				errorElement: "span",
				showErrors: function(errorMap, errorList){
				    var numErrores = this.numberOfInvalids();
					if(numErrores == 1){
						$("#contenedor-alerta .texto span").html("Se presentó "+ this.numberOfInvalids() + " error.");
					    $("#contenedor-alerta").addClass('block');
					    setTimeout(function(){
					    	 $("#contenedor-alerta").removeClass('block');
					    }, 0)   
					    this.defaultShowErrors();
					}else{
						$("#contenedor-alerta .texto span").html("Se presentaron "+ this.numberOfInvalids() + " errores.");
					    $("#contenedor-alerta").addClass('block');
					    setTimeout(function(){
					    	 $("#contenedor-alerta").removeClass('block');
					    }, 0)  
					    this.defaultShowErrors();	
					}
				},
				unhighlight: function(element, errorClass) {
				    var numberOfInvalids = this.numberOfInvalids();				    
				    if (numberOfInvalids === 0) {
				        $(element).closest('body').find("#contenedor-alerta").removeClass('block');
				    }else{
				    	$(element).closest('body').find("#contenedor-alerta").addClass('block');
				    }   
				},
				highlight: function(element){
					$(element).closest('.control-clone').removeClass('success').addClass('error');
				},
				success: function(element){
					element.closest('.control-clone').removeClass('error').addClass('success');
				},
				submitHandler: function (form) {
					             
                }
			});		    
		})
	</script>	
</body>
</html>