<!doctype html>
<html lang="en">
<head>
	<!-- ===================================
	{{Chunk site_seo}}
	==================================== -->
	<?php include 'includes/seo.php' ;?>
	<!-- ===================================
	{{fin Chunk site_seo}}
	==================================== -->
	<meta name="description" content="Detalle del Producto">
    <meta property="og:title" content="[[*page_title]]" /> 
    <meta property="og:url" content="[[++site_url]][[~id]]" />
    <meta property="og:image" content="[[*page_imagen]]" />
    <meta property="og:description" content="[[*page_description]]" /> 
    <meta name="twitter:image:src" content="[[*page_imagen]]"/>
	<title>[[*page_title]]</title>
</head>
<body>	
	<!-- ===================================
	{{Chunk site_header}}
	==================================== -->
	<?php include 'includes/header.php' ?>
	<!-- ===================================
	{{fin Chunk site_header}}
	==================================== -->

	<!-- ===================================
	{{Contenido principal}}
	==================================== -->
	<section id="principal" class="cleaner perfil maxw container-fluid">
		
		<article id="miscompras" class="cleaner row mt2 maxw2 ma mb">
			<div class="col-md-12 cleaner">		 		
				<div class="listadeproductos cleaner row ">
					<div class="col-md-4">
						<?php include 'includes/menu_perfil.php' ?>
					</div>
					<div class="col-md-8">								
						<div class="tospace cleaner">
							<div id="titulo" class="cleaner">
								<div class="row cleaner">
									<div class="col-md-12 cleaner">
										<div class="titulo cleaner">
											<div class="capa cleaner">
												<div class="lineas cleaner">
													<h2>Mis productos<strong> comprados</strong></h2>
												</div>
											</div>
										</div> 
									</div>
								</div>
							</div>
							<div class="cleaner ">
								<ul id="comprasrealizadas" class="cleaner productosfavoritos grid effect-6 load-posts">
									<!-- Carga de la galeria de productos -->			
								</ul>
								<script id="tmp-miscompras" type="text/template">
									{{#productos}}				
										<li class="col-productos cleaner mb ">
											<div class="cleaner pr">
												<div class="cleaner item">
													<div class="producto cleaner">
														<div class="foto cleaner">
															<img src="{{foto}}" alt="{{nombreproducto}}">
														</div>
														<div class="bg cleaner transition">
															<div class="content cleaner">
																<div class="cleaner descripcion">																						
																	<div class="cleaner tobottom">
																		<a href="producto.php" class=" mybut but-style1">
																			<span class="cleaner">Ver producto</span>
																		</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="cleaner hidden favoritos-but-responsive">
														<a href="javascript:void(0)" class="mybut but-style1">
															<span class="cleaner">Ver producto</span>
														</a>
													</div>
													<div class="cleaner nombre">													
														<div class="cleaner tal txt"><p>{{nombreproducto}}</p></div>
													</div>
													<div class="cleaner nombre">
														<div class="cleaner"><h4>Precio: {{precio}}</h4></div>
														<div class="cleaner"><h4>Fecha: {{precio}}</h4></div>
														<div class="cleaner"><h4>Estado: {{modalidad}}</h4></div>
													</div>												
												</div>		
											</div>		
										</li>
									{{/productos}}
								</script>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</article>	
	</section>

	<!-- ==========================================
	{{fin del contenido principal}}
	=========================================== -->

	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<?php include 'includes/footer.php' ?>	
	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<!-- {Galeria dinamica - Codrops} -->
	<script src="js/load_scrolling/classie.min.js"></script>
	<script src="js/load_scrolling/modernizr.custom.2.js"></script>
	<script src="js/load_scrolling/masonry.pkgd.min.js"></script>
	<script src="js/load_scrolling/imagesloaded.js"></script>
	<script src="js/load_scrolling/AnimOnScroll.js"></script>
	<script>
		$(window).on({
			load: function(){
				// Cargar Productos para Vender {Footer}
				$.getJSON('json/productos.json', function(data) {
				    var template = $('#tmp-miscompras').html();
				    var info = Mustache.to_html(template, data);
				    $('#comprasrealizadas').html(info);
				}).done(function(){					
					
					new AnimOnScroll( document.getElementById( 'comprasrealizadas' ), {
						minDuration : 0.4,
						maxDuration : 0.7,
						viewportFactor : 0.2
					});				

					// Agregar eventos si es Mobile
					setTimeout(function(){
						comprobarnavegador();
					},100)
				});
						
			},
			resize: function(){				
			}
		});

		$(document).ready(function(){
			
				    
		})

		
	</script>	
</body>
</html>