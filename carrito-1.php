<!doctype html>
<html lang="en">
<head>
	<!-- ===================================
	{{Chunk site_seo}}
	==================================== -->
	<?php include 'includes/seo.php' ;?>
	<!-- ===================================
	{{fin Chunk site_seo}}
	==================================== -->
	<meta name="description" content="Detalle del Producto">
    <meta property="og:title" content="[[*page_title]]" /> 
    <meta property="og:url" content="[[++site_url]][[~id]]" />
    <meta property="og:image" content="[[*page_imagen]]" />
    <meta property="og:description" content="[[*page_description]]" /> 
    <meta name="twitter:image:src" content="[[*page_imagen]]"/>
	<title>[[*page_title]]</title>	
</head>
<body>	
	<!-- ===================================
	{{Chunk site_header}}
	==================================== -->
	<?php include 'includes/header.php' ?>
	<!-- ===================================
	{{fin Chunk site_header}}
	==================================== -->

	<!-- ===================================
	{{Contenido principal}}
	==================================== -->
	<section id="principal" class="cleaner carritodecompras maxw container-fluid">
		<article id="pasos" class="cleaner maxw2 mt2 ">
			<div class="row cleaner">
				<div class="col-md-4 cleaner tac">
					<a href="carrito-1.php" class="active"><i class="myicon-products"></i>Mis productos</a>
				</div>
				<div class="col-md-4 cleaner tac">
					<a href="carrito-2.php" class=""><i class="myicon-info"></i>Mis Datos</a>
				</div>
				<div class="col-md-4 cleaner tac">
					<a href="carrito-3.php" class=""><i class="myicon-methods"></i>Métodos de pago</a>
				</div>
			</div>
		</article>	
		<form action="carrito-2.php" id="frm-carrito" class="cleaner">
			<article id="mispedidos" class="cleaner ">
				<div id="titulo" class="cleaner">
					<div class="row cleaner">
						<div class="col-md-12 cleaner">
							<div class="titulo cleaner">
								<div class="capa cleaner">
									<div class="lineas cleaner">
										<h2>Esto es lo que <strong>llevaré</strong></h2>
									</div>
								</div>
							</div> 
						</div>
					</div>
				</div>
				<div class="listadeproductos cleaner maxw2">
					<div class="producto counterproduct cleaner animated">
						<div class="row cleaner">
							<div class="cleaner col-md-2">
								<div class="fotoproducto cleaner">
									<img src="fotos/producto.jpg" alt="" class="cleaner">
								</div>							
							</div>
							<div class="cleaner vat col-md-10">	
								<div class="cleaner titulo">
									<h2>Nombre del Producto <span>(S/ 35.00)</span></h2>
									<a href="javascript:void(0)" class="cleaner delete toanimated1"><i class="myicon-delete animated"></i></a>
								</div>
								<div class="cleaner">
									<label for="fecha dib">Datos de entrega:</label>
								</div>
								<div class="cleaner cajas row">
									<div class="cleaner minicaja col-md-3 toicon">
										<div class="control-clone cleaner">
											<div class="cleaner">
												<input id="fecha1" name="fecha1" type="text" class="myinput style2 dib validacion" placeholder="Fecha" >
												<i class="myicon-calendar triggerdate animated"></i>
											</div>
										</div>
									</div>
									<div class="cleaner minicaja col-md-3 toicon ">
										<div class="cleaner control-clone">
											<input id="hora1" name="hora1" type="text" class="myinput style2 dib validacion" placeholder="Hora" >
											<i class="myicon-time triggertime animated "></i>
										</div>
									</div>
									<div class="cleaner minicaja col-md-3">
										<select name="cantidad" id="cantidad" class="toselect">
											<option></option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
										</select>
									</div>
									<div class="cleaner total minicaja col-md-3 tar">
										<em>Total: S./200.00</em>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="producto counterproduct cleaner animated">
						<div class="row cleaner">
							<div class="cleaner col-md-2">
								<div class="fotoproducto cleaner">
									<img src="fotos/producto.jpg" alt="" class="cleaner">
								</div>
							</div>
							<div class="cleaner vat col-md-10">	
								<div class="cleaner titulo">
									<h2>Nombre del Producto <span>(S/ 35.00)</span></h2>
									<a href="javascript:void(0)" class="cleaner delete toanimated1"><i class="myicon-delete animated"></i></a>
								</div>
								<div class="cleaner">
									<label for="fecha dib">Datos de entrega:</label>
								</div>
								<div class="cleaner cajas row">
									<div class="cleaner minicaja col-md-3 toicon">
										<div class="control-clone cleaner">
											<div class="cleaner">
												<input id="fecha2" name="fecha2" type="text" class="myinput style2 dib validacion" placeholder="Fecha" >
												<i class="myicon-calendar triggerdate animated"></i>
											</div>
										</div>
									</div>
									<div class="cleaner minicaja col-md-3 toicon ">
										<div class="cleaner control-clone">
											<input id="hora2" name="hora2" type="text" class="myinput style2 dib validacion" placeholder="Hora" >
											<i class="myicon-time triggertime animated "></i>
										</div>
									</div>
									<div class="cleaner minicaja col-md-3">
										<select name="cantidad" id="cantidad" class="toselect">
											<option></option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
										</select>
									</div>
									<div class="cleaner total minicaja col-md-3 tar">
										<em>Total: S./200.00</em>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="nohaycompras cleaner">
					<div class="cleaner">
						<p>No tienes nada en el carrito</p>
					</div>
					<div class="cleaner mt">
						<a href="galeria.php" class="animated mybut but-style1">
							<span class="cleaner">Volver a productos</span>
						</a>
					</div>
				</div>
			</article>	
			<article id="ubicacion" class="nadaproductos cleaner ">
				<div class="row cleaner">
					<div class="col-md-12 cleaner">
						<div class="titulo cleaner">
							<div class="capa cleaner">
								<div class="lineas cleaner">
									<h2>¿A dónde llevamos <strong>los productos</strong>?</h2>
								</div>
							</div>
						</div> 
					</div>
				</div>
				<div class="row cleaner">
					<div class="col-md-12 cleaner">
						<div id="mimapa" class="cleaner maxw3">							
							<div class="cleaner">
								<input id="buscarUbicacion" class="myinput style2" type="text" placeholder="Buscar mi ubicación en el mapa (*)">					    
							</div>
							<div class="cleaner txt">
								<p>(*)Digite su ubicación y presionar la tecla "Enter" para encontrarla.</p>
							</div>
							<div id="load-gmap" class="cleaner" style="width:100%;height:290px"></div>
							<div class="cleaner mt control-clone">
								<input id="direccion" name="direccion" type="text" class="myinput style2" placeholder="Indicar la dirección exacta del lugar" >
								<!-- Me muestra las coordenadas -->														
								<input type="hidden" name="latitud" id="latitud" value="">
								<input type="hidden" name="longitud" id="longitud" value="">
							</div>
							<!-- Esta opciòn solo se mostrará si en el Perfil, ha ingresado datos de su casa y domicilio -->										
							<div class="cleaner mt">
								<a id="cambiarentrega" href="javascript:void(0)" class="enlace"><i class="icon-eye"></i> Enviármelos a mi hogar (ver los datos)</a>
							</div>	
							<!-- end -->							
						</div>
					</div>						
				</div>
			</article>
			<article id="calcularprecio" class="nadaproductos cleaner ">
				<div class="row cleaner">				
					<div class="col-md-12 cleaner">
						<div class="titulo cleaner">
							<div class="capa cleaner">
								<div class="lineas cleaner">
									<h2>Calculemos <strong>el precio final</strong></h2>
								</div>
							</div>
						</div> 
					</div>				
				</div>
				<div class="row cleaner">
					<div class="cleaner maxw3 dscto">
						<div class="col-md-12 cleaner">
							<div class="row cleaner">
								<div class="col-md-12 cleaner">
									<input type="text" class="style2 myinput cleaner" placeholder="Ingresa código de descuento">
								</div>
							</div>
							<div class="row cleaner space bb">
								<div class="col-md-4">
									<p>Subtotal: S./6000.00</p>
								</div>
								<div class="col-md-4">
									<p>Envio: S./6000.00</p>
								</div>
								<div class="col-md-4">
									<p>Dscto: S./6000.00</p>
								</div>
							</div>
							<div class="row cleaner space bb tac total">
								<div class="col-md-12 cleaner">
									<em>Total de la compra: s/620.00</em>
								</div>
							</div>
						</div>
					</div>
				</div>
			</article>
			<article id="mensaje" class="nadaproductos cleaner">
				<div class="row cleaner">
					<div class="col-md-12 cleaner">
						<div class="titulo cleaner">
							<div class="capa cleaner">
								<div class="lineas cleaner">
									<h2>¿Deseas enviarle <strong>un mensaje</strong>?</h2>
								</div>
							</div>
						</div> 
					</div>
				</div>
				<!-- NO EDITAR LOS VALUE DE LOS INPUT TIPOS RADIO -->
				<div id="myform" class="row cleaner">
					<div class="content cleaner maxw3">
						<div class="col-md-6">
							<div class="content-radio cleaner tac ma">
								<div class="cleaner">
									<input type="radio" name="enviarmensaje" id="si" value="si" class="radio" checked>
									<label for="si"></label>
								</div>
								<div class="cleaner">
									<label for="si">Sí, me gustaría</label>
								</div>
							</div>
						</div>	
						<div class="col-md-6">
							<div class="content-radio cleaner tac ma">
								<div class="cleaner">
									<input type="radio" name="enviarmensaje" id="no" value="no" class="radio" >
									<label for="no"></label>
								</div>
								<div class="cleaner">
									<label for="no">No, gracias</label>
								</div>
							</div>						
						</div>	
					</div>
				
					<div id="frm-mensaje" class="cleaner maxw3">
						<div class="row cleaner">
							<div class="col-md-12 cleaner">
								<div class=" cleaner">
									<div class="col-md-6 cleaner">
										<div class="control-clone cleaner">
					                        <div class="cleaner">
					                            <input type="text" class="myinput style2 cleaner" placeholder="¿Para quién es el obsequio?" id="nombre" name="nombre">
					                        </div>
					                    </div>
									</div>							
				                    <div class="col-md-6 cleaner">
										<div class="control-clone cleaner">
					                        <div class="cleaner">
					                            <input type="text" class="myinput style2 cleaner" placeholder="¿Cuál es la ocasión?" id="ocasion" name="ocasion">
					                        </div>
					                    </div>
									</div>
								</div>
							</div>
						 	<div class="col-md-12 cleaner ">
		                        <div class="cleaner col-md-12">
		                        	<div class="cleaner control-clone">
			                            <textarea name="comentario" id="comentario" class="cleaner mytextarea style1" placeholder="comentario"></textarea>
			                        </div>
		                        </div>
		                    </div>
		                    <div class="col-md-12 cleaner">
								<div class="cleaner">
									<div class="col-md-6 cleaner">
										<div class="control-clone cleaner">
					                        <div class="cleaner">
					                            <input type="text" class="myinput style2 cleaner" placeholder="¿A nombre de quièn lo enviamos?" id="elqueregala" name="elqueregala">
					                        </div>
					                    </div>
									</div>							
				                    <div class="col-md-6 cleaner">
										<div class="control-clone cleaner">
					                        <div class="content-check cleaner">
												<div class="cleaner">
													<input type="checkbox" name="anonimo" id="anonimo" value="1" class="check">
													<label for="anonimo"></label>
												</div>
												<div class="cleaner">
													<label for="anonimo">Enviarlo como anónimo</label>
												</div>
											</div>
					                    </div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</article>
			<div class="butsubmit cleaner maxw3 tac nadaproductos ">
				<button type="submit" class="cleaner animated mybut but-style1">
					<span class="cleaner">Ir al siguiente paso</span>
				</button>
			</div>
		</form>
	</section>

	<!-- Productos favoritos -->
	<section id="favoritos" class="cleaner maxw movetrigger container-fluid">
		<div class="row cleaner">
			<div class="col-md-12 cleaner">
				<div class="titulo cleaner">
					<div class="capa cleaner">					
						<div class="lineas cleaner flechas">
							<a href="javascript:void(0)" class="flecha toizq cleaner noresponsive"><i class="icon-arrow-left"></i></a>
							<h2>Productos que has visto<strong> en Chocolate Express</strong></h2>
							<a href="javascript:void(0)" class="flecha toizq cleaner responsive"><i class="icon-arrow-left"></i></a>
							<a href="javascript:void(0)" class="flecha toder cleaner"><i class="icon-arrow-right"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row cleaner">
			<!-- Productos Favoritos -->			
			<?php include 'includes/productosfavoritos.php' ?>
			<!-- end -->
		</div>
	</section>	
	
	<!-- end Favoritos -->

	<!-- ==========================================
	{{fin del contenido principal}}
	=========================================== -->

	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<?php include 'includes/footer.php' ?>	
	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
	<script>
		$(window).on({
			load: function(){
				// Primera carga, con coordenadas por defecto al establecimiento de Chocolate Express
				var aquiestaChocolateExpress = new loadGmap(-12.127498, -77.028022);

				// Cargar Productos vistos/Favoritos para Vender {Footer}
				$.getJSON('json/productos.json', function(data) {
				    var template = $('#tmp-favoritos').html();
				    var info = Mustache.to_html(template, data);
				    $('#productosfavoritos').html(info);
				}).done(function(){					
					$('.productosfavoritos').owlCarousel({
					    items:5,
					    dots:false,
					    loop:true,
					    margin:30,
					    // autoHeight: true,
					    responsive:{
					        0:{
					            items:1
					        },
					        450:{
					            items:1
					        },
					        550:{
					            items:2
					        },
					        800:{
					            items:3
					        },
					        1000:{
					            items:4
					        },
					        1200:{
					            items:5
					        }
					    }
					});
					$('.productosfavoritos').removeClass('opacar').delay(10).promise().done(function(){


						$('.descripcion .tobottom').remove();
						$('.descripcion .totop').css('top','30px');

						$('.like').on('click', function(){
							$(this).toggleClass('activo');					

							if($(this).hasClass('activo')){
								$(this).find('i').addClass('bounceIn');						
							}else{
								$(this).find('i').removeClass('bounceIn');
							}
						})
					});
				});

			},
			resize: function(){
				$('#cargar-productos').on('refresh.owl.carousel', function(){
					$('.owl-height').removeAttr('style')
				});
			}
		});

		$(document).ready(function(){

			// Agregar clase, cuando el producto se subió  carrito
			$('.tocarro').on('click', '.agregaralcarrito', function(){
				// $(this).closest('.producto').find('.accion').addClass('processing');
				$(this).closest('.agregarproducto').html('<img src="imagenes/loading.gif" />').delay(1050).promise().done(function(){
					$(this).closest('.item').addClass('prodadded').find('.agregarproducto').html('<i class="icon-circle-check"></i> Producto agregado');
				});			
			})


			// Ver mi direcciòn ingresada en mi perfil
			$("#cambiarentrega").on('click',function(){
				var aquiestamicasa = new loadGmap(-12.093767, -77.052993);
				$('#direccion').val('Mz s lote 1 calle las MOras Urb Ceres (estos datos son capturados del perfil del usuario, si es que lo hay)')
			});
			// end


			$('#frm-carrito').validate({
				rules: {		
					nombre: {
						required: true
					},
					ocasion: {
						required: true
					},
					comentario: {
						required: true
					},
					elqueregala: {
						required: true
					},
					direccion: {
						required: true
					}
				},
				messages: {					
					nombre: {
						required: '&nbsp;'
					},
					ocasion: {
						required: '&nbsp;'
					},
					comentario: {
						required: '&nbsp;'
					},
					elqueregala: {
						required: '&nbsp;'
					},
					direccion: {
						required: '&nbsp;'
					}
				},				
				errorClass: "incorrecto",
				errorElement: "span",
				showErrors: function(errorMap, errorList){
				    var counterError = this.numberOfInvalids();					  
					$('.validacion').each(function(i, elem){
						if($(elem).val() == ''){
							$(elem).closest('.control-clone').addClass('error')
							counterError++;
						}else{
							$(elem).closest('.control-clone').removeClass('error')
						}
					});
					if(counterError == 1){
						$("#contenedor-alerta .texto span").html("Se presentó "+ counterError + " error.");						
					}else{
						$("#contenedor-alerta .texto span").html("Se presentaron "+ counterError + " errores.");
					}
					this.defaultShowErrors();
				},				
				unhighlight: function(element){
					$(element).closest('.control-clone').removeClass('error').addClass('success');
				},
				highlight: function(element){
					$(element).closest('.control-clone').removeClass('success').addClass('error');
				},
				invalidHandler: function (event, validator) {
		            var existeError = validator.numberOfInvalids();		            
		            if (existeError) {		            	
		            	$('body').addClass('frmError');  
		            	$("#contenedor-alerta").closest('body').addClass('showContenedor');
		            	$('button:submit').attr('disabled','disabled');
					  	setTimeout(function(){
					    	$('body').removeClass('showContenedor');
					    	$('button:submit').removeAttr('disabled');
					    }, 4900);

		            }else{
		            	$('body').removeClass('frmError');
		            }
		        },
				success: function(element){
					element.closest('body').removeClass('frmError').addClass('frmSuccess');
				}
			});
			
			// Ir al top, si existen errores en la data de los productos
			$('form').on('submit',function(event){
				var error = 0;
				$('.validacion').each(function(i, elem){
					if($(elem).val() == ''){
						$('body,html').animate({
	                        scrollTop:$('#mispedidos').offset().top
	                    },1000);
					}			
				});
			});

			// Eliminar productos "nadaproductos"
			$('.delete').click(function(){
				$(this).closest('.producto').fadeOut('500',function(){
					$(this).remove();
				})
				// Para ver si se descativa secciones de la página
				contarProductos();
			});
			
		})		

		function contarProductos(){
			$('.listadeproductos').each(
				function() {
					var productos = ($(this).find('.producto')).length;
					var total = productos-1;
					if(total==0){
						$('.nohaycompras').fadeIn();
						$('.listadeproductos').fadeOut(1200, function(){
							
							$('.nadaproductos ').fadeOut('slow');
						})						
					}
				}
		    );
		}

		
	</script>	
</body>
</html>