<!doctype html>
<html lang="en">
<head>
	<!-- ===================================
	{{Chunk site_seo}}
	==================================== -->
	<?php include 'includes/seo.php' ;?>
	<!-- ===================================
	{{fin Chunk site_seo}}
	==================================== -->
	<meta name="description" content="Detalle del Producto">
    <meta property="og:title" content="[[*page_title]]" /> 
    <meta property="og:url" content="[[++site_url]][[~id]]" />
    <meta property="og:image" content="[[*page_imagen]]" />
    <meta property="og:description" content="[[*page_description]]" /> 
    <meta name="twitter:image:src" content="[[*page_imagen]]"/>
	<title>[[*page_title]]</title>
	<style>
		.error input.myinput:read-only{background: #EF7169 !important;border-color: #fff !important;color:#fff !important;}
		input.myinput:read-only {
			border: 1px solid #f3a848 !important;
			padding: 9px !important;
			text-align: left !important;
			font: 18px roboto !important;
			color: #66340f !important;
			overflow: visible !important;
			background: #fff !important;
		}
		#fotoperfil {opacity: 0;height: 1px;}
		/*Subiendo archivo*/
		.capaloading .icono:after { content: 'Subiendo...';  position: absolute; bottom: -20px; left: -7px; font: 12px roboto; color: #817878;}
		.capaloading{position: fixed; width: 100%; height: 100%; background-color: rgba(255, 255, 255, 0.77); z-index: 9999; display: none; left:0;  top:0;}
		.capaloading .icono{position:absolute;width:30PX;height:30PX;margin:auto;top:0;bottom:0;right:0;left:0;background-image: url('imagenes/loader.gif');}
	</style>
</head>
<body>	
	<!-- ===================================
	{{Chunk site_header}}
	==================================== -->
	<?php include 'includes/header.php' ?>
	<!-- ===================================
	{{fin Chunk site_header}}
	==================================== -->
	
	<!-- ===================================
	{{Contenido principal}}
	==================================== -->
	<!-- Subiendo archivo -->	
	<div class="capaloading cleaner">
	    <div class="icono cleaner"></div>
	</div>

	<section id="principal" class="cleaner perfil maxw container-fluid">		
		<article id="mispedidos" class="cleaner row mt2 maxw2 ma mb">
			<div class="col-md-12 cleaner">		 		
				<div class="listadeproductos cleaner row ">
					<div class="col-md-4">
						<?php include 'includes/menu_perfil.php' ?>
					</div>
					<div class="col-md-8">								
						<div class="tospace cleaner">
							<div id="titulo" class="cleaner">
								<div class="row cleaner">
									<div class="col-md-12 cleaner">
										<div class="titulo cleaner">
											<div class="capa cleaner">
												<div class="lineas cleaner">
													<h2>Cambiar foto de<strong> perfil</strong></h2>
												</div>
											</div>
										</div> 
									</div>
								</div>
							</div>
							<div class="cleaner mt">
								<form action="perfil.php" id="frm-subir" class="cleaner">
									<div class="row cleaner">
										<div class="col-md-12 cleaner">
											<div class="row cleaner">
												<div class="col-md-8 cleaner control-clone condition-to-send mb">
													<input id="getfotoperfil" name="getfotoperfil" type="text" class="myinput style2 cleaner " placeholder="Ubicación de la foto" readOnly>
												</div>
												<div class="col-md-4 cleaner">
													<div class="cleaner">
														<a id="but-fotoperfil" href="javascript:void(0)" class="cleaner mybut but-style2">
															<span><i class="icon-cloud-upload" style="vertical-align:-1px"></i>&nbsp;Examinar</span>
														</a>
														<input id="fotoperfil" type="file" name="foto1" class="cleaner">
													</div>														
												</div>
											</div>
										</div>
										<div class="col-md-12 cleaner">
											<div class="cleaner mt ma tac">
												<button type="submit" class="cleaner animated mybut but-style1">
													<span class="cleaner">Actualizar mi foto</span>
												</button>
											</div>
										</div>
									</div>
									
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</article>	
	</section>

	<!-- ==========================================
	{{fin del contenido principal}}
	=========================================== -->

	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<?php include 'includes/footer.php' ?>	
	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<script>
		$(window).on({
			load: function(){							
			},
			resize: function(){				
			}
		});

		$(document).ready(function(){
			// Subiendo archivo
			$('#but-fotoperfil').click(function(event) {
				$('#fotoperfil').trigger('click')
			});
			

			// URL destino de la foto
			$('#fotoperfil').change(function(){
	            $('#getfotoperfil').val($(this).val());
	            if($('#fotoperfil').val() != ''){
	                $(this).closest('.control-clone').removeClass('error');
	                $(this).closest('.control-clone').addClass('success');
	            }
	        });
	        $('#getfotoperfil').change(function(){
	            if($('#getfotoperfil').val() != ''){
	                $(this).closest('.control-clone').removeClass('error');
	                $(this).closest('.control-clone').addClass('success');
	            }
	        });

	        // Actualizar foto
	        $("#fotoperfil").change(function(){
	            readFoto(this);
	            if($('#fotoperfil').val() != ""){
	                $(this).closest('.todomifoto').find('.delete').fadeIn();
	            }
	        });
	        function readFoto(input) {
	            if (input.files && input.files[0]) {
	                var reader = new FileReader();            
	                reader.onload = function (e) {
	                    $('#fotousuario').attr('src', e.target.result);
	                }                
	                reader.readAsDataURL(input.files[0]);
	            }
	        }

			$('#frm-subir').validate({
				ignore: '',
				rules: {		
					getfotoperfil: {
						required: true
					}		
				},
				messages: {					
					getfotoperfil: {
						required: '&nbsp;'

					}
				},
				errorClass: "incorrecto",
				errorElement: "span",
				showErrors: function(errorMap, errorList){
				    var numErrores = this.numberOfInvalids();
					if(numErrores == 1){
						$("#contenedor-alerta .texto span").html("Se presentó "+ this.numberOfInvalids() + " error.");
					    $("#contenedor-alerta").addClass('block');
					    setTimeout(function(){
					    	 $("#contenedor-alerta").removeClass('block');
					    }, 0)   
					    this.defaultShowErrors();
					}else{
						$("#contenedor-alerta .texto span").html("Se presentaron "+ this.numberOfInvalids() + " errores.");
					    $("#contenedor-alerta").addClass('block');
					    setTimeout(function(){
					    	 $("#contenedor-alerta").removeClass('block');
					    }, 0)  
					    this.defaultShowErrors();	
					}
				},
				unhighlight: function(element, errorClass) {
				    var numberOfInvalids = this.numberOfInvalids();				    
				    if (numberOfInvalids === 0) {
				        $(element).closest('body').find("#contenedor-alerta").removeClass('block');
				    }else{
				    	$(element).closest('body').find("#contenedor-alerta").addClass('block');
				    }   
				},
				highlight: function(element){
					$(element).closest('.control-clone').removeClass('success').addClass('error');
					$('body').removeClass('listoparaenviar');
				},
				success: function(element){
					element.closest('.control-clone').removeClass('error').addClass('success');
					$('body').addClass('listoparaenviar');
				},
				submitHandler: function (form) {
				    if($('.condition-to-send').hasClass('error')){
		            	return false
		            }else{
		            	setTimeout(function(){
		                    $('.listoparaenviar .capaloading').fadeIn();
		                },1000)
		                return false
		            }
                }
			});	    
		})
	</script>	
</body>
</html>