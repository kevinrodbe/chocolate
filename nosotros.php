<!doctype html>
<html lang="en">
<head>
	<!-- ===================================
	{{Chunk site_seo}}
	==================================== -->
	<?php include 'includes/seo.php' ;?>
	<!-- ===================================
	{{fin Chunk site_seo}}
	==================================== -->
	<meta name="description" content="Detalle del Producto">
    <meta property="og:title" content="[[*page_title]]" /> 
    <meta property="og:url" content="[[++site_url]][[~id]]" />
    <meta property="og:image" content="[[*page_imagen]]" />
    <meta property="og:description" content="[[*page_description]]" /> 
    <meta name="twitter:image:src" content="[[*page_imagen]]"/>
	<title>[[*page_title]]</title>
	<style>
		/*Galeria c/ click para detalles - Producto*/
		#milistaproductos .flechas i{line-height: 38px}
		#milistaproductos .flechas{text-align: center;margin:0 auto;}
		#milistaproductos .flechas .toizq{position: absolute;left: 0px;top: 0;bottom: 0;margin: auto;width: 40px;height: 40px;}
		#milistaproductos .flechas .toder {position: absolute;right: 0px;top: 0;bottom: 0;margin: auto;	width: 40px;height: 40px;}
		#milistaproductos #photo{margin: 0px auto;text-align: center;max-width: 350px;width:100%;}
        #milistaproductos .item{max-width: 250px;margin: 0 auto;cursor: pointer;}
        #milistaproductos #cargar-productos{margin:20px auto;position: relative;max-width: 80%}
	</style>
</head>
<body>
	<!-- ===================================
	{{Chunk site_header}}
	==================================== -->
	<?php include 'includes/header.php' ?>
	<!-- ===================================
	{{fin Chunk site_header}}
	==================================== -->

	<!-- ===================================
	{{Contenido principal}}
	==================================== -->
	<section id="principal" class="cleaner">		
		<article id="galeria" class="cleaner mt2 maxw">
			<div id="titulo" class="cleaner">
				<div class="row cleaner">
					<div class="col-md-12 cleaner">
						<div class="titulo cleaner">
							<div class="capa cleaner">
								<div class="lineas cleaner">
									<h2>Un poco acerca de <strong>nosotros</strong></h2>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="milistaproductos" class="cleaner movetrigger detallesdeproducto">
				<div class="row cleaner">
					<div class="col-md-4 cleaner">
						<div class="cleaner izquierda">                           
                            <!-- Template Productos -->
							<script id="tmp-fotos-nosotros" type="text/template">
								{{#productos}}				
									<div class="item cleaner">										
						                <img src="{{foto}}" alt="{{foto}}" class="img">
						            </div>
								{{/productos}}
							</script>
                            <div class="cleaner flechas pr">
                            	<div id="cargar-productos" class="owl-carousel">
	                               <!-- Cargar los productos de productos.json  -->
	                            </div>	
								<a href="javascript:void(0)" class="flecha toizq cleaner"><i class="icon-arrow-left"></i></a>
                            	<a href="javascript:void(0)" class="flecha toder cleaner"><i class="icon-arrow-right"></i></a>
							</div>
                        </div>
					</div>
					<div class="col-md-8 cleaner">
						<div class="cleaner derecha">							
							<div class="cleaner descripcion">
								<p>nteger nec massa quis nunc vehicula elementum vitae at diam. Sed sit amet tellus vel ante sodales tristique eu ut sem. Mauris quis mi ut lorem molestie condimentum. Maecenas quis augue vel eros tempor maximus. Vestibulum porta ultricies leo, ut faucibus enim tincidunt id. Nullam consequat tincidunt metus, nec tristique nisi elementum eu. Donec interdum, odio at volutpat faucibus, augue elit interdum sem, sed eleifend dui justo eget quam.</p>
							</div>
							<div class="cleaner">
								<ul class="cleaner mylist-0">
									<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae ex varius, vulputate justo quis, tristique urna.</li>
									<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae ex varius, vulputate justo quis, tristique urna.</li>
									<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae ex varius, vulputate justo quis, tristique urna.</li>
								</ul>
							</div>
						</div>
					</div>					
				</div>
			</div>			
		</article>
		<article id="procesos" class="cleaner mt maxw">
			<div class="row cleaner">
				<div class="col-md-12 cleaner">
					<div class="titulo cleaner">
						<div class="capa cleaner">
							<div class="lineas cleaner">
								<h2>Compre fácil  y seguro en <strong>Chocolate Express</strong></h2>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="cleaner content img">
				<div class="row cleaner">
					<div class="col-md-3 myline3 cleaner">
						<div class="caja cleaner">
							<div class="cleaner noresponsive">
								<img src="imagenes/entrarweb.png" alt="">
							</div>
							<div class="cleaner">
								<h3>1. Entra a nuestra web</h3>
							</div>
							<div class="cleaner">
								<p>Visita nuestra web para encontrar lo más finios regalos</p>
							</div>
							<div class="cleaner responsive">
								<img src="imagenes/entrarweb.png" alt="">
							</div>
						</div>
					</div>
					<div class="col-md-3 myline3 cleaner">
						<div class="caja cleaner">
							<div class="cleaner noresponsive">
								<img src="imagenes/elijeproducto.png" alt="">
							</div>
							<div class="cleaner">
								<h3>2. Elige tu producto</h3>
							</div>
							<div class="cleaner">
								<p>Elige de nuestra gran variedad de productos.</p>
							</div>
							<div class="cleaner responsive">
								<img src="imagenes/elijeproducto.png" alt="">
							</div>
						</div>
					</div>
					<div class="col-md-3 myline3 cleaner">
						<div class="caja cleaner">
							<div class="cleaner noresponsive">
								<img src="imagenes/compraseguro.png" alt="">
							</div>
							<div class="cleaner">
								<h3>3. Compra seguro</h3>
							</div>
							<div class="cleaner">
								<p>Toda operación estará protegida por una conexión segura.</p>
							</div>
							<div class="cleaner responsive">
								<img src="imagenes/compraseguro.png" alt="">
							</div>
						</div>
					</div>
					<div class="col-md-3 myline3 cleaner">
						<div class="caja cleaner">
							<div class="cleaner noresponsive">
								<img src="imagenes/entregaregalo.png" alt="">
							</div>
							<div class="cleaner">
								<h3>4. Entrega del regalo</h3>
							</div>
							<div class="cleaner">
								<p>El regalo llegará en perfectas condiciones a su destino.</p>
							</div>
							<div class="cleaner responsive">
								<img src="imagenes/entregaregalo.png" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>			
		</article>		
		<article id="noticias" class="cleaner maxw movetrigger">
			<div class="row cleaner">
				<div class="col-md-12 cleaner">
					<div class="titulo cleaner">
						<div class="capa cleaner">					
							<div class="lineas cleaner flechas">
								<a href="javascript:void(0)" class="flecha toizq cleaner noresponsive"><i class="icon-arrow-left"></i></a>
								<h2>Lo acontecido en <strong>Chocolate Express</strong></h2>
								<a href="javascript:void(0)" class="flecha toizq cleaner responsive"><i class="icon-arrow-left"></i></a>
								<a href="javascript:void(0)" class="flecha toder cleaner"><i class="icon-arrow-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row cleaner">
				<ul id="news" class="cleaner news cleanresponsive mt mb">
					<li class="item mt mb">				
						<div class="row cleaner ma">
							<div class="col-md-4 cleaner">
								<div class="cleaner izquierda">                           
		                            <img src="imagenes/laptop-3.png" alt="Post 1" class="img">
		                        </div>
							</div>
							<div class="col-md-8 cleaner">
								<div class="cleaner derecha">							
									<div class="cleaner richtext ">
										<h2>En Chocolate Express</h2>
										<p>nteger nec massa quis nunc vehicula elementum vitae at diam. Sed sit amet tellus vel ante sodales tristique eu ut sem. Mauris quis mi ut lorem molestie condimentum. Maecenas quis augue vel eros tempor maximus. Vestibulum porta ultricies leo, ut faucibus enim tincidunt id. Nullam consequat tincidunt metus, nec tristique nisi elementum eu. Donec interdum, odio at volutpat faucibus, augue elit interdum sem, sed eleifend dui justo eget quam.</p>
										<p>nteger nec massa quis nunc vehicula elementum vitae at diam. Sed sit amet tellus vel ante sodales tristique eu ut sem. Mauris quis mi ut lorem molestie condimentum. Maecenas quis augue vel eros tempor maximus. Vestibulum porta ultricies leo, ut faucibus enim tincidunt id. Nullam consequat tincidunt metus, nec tristique nisi elementum eu. Donec interdum, odio at volutpat faucibus, augue elit interdum sem, sed eleifend dui justo eget quam.</p>
										
									</div>
									
								</div>
							</div>					
						</div>
					</li>
					<li class="item mt mb">				
						<div class="row cleaner ma">
							<div class="col-md-4 cleaner">
								<div class="cleaner izquierda">                           
		                            <img src="imagenes/laptop-3.png" alt="Post 1" class="img">
		                        </div>
							</div>
							<div class="col-md-8 cleaner">
								<div class="cleaner derecha">							
									<div class="cleaner richtext ">
										<h2>En Chocolate Express</h2>
										<p>nteger nec massa quis nunc vehicula elementum vitae at diam. Sed sit amet tellus vel ante sodales tristique eu ut sem. Mauris quis mi ut lorem molestie condimentum. Maecenas quis augue vel eros tempor maximus. Vestibulum porta ultricies leo, ut faucibus enim tincidunt id. Nullam consequat tincidunt metus, nec tristique nisi elementum eu. Donec interdum, odio at volutpat faucibus, augue elit interdum sem, sed eleifend dui justo eget quam.</p>
										<p>nteger nec massa quis nunc vehicula elementum vitae at diam. Sed sit amet tellus vel ante sodales tristique eu ut sem. Mauris quis mi ut lorem molestie condimentum. Maecenas quis augue vel eros tempor maximus. Vestibulum porta ultricies leo, ut faucibus enim tincidunt id. Nullam consequat tincidunt metus, nec tristique nisi elementum eu. Donec interdum, odio at volutpat faucibus, augue elit interdum sem, sed eleifend dui justo eget quam.</p>
										
									</div>
									
								</div>
							</div>					
						</div>
					</li>
					
				</ul>
			</div>
		</article>	
		<article id="aliados" class="cleaner maxw movetrigger">
			<div class="row cleaner">
				<div class="col-md-12 cleaner">
					<div class="titulo cleaner">
						<div class="capa cleaner">					
							<div class="lineas cleaner flechas">
								<a href="javascript:void(0)" class="flecha toizq cleaner noresponsive"><i class="icon-arrow-left"></i></a>
								<h2>Trabajamos con las siguientes <strong>empresas</strong></h2>
								<a href="javascript:void(0)" class="flecha toizq cleaner responsive"><i class="icon-arrow-left"></i></a>
								<a href="javascript:void(0)" class="flecha toder cleaner"><i class="icon-arrow-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row cleaner">
				<ul id="clientes" class="cleaner clientes cleanresponsive mt mb">
					<li class="item">				
						<div class="producto cleaner">
							<div class="foto cleaner">
								<img src="fotos/cliente1.jpg" alt="Cliente 1">
							</div>					
						</div>
					</li>
					<li class="item">				
						<div class="producto cleaner">
							<div class="foto cleaner">
								<img src="fotos/cliente2.jpg" alt="Cliente 1">
							</div>					
						</div>
					</li>
					<li class="item">				
						<div class="producto cleaner">
							<div class="foto cleaner">
								<img src="fotos/cliente3.jpg" alt="Cliente 1">
							</div>					
						</div>
					</li>
					<li class="item">				
						<div class="producto cleaner">
							<div class="foto cleaner">
								<img src="fotos/cliente1.jpg" alt="Cliente 1">
							</div>					
						</div>
					</li>
					<li class="item">				
						<div class="producto cleaner">
							<div class="foto cleaner">
								<img src="fotos/cliente2.jpg" alt="Cliente 1">
							</div>					
						</div>
					</li>
					<li class="item">				
						<div class="producto cleaner">
							<div class="foto cleaner">
								<img src="fotos/cliente3.jpg" alt="Cliente 1">
							</div>					
						</div>
					</li>
					<li class="item">				
						<div class="producto cleaner">
							<div class="foto cleaner">
								<img src="fotos/cliente1.jpg" alt="Cliente 1">
							</div>					
						</div>
					</li>
					<li class="item">				
						<div class="producto cleaner">
							<div class="foto cleaner">
								<img src="fotos/cliente2.jpg" alt="Cliente 1">
							</div>					
						</div>
					</li>
					<li class="item">				
						<div class="producto cleaner">
							<div class="foto cleaner">
								<img src="fotos/cliente3.jpg" alt="Cliente 1">
							</div>					
						</div>
					</li>
					<li class="item">				
						<div class="producto cleaner">
							<div class="foto cleaner">
								<img src="fotos/cliente1.jpg" alt="Cliente 1">
							</div>					
						</div>
					</li>
					<li class="item">				
						<div class="producto cleaner">
							<div class="foto cleaner">
								<img src="fotos/cliente2.jpg" alt="Cliente 1">
							</div>					
						</div>
					</li>
					<li class="item">				
						<div class="producto cleaner">
							<div class="foto cleaner">
								<img src="fotos/cliente3.jpg" alt="Cliente 1">
							</div>					
						</div>
					</li>
				</ul>
			</div>
		</article>	
	</section>


	<!-- ==========================================
	{{fin del contenido principal}}
	=========================================== -->

	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<?php include 'includes/footer.php' ?>	
	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->

	<script>
		$(window).on({
			load: function(){				  
			},
			resize: function(){
				$('#cargar-productos').on('refresh.owl.carousel', function(){
					$('.owl-height').removeAttr('style')
				});
			}
		});

		$(document).ready(function(){          

			$('.news').owlCarousel({
			    items:1,
			    dots:false,
			    loop:true,
			    margin:0,
			    autoHeight: true			    
			});

			$('.clientes').owlCarousel({
			    items:8,
			    dots:false,
			    loop:true,
			    margin:30,
			    // autoHeight: true,
			    responsive:{
			        0:{
			            items:2
			        },
			        450:{
			            items:3
			        },
			        550:{
			            items:3
			        },
			        800:{
			            items:4
			        },
			        1000:{
			            items:6
			        },
			        1200:{
			            items:8
			        }
			    }
			});

			// Cargar fotos de Noosotros
			var fotosdenosotros = {
				"productos":[
					{							
						"foto": "fotos/producto.jpg"
					},
					{							
						"foto": "fotos/producto2.jpg"
					},
					{							
						"foto": "fotos/producto.jpg"
					},
					{							
						"foto": "fotos/producto2.jpg"
					},
					{							
						"foto": "fotos/producto.jpg"
					},
					{							
						"foto": "fotos/producto2.jpg"
					}
				]
			}
			var template = $('#tmp-fotos-nosotros').html();
		    var info = Mustache.to_html(template, fotosdenosotros);
		    $('#cargar-productos').html(info).delay(500).promise().done(function(){
		    	$('#cargar-productos').owlCarousel({
	                items: 1,
	                dots: false,
	                lazyLoad: true,
	                autoHeight: true,
	                loop: true,
	                margin: 10,
	                responsive: {
	                    0: {
	                        items: 1
	                    },
	                    1200: {
	                        items: 1
	                    }
	                }
	            });
		    });            

			// Agregar eventos si es Mobile
			setTimeout(function(){
				comprobarnavegador();
			},100)

		})
	</script>
</body>
</html>