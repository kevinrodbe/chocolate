<!doctype html>
<html lang="en">
<head>
	<!-- ===================================
	{{Chunk site_seo}}
	==================================== -->
	<?php include 'includes/seo.php' ;?>
	<!-- ===================================
	{{fin Chunk site_seo}}
	==================================== -->
	<meta name="description" content="Detalle del Producto">
    <meta property="og:title" content="[[*page_title]]" /> 
    <meta property="og:url" content="[[++site_url]][[~id]]" />
    <meta property="og:image" content="[[*page_imagen]]" />
    <meta property="og:description" content="[[*page_description]]" /> 
    <meta name="twitter:image:src" content="[[*page_imagen]]"/>
	<title>[[*page_title]]</title>	
	<style>
		.maxwimg{max-width: 300px;margin: 0 auto}
	</style>
</head>
<body>
	<!-- ===================================
	{{Chunk site_header}}
	==================================== -->
	<?php include 'includes/header.php' ?>
	<!-- ===================================
	{{fin Chunk site_header}}
	==================================== -->

	<!-- ===================================
	{{Contenido principal}}
	==================================== -->
	<section id="principal" class="cleaner">		
		<article id="formularios" class="cleaner mt2 maxw">
			<div id="titulo" class="cleaner">
				<div class="row cleaner">
					<div class="col-md-12 cleaner">
						<div class="titulo cleaner">
							<div class="capa cleaner">
								<div class="lineas cleaner">
									<h2>Ups! <strong>Error 404</strong></h2>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="micontacto" class="cleaner maxw3 mt mb">
				<div class="tac ma cleaner mt mb">
					<div class="globo cleaner tac txt">
						<p>Ha ocurido un error en la página, o la misma ya no existe.</strong></p>
					</div>
					<div class="foto cleanr">
						<img src="imagenes/laptop-chocolate.svg" alt="Contactar ahora" class="cleaner full maxwimg">
					</div>
					<div class="cleaner mt">
						<a href="./" class="animated mybut but-style2 mt">
							<span class="cleaner"><i class="icon-home"></i>&nbsp;&nbsp;Volver al inicio</span>
						</a>
					</div>
				</div>
			</div>	
		</article>
	
	</section>


	<!-- ==========================================
	{{fin del contenido principal}}
	=========================================== -->

	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->
	<?php include 'includes/footer.php' ?>	
	<!-- ===================================
	{{Chunk site_footer}}
	==================================== -->

	<script>
		$(window).on({
			load: function(){				  
			},
			resize: function(){				
			}
		});

		$(document).ready(function(){          


		})
	</script>
</body>
</html>